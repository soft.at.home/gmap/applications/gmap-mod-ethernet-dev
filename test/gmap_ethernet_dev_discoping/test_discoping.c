/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <fcntl.h>
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev_bridge.h"
#include <yajl/yajl_gen.h> // required by amxj/amxj_variant.h
#include "amxj/amxj_variant.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include "amxd/amxd_object.h"
#include <string.h>
#include <amxd/amxd_transaction.h>
#include <arpa/inet.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "test_discoping.h"
#include "../common/mock_discoping.h"


/** construct result that gmap_device_get will return */
static amxc_var_t* s_retval_of_gmap_get(const char* key, const char* netdev_name, uint32_t netdev_index, const char* mac) {
    amxc_var_t* dev;
    amxc_var_new(&dev);
    amxc_var_set_type(dev, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dev, "Key", key);
    amxc_var_add_key(cstring_t, dev, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, dev, "NetDevName", netdev_name);
    amxc_var_add_key(uint32_t, dev, "NetDevIndex", netdev_index);
    return dev;
}

static void s_set_ip_data(gmap_query_t* query_macip, const char* mac, gmap_query_action_t action) {
    amxc_var_t bridge_query_fields;
    amxc_var_t* gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, mac);

    amxc_var_init(&bridge_query_fields);
    amxc_var_set_type(&bridge_query_fields, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &bridge_query_fields, "Key", "bridge1");
    amxc_var_add_key(cstring_t, &bridge_query_fields, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, &bridge_query_fields, "NetDevName", "bridge_netdevname");
    amxc_var_add_key(uint32_t, &bridge_query_fields, "NetDevIndex", 1);

    query_macip->fn(query_macip, "bridge1", &bridge_query_fields, action);

    amxc_var_clean(&bridge_query_fields);
    amxc_var_delete(&gmap_bridge);
}

static void s_create_bridge_and_discoping(
    mock_discoping_t* mock_ext_discoping,
    amxc_var_t** gmap_bridge,
    gmap_eth_dev_bridge_collection_t** bridges,
    gmap_query_t* query_macip,
    const char* ip_odl,
    const char* ip) {

    assert_non_null(mock_ext_discoping);
    assert_non_null(gmap_bridge);
    assert_non_null(bridges);
    assert_null(*bridges);
    assert_non_null(query_macip);

    gmap_client_init(get_dummy_bus_ctx());

    *gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, "0b:07:01:0d:09:03");

    expect_string(__wrap_gmap_query_open_ext_2, expression, "bridge mac interface lan && .PhysAddress != '' && .NetDevName != '' && (ipv4 || ipv6)");
    will_return(__wrap_gmap_query_open_ext_2, query_macip);
    will_return(__wrap_discoping_new, mock_ext_discoping);
    will_return(__wrap_gmap_eth_dev_bridgetable_new, NULL);
    if(gmap_eth_dev_get_ctx_dhcp() != NULL) {
        will_return(__wrap_gmap_eth_dev_dhcp_reader_new, NULL);
    }
    will_return(__wrap_gmap_eth_dev_neighbors_new, NULL);
    gmap_eth_dev_bridge_collection_new(bridges, get_dummy_parser());
    assert_non_null(*bridges);

    test_util_parse_odl("../common/dummy-gmap.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge-mac.odl");
    test_util_parse_odl(ip_odl);
    test_util_handle_events();

    s_set_ip_data(query_macip, "0b:07:01:0d:09:03", gmap_query_expression_start_matching);
    test_util_handle_events();

    mock_discoping_assert(mock_ext_discoping, 1, 0, "bridge_netdevname", "0b:07:01:0d:09:03", ip);
}

static void s_create_bridge_and_discoping_ipv4(
    mock_discoping_t* mock_ext_discoping,
    amxc_var_t** gmap_bridge,
    gmap_eth_dev_bridge_collection_t** bridges,
    gmap_query_t* query_macip) {
    s_create_bridge_and_discoping(mock_ext_discoping, gmap_bridge, bridges, query_macip, "gmap-mockdata-bridge-ip.odl", "192.168.1.1");
}

static void s_test_discoping_up(const char* ip, uint32_t family, const char* expected_address_scope, const char* expected_address_source) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    const char* expected_tags = NULL;

    switch(family) {
    case AF_INET:
        expected_tags = "lan edev mac physical ipv4";
        break;
    case AF_INET6:
        expected_tags = "lan edev mac physical ipv6";
        break;
    default:
        assert_true(false);
        break;
    }

    // GIVEN a discoping with opened "arp socket":
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT the device to be created
    mock_gmap_expect_createDeviceOrGetKey("00:11:22:ff:ee:dd", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = expected_tags,
        .expected_discovery_source = "neigh",
    }));
    // EXPECT the IP to be added to the device
    mock_gmap_expect_ip_device_get_address("id1", family, ip, NULL);
    mock_gmap_expect_ip_device_add_address("id1", family, ip, expected_address_scope, "reachable", expected_address_source, false, amxd_status_ok);

    // WHEN an IP is reachable according to discoping
    mock_ext_discoping.up_cb(ip, family, "00:11:22:ff:ee:dd", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);
    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_up(UNUSED void** state) {
    s_test_discoping_up("192.168.1.100", AF_INET, "global", "Static");
}

void test_gmap_eth_dev_discoping__ipv6_discoping_up(UNUSED void** state) {
    s_test_discoping_up("2a02:1802:94:43c0::1", AF_INET6, "global", "Static");
}

void test_gmap_eth_dev_discoping__ipv4_discoping_up_autoip(UNUSED void** state) {
    s_test_discoping_up("169.254.42.120", AF_INET, "link", "AutoIP");
}

void test_gmap_eth_dev_discoping__ipv6_discoping_up_link_local(UNUSED void** state) {
    s_test_discoping_up("fe80::a:b:c:100", AF_INET6, "link", "Static");
}

void test_gmap_eth_dev_discoping__ipv4_discoping_up_twice(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    amxc_var_t* address_data = NULL;

    // GIVEN an discoping that already received that an IP is up
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);
    mock_gmap_expect_createDeviceOrGetKey("00:11:22:ff:ee:dd", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4",
        .expected_discovery_source = "neigh",
    }));
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.100", NULL);
    mock_gmap_expect_ip_device_add_address("id1", AF_INET, "192.168.1.100", "global", "reachable", "Static", false, amxd_status_ok);
    mock_ext_discoping.up_cb("192.168.1.100", AF_INET, "00:11:22:ff:ee:dd", mock_ext_discoping.userdata);

    // EXPECT the device to be already exist
    mock_gmap_expect_createDeviceOrGetKey("00:11:22:ff:ee:dd", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4",
        .expected_discovery_source = "neigh",
    }));
    mock_gmap_expect_setTag("id1", "ipv4", NULL, gmap_traverse_this);
    // EXPECT the IP to already exist:
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", "not reachable");
    amxc_var_add_key(bool, address_data, "Reserved", true);
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.100", address_data);
    // EXPECT the IP to be updated
    mock_gmap_expect_ip_device_set_address("id1", AF_INET, "192.168.1.100", NULL, "reachable", NULL, true, amxd_status_ok);

    // WHEN an IP is announced again as reachable:
    mock_ext_discoping.up_cb("192.168.1.100", AF_INET, "00:11:22:ff:ee:dd", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);
    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_down_exist_not_owned(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    amxc_var_t* address_data = NULL;

    // GIVEN an discoping
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT device to be found
    mock_gmap_expect_findByMac("00:11:22:AA:BB:cc", "mykey");
    // EXPECT ip to exist
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", "reachable");
    amxc_var_add_key(cstring_t, address_data, "AddressSource", "DHCP");
    amxc_var_add_key(bool, address_data, "Reserved", true);
    mock_gmap_expect_ip_device_get_address("mykey", AF_INET, "192.168.1.123", address_data);
    // EXPECT ip to be marked as unreachable
    mock_gmap_expect_ip_device_set_address("mykey", AF_INET, "192.168.1.123", NULL,
                                           "not reachable", NULL, true, amxd_status_ok);

    // WHEN an IP goes down
    mock_ext_discoping.down_cb("192.168.1.123", AF_INET, "00:11:22:Aa:bB:cc", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

static void s_test_gmap_eth_dev_discoping__discoping_down_exist_owned(const char* ip, uint32_t family) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    amxc_var_t* address_data = NULL;

    // GIVEN an discoping
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT device to be found
    mock_gmap_expect_findByMac("00:11:22:AA:BB:cc", "mykey");
    // EXPECT ip to exist
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", "reachable");
    amxc_var_add_key(cstring_t, address_data, "AddressSource", "Static");
    amxc_var_add_key(bool, address_data, "Reserved", true);
    mock_gmap_expect_ip_device_get_address("mykey", family, ip, address_data);
    // EXPECT ip to be deleted
    mock_gmap_expect_ip_device_delete_address("mykey", family, ip, amxd_status_ok);

    // WHEN an IP goes down
    mock_ext_discoping.down_cb(ip, family, "00:11:22:Aa:bB:cc", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_down_exist_owned(UNUSED void** state) {
    s_test_gmap_eth_dev_discoping__discoping_down_exist_owned("192.168.1.123", AF_INET);
}

void test_gmap_eth_dev_discoping__ipv6_discoping_down_exist_owned(UNUSED void** state) {
    s_test_gmap_eth_dev_discoping__discoping_down_exist_owned("2a02:1802:94:43c0::1", AF_INET6);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_down_ip_does_not_exist(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT device to be found
    mock_gmap_expect_findByMac("00:11:22:AA:BB:cc", "mykey");
    // EXPECT ip to not exist, and nothing else to happen.
    mock_gmap_expect_ip_device_get_address("mykey", AF_INET, "192.168.1.123", NULL);

    // WHEN an IP goes down
    mock_ext_discoping.down_cb("192.168.1.123", AF_INET, "00:11:22:Aa:bB:cc", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_down_device_does_not_exist(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT device to not be found, and nothing else to happen.
    mock_gmap_expect_findByMac("00:11:22:AA:BB:cc", NULL);

    // WHEN an IP goes down
    mock_ext_discoping.down_cb("192.168.1.123", AF_INET, "00:11:22:Aa:bB:cc", mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

static void s_test_gmap_eth_dev_discoping__discoping_collision(const char* ip, uint32_t family) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    amxc_var_t maclist;
    amxc_var_t* address_data;
    const char* expected_tags = NULL;
    const char* expected_set_tags = NULL;

    amxc_var_init(&maclist);
    amxc_var_set_type(&maclist, AMXC_VAR_ID_LIST);
    switch(family) {
    case AF_INET:
        expected_tags = "lan edev mac physical ipv4";
        expected_set_tags = "ipv4";
        break;
    case AF_INET6:
        expected_tags = "lan edev mac physical ipv6";
        expected_set_tags = "ipv6";
        break;
    default:
        assert_true(false);
        break;
    }

    // GIVEN an discoping
    s_create_bridge_and_discoping_ipv4(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip);

    // EXPECT some devices to exist, some not
    mock_gmap_expect_createDeviceOrGetKey("aA:Bb:00:00:00:01", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = expected_tags,
        .expected_discovery_source = "neigh",
    }));
    mock_gmap_expect_createDeviceOrGetKey("aA:Bb:00:00:00:02", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev2",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = expected_tags,
        .expected_discovery_source = "neigh",
    }));
    mock_gmap_expect_createDeviceOrGetKey("aA:Bb:00:00:00:03", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev3",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = expected_tags,
        .expected_discovery_source = "neigh",
    }));
    mock_gmap_expect_setTag("dev1", expected_set_tags, NULL, gmap_traverse_this);
    mock_gmap_expect_setTag("dev2", expected_set_tags, NULL, gmap_traverse_this);
    // EXPECT some addresses to exist, some not
    // dev1:
    mock_gmap_expect_ip_device_get_address("dev1", family, ip, NULL);
    // dev2:
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", "reachable");
    amxc_var_add_key(bool, address_data, "Reserved", true);
    amxc_var_add_key(bool, address_data, "AddressSource", "DHCP");
    mock_gmap_expect_ip_device_get_address("dev2", family, ip, address_data);
    // dev3:
    mock_gmap_expect_ip_device_get_address("dev3", family, ip, NULL);
    // EXPECT addresses to be created or updated
    mock_gmap_expect_ip_device_add_address("dev1", family, ip, "global", "error", "Static", false, amxd_status_ok);
    mock_gmap_expect_ip_device_set_address("dev2", family, ip, NULL, "error", NULL, true, amxd_status_ok);
    mock_gmap_expect_ip_device_add_address("dev3", family, ip, "global", "error", "Static", false, amxd_status_ok);

    // WHEN a collision happens
    amxc_var_add(cstring_t, &maclist, "aa:BB:00:00:00:01");
    amxc_var_add(cstring_t, &maclist, "aa:BB:00:00:00:02");
    amxc_var_add(cstring_t, &maclist, "aa:BB:00:00:00:03");
    mock_ext_discoping.collision_cb(ip, family, &maclist, mock_ext_discoping.userdata);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
    amxc_var_clean(&maclist);
}

void test_gmap_eth_dev_discoping__ipv4_discoping_collision(UNUSED void** state) {
    s_test_gmap_eth_dev_discoping__discoping_collision("192.168.1.2", AF_INET);
}

void test_gmap_eth_dev_discoping__ipv6_discoping_collision(UNUSED void** state) {
    s_test_gmap_eth_dev_discoping__discoping_collision("2a02:1802:94:43c0::1", AF_INET6);
}
