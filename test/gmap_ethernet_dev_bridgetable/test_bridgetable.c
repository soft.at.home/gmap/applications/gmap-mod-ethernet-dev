/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

 #include <fcntl.h>
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_bridgetable.h"
#include <yajl/yajl_gen.h> // required by amxj/amxj_variant.h
#include "amxj/amxj_variant.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include "amxd/amxd_object.h"
#include <string.h>
#include <amxd/amxd_transaction.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "test_bridgetable.h"
#include "../common/mock_discoping.h"
#include <arpa/inet.h>

#define MAC_DEVICE "11:22:AA:BB:CC:DD"

static gmap_query_t query_linker = {0};
static gmap_query_t query_bridge = {0};
static mock_discoping_t mock_ext_discoping = {0};
static gmap_eth_dev_bridge_collection_t bridges = { 0 };
static gmap_eth_dev_bridge_t bridge = {
    .netdev = (char*) "mybridge",
    .netdev_index = 5,
    .mac = (char*) "60:50:40:30:20:10",
};
static gmap_eth_dev_discoping_t* discoping = NULL;
static gmap_eth_dev_bridgetable_t* test_bridgetable = NULL;

static void s_testhelper_create_bridgetable(uint32_t proto_family) {
    assert_null(discoping);
    discoping = test_util_new_discoping_ext(&mock_ext_discoping, true);
    bridge.discoping = discoping;
    bridge.collection = &bridges;
    bridge.proto_family = proto_family;

    /* bridgetable linker expression */
    expect_string(__wrap_gmap_query_open_ext_2, expression,
                  "self interface && (eth || wifi) && .NetDevIndex != 0 && .PhysAddress ^= \"60:50:40:30:20:10\"");
    will_return(__wrap_gmap_query_open_ext_2, &query_linker);

    gmap_eth_dev_bridgetable_new(&test_bridgetable, &bridge);
    assert_non_null(test_bridgetable);
}

static void s_testhelper_delete_bridgetable() {
    assert_non_null(test_bridgetable);
    assert_non_null(bridge.discoping);
    assert_non_null(bridge.collection);

    expect_value(__wrap_gmap_query_close, query, &query_linker);
    gmap_eth_dev_bridgetable_delete(&test_bridgetable);
    gmap_eth_dev_discoping_delete(&discoping);

    bridge.collection = NULL;
    bridge.discoping = NULL;
    discoping = NULL;
    test_bridgetable = NULL;
    mock_ext_discoping = (mock_discoping_t) {};
    query_linker = (gmap_query_t) {};
    query_bridge = (gmap_query_t) {};
}

int test_setup_bridgetable(void** state) {
    test_setup_without_component_init(state);
    assert_null(test_bridgetable);
    assert_null(bridge.discoping);
    assert_null(bridge.collection);
    s_testhelper_create_bridgetable(AF_INET);
    return 0;
}

int test_teardown_bridgetable(void** state) {
    s_testhelper_delete_bridgetable();
    return test_teardown(state);
}

static void s_testhelper_expect_createIfActive(bool device_already_exists, bool active) {
    mock_gmap_expect_createIfActive(MAC_DEVICE, (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = device_already_exists,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), active);
}

static void s_expect_tag_set(const char* key, const char* tag) {
    expect_string(__wrap_gmap_device_setTag, key, key);
    expect_string(__wrap_gmap_device_setTag, tags, tag);
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__nonexist_inactive(UNUSED void** state) {
    // GIVEN a bridgetable
    gmap_eth_dev_bridgetable_t* bridgetable = test_bridgetable;
    assert_non_null(bridgetable);

    // EXPECT device does not exist yet, is *not* created, and no links set
    // (we don't create devices for inactive, to avoid re-creating them after they are manually deleted)
    s_testhelper_expect_createIfActive(false, false);

    // WHEN the device is updated but not activated
    gmap_eth_dev_bridgetable_create_or_update_device(bridgetable, MAC_DEVICE, false, 123);
}

static void s_testhelper_create_or_update_device__nonexist_active(const char* port_bssid, const char* expect_link_type, const char* expected_tag) {
    // GIVEN bridgetable and wifi port
    amxc_var_t* port = test_util_create_port("myPort", port_bssid, "Device.Ethernet.Interface.100.");
    assert_non_null(test_bridgetable);

    // EXPECT device does not exist yet and is created because considered active
    s_testhelper_expect_createIfActive(false, true);
    // EXPECT port found
    test_util_expect_search_port("myPort", port);
    // EXPECT device linked to port
    mock_gmap_expect_devices_linkReplace("myPort", "id1", expect_link_type, "mod-eth-dev");
    // EXPECT values updated to be consistent with link
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myPort",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.100."
    }));
    // EXPECT tag set
    s_expect_tag_set("id1", expected_tag);
    // EXPECT set active:
    mock_gmap_expect_setActive("id1", true, true);

    // WHEN the device is created and activated
    gmap_eth_dev_bridgetable_create_or_update_device(test_bridgetable, MAC_DEVICE, true, 123);

    // cleanup
    amxc_var_delete(&port);
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__nonexist_active_wifi(UNUSED void** state) {
    s_testhelper_create_or_update_device__nonexist_active("", "wifi", "wifi");
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__nonexist_active_eth(UNUSED void** state) {
    s_testhelper_create_or_update_device__nonexist_active(NULL, "ethernet", "eth");
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__nonexist_active_portnonexist(UNUSED void** state) {
    // GIVEN a bridgetable and no port
    assert_non_null(test_bridgetable);

    // EXPECT device does not exist yet and is created because considered active
    s_testhelper_expect_createIfActive(false, true);
    // EXPECT no port found
    test_util_expect_search_port(NULL, NULL);
    // EXPECT no link set
    // EXPECT no values updated to be consistent with link
    // EXPECT no "wifi" tag set
    // EXPECT set active
    mock_gmap_expect_setActive("id1", true, true);

    // WHEN the device is created and activated
    gmap_eth_dev_bridgetable_create_or_update_device(test_bridgetable, MAC_DEVICE, true, 123);
}

static void s_expect_cleartag_for_inactive(const char* key) {
    expect_string(__wrap_gmap_device_clearTag, key, key);
    expect_string(__wrap_gmap_device_clearTag, tags, "wifi eth");
    expect_value(__wrap_gmap_device_clearTag, expression, NULL);
    expect_value(__wrap_gmap_device_clearTag, mode, gmap_traverse_this);
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__becomes_inactive(UNUSED void** state) {
    // GIVEN a bridgetable and ethernet port fields
    amxc_var_t* eth_port = test_util_create_port("myport", NULL, "Device.Ethernet.Interface.200.");
    assert_non_null(test_bridgetable);

    // EXPECT device not attempted to be created because inactive, but happened to already exist
    s_testhelper_expect_createIfActive(true, false);
    // EXPECT the device's is set as inactive
    mock_gmap_expect_setActive("id1", false, true);
    // EXPECT eth tag to be removed
    s_expect_cleartag_for_inactive("id1");

    // WHEN calling create_or_update
    gmap_eth_dev_bridgetable_create_or_update_device(test_bridgetable, MAC_DEVICE, false, 123);

    // cleanup
    amxc_var_delete(&eth_port);
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__becomes_active_eth(UNUSED void** state) {
    // GIVEN an ethernet port
    amxc_var_t* eth_port = test_util_create_port("myport", NULL, "Device.Ethernet.Interface.300.");
    assert_non_null(test_bridgetable);

    // EXPECT device be attempted to be created (because active), and to already exist
    s_testhelper_expect_createIfActive(true, true);
    // EXPECT port to be found
    test_util_expect_search_port("myport", eth_port);
    // EXPECT the device to be linked to the port in gmap
    mock_gmap_expect_devices_linkReplace("myport", "id1", "ethernet", "mod-eth-dev");
    // EXPECT active to be set to true
    mock_gmap_expect_setActive("id1", true, true);
    // EXPECT "eth" tag to be added
    expect_string(__wrap_gmap_device_setTag, key, "id1");
    expect_string(__wrap_gmap_device_setTag, tags, "eth");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // EXPECT fields to be updated to being linked
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.300."
    }));
    mock_gmap_ip_device_get_addresses("id1", "gmap-mockdata-empty-addresses.json");

    // WHEN notifying that an (existing) device on that port is active
    gmap_eth_dev_bridgetable_create_or_update_device(test_bridgetable, MAC_DEVICE, true, 123);

    // cleanup
    amxc_var_delete(&eth_port);
}

void test_gmap_eth_dev_bridgetable_create_or_update_device__becomes_active_wifi(UNUSED void** state) {
    // GIVEN a wifi port
    amxc_var_t* wifi_port = test_util_create_port("myport", "", "Device.Ethernet.Interface.400.");
    assert_non_null(test_bridgetable);

    // EXPECT device be attempted to be created (because active), and to already exist
    s_testhelper_expect_createIfActive(true, true);
    // EXPECT port to be found
    test_util_expect_search_port("myport", wifi_port);
    // EXPECT the device to be linked to the port in gmap
    mock_gmap_expect_devices_linkReplace("myport", "id1", "wifi", "mod-eth-dev");
    // EXPECT device to be set as active
    mock_gmap_expect_setActive("id1", true, true);
    // EXPECT "wifi" tag to be added
    expect_string(__wrap_gmap_device_setTag, key, "id1");
    expect_string(__wrap_gmap_device_setTag, tags, "wifi");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // EXPECT fields to be updated to being linked
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.400."
    }));
    mock_gmap_ip_device_get_addresses("id1", "gmap-mockdata-empty-addresses.json");

    // WHEN notifying that an (existing) device on that port is active
    gmap_eth_dev_bridgetable_create_or_update_device(test_bridgetable, MAC_DEVICE, true, 123);

    // cleanup
    amxc_var_delete(&wifi_port);
}

void test_gmap_eth_dev_bridgetable_index_of_path(UNUSED void** state) {
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path(NULL));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path(""));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("nonsense"));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("NetDev."));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("NetDev.Something.Else.4"));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("NetDev.Something.Link.4."));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("NetDev.Something.Link.4."));
    assert_int_equal(0, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.four.5."));
    assert_int_equal(5, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.5."));
    assert_int_equal(5, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.5.Somehting"));
    assert_int_equal(5, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.5.Somehting."));
    assert_int_equal(5, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.5.6"));
    assert_int_equal(5, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.5.6."));
    assert_int_equal(100, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.100.Link.6."));
    assert_int_equal(200, gmap_eth_dev_bridgetable_index_of_path("NetDev.Link.200.BridgeTable.1.MACAddress"));
}

void test_gmap_eth_dev_bridgetable_macs_for_port__no_netdev(UNUSED void** state) {
    amxc_htable_t* macs = NULL;

    // GIVEN no netdev
    assert_null(amxd_dm_findf(get_dummy_dm(), "NetDev."));

    // WHEN asking the macs for a port
    macs = gmap_eth_dev_bridgetable_macs_for_port(1);

    // THEN retrieving macs failed
    assert_true(macs == NULL || amxc_htable_size(macs) == 0);

    amxc_htable_delete(&macs, variant_htable_it_free);
}

void test_gmap_eth_dev_bridgetable_macs_for_port__no_port(UNUSED void** state) {
    amxc_htable_t* macs = NULL;
    s_testhelper_create_bridgetable(AF_INET);

    // GIVEN a port does not exist in netdev (but other ports do)
    mock_gmap_expect_createIfActive("00:00:00:00:00:01", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    mock_gmap_expect_createIfActive("00:00:00:00:00:02", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id2",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    mock_gmap_expect_setActive("id1", true, true);
    mock_gmap_expect_setActive("id2", true, true);
    test_util_expect_search_port(NULL, NULL);
    test_util_expect_search_port(NULL, NULL);
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata.odl");
    test_util_handle_events();

    // non-existing port:
    assert_null(amxd_dm_findf(get_dummy_dm(), "NetDev.Link.2."));
    // existing port:
    assert_non_null(amxd_dm_findf(get_dummy_dm(), "NetDev.Link.10."));

    // WHEN asking the macs for a port that does not exist
    macs = gmap_eth_dev_bridgetable_macs_for_port(2);

    // THEN retrieving macs failed
    assert_true(macs == NULL || amxc_htable_size(macs) == 0);

    // cleanup:
    amxc_htable_delete(&macs, variant_htable_it_free);
    s_testhelper_delete_bridgetable();
}

static uint32_t s_htable_get_uint32(amxc_htable_t* htable, const char* key) {
    amxc_htable_it_t* it = NULL;
    amxc_var_t* value = NULL;
    assert_non_null(htable);
    assert_non_null(key);
    it = amxc_htable_get(htable, key);
    assert_non_null(it);
    value = amxc_htable_it_get_data(it, amxc_var_t, hit);
    assert_non_null(value);
    return amxc_var_constcast(uint32_t, value);
}

void test_gmap_eth_dev_bridgetable_macs_for_port__ok(UNUSED void** state) {
    amxc_htable_t* macs = NULL;

    // GIVEN a port in NetDev
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata.odl");

    // WHEN asking the macs for a port
    macs = gmap_eth_dev_bridgetable_macs_for_port(123);

    // THEN the MACS from NetDev are retrieived
    assert_non_null(macs);
    assert_int_equal(2, amxc_htable_size(macs));
    assert_int_equal(123, s_htable_get_uint32(macs, "00:00:00:00:00:01"));
    assert_int_equal(123, s_htable_get_uint32(macs, "00:00:00:00:00:02"));

    // cleanup:
    amxc_htable_delete(&macs, variant_htable_it_free);
}

void test_gmap_eth_dev_bridgetable_scenario_macs_at_startup(void** state) {
    // GIVEN a port in NetDev and in gmap
    amxc_var_t* eth2_gmap = test_util_create_port("gmap-eth2", NULL, "Device.Ethernet.Interface.500.");
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata.odl");
    test_util_handle_events();

    // EXPECT the two devices that exist in NetDev are created in gMap
    mock_gmap_expect_createIfActive("00:00:00:00:00:01", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    mock_gmap_expect_createIfActive("00:00:00:00:00:02", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id2",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    // EXPECT the devices to be linked
    // first device:
    test_util_expect_search_port("gmap-eth2", eth2_gmap);
    s_expect_tag_set("id1", "eth");
    mock_gmap_expect_devices_linkReplace("gmap-eth2", "id1", "ethernet", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "gmap-eth2",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.500."
    }));
    mock_gmap_expect_setActive("id1", true, true);
    // second device:
    test_util_expect_search_port("gmap-eth2", eth2_gmap);
    s_expect_tag_set("id2", "eth");
    mock_gmap_expect_devices_linkReplace("gmap-eth2", "id2", "ethernet", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "id2");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "gmap-eth2",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.500."
    }));
    mock_gmap_expect_setActive("id2", true, true);
    mock_gmap_ip_device_get_addresses("id2", "gmap-mockdata-empty-addresses.json");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    s_testhelper_create_bridgetable(AF_INET);
    test_util_handle_events();

    // cleanup:
    amxc_var_delete(&eth2_gmap);
    s_testhelper_delete_bridgetable();
}

void test_gmap_eth_dev_bridgetable_scenario_device_appears_in_netdev(UNUSED void** state) {
    amxc_var_t* myport_gmap = NULL;

    s_testhelper_create_bridgetable(AF_INET);

    // GIVEN an empty netdev port and a gmap port
    myport_gmap = test_util_create_port("myport-gmap", "", "Device.Ethernet.Interface.600.");
    test_util_parse_odl("netdev-dm.odl");
    test_util_handle_events();
    test_util_parse_odl("netdev-mockdata-one-port.odl");
    test_util_handle_events();

    // EXPECT the device to be created in gMap
    mock_gmap_expect_createIfActive("33:33:33:33:33:33", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id3",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    // EXPECT the device to be linked
    test_util_expect_search_port("myport-gmap", myport_gmap);
    s_expect_tag_set("id3", "wifi");
    mock_gmap_expect_devices_linkReplace("myport-gmap", "id3", "wifi", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "id3");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport-gmap",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.600."
    }));
    mock_gmap_expect_setActive("id3", true, true);

    // WHEN the device appears in netdev
    test_util_parse_odl("netdev-mockdata-one-dev.odl");
    test_util_handle_events();

    // cleanup:
    amxc_var_delete(&myport_gmap);
    s_testhelper_delete_bridgetable();
}



static void s_test_gmap_eth_dev_bridgetable_add_entry_triggers_discoping(uint32_t protocol_family) {
    amxc_var_t* myport_gmap = NULL;

    s_testhelper_create_bridgetable(protocol_family);

    // GIVEN an empty netdev port and a gmap port
    myport_gmap = test_util_create_port("myport-gmap", "", "Device.Ethernet.Interface.600.");
    test_util_parse_odl("netdev-dm.odl");
    test_util_handle_events();
    test_util_parse_odl("netdev-mockdata-one-port.odl");
    test_util_handle_events();
    mock_gmap_expect_createIfActive("33:33:33:33:33:33", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id4",
        .will_return_already_exists = true, // <-- device exists
        .will_return_success = true,
        .expected_tags = "lan edev mac physical",
    }), true);
    test_util_expect_search_port("myport-gmap", myport_gmap);
    s_expect_tag_set("id4", "wifi");
    mock_gmap_expect_devices_linkReplace("myport-gmap", "id4", "wifi", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "id4");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport-gmap",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.600."
    }));
    mock_gmap_expect_setActive("id4", true, true);

    // EXPECT that the list of IP addressed will be fetched
    mock_gmap_ip_device_get_addresses("id4", "gmap-mockdata-addresses.json");

    // EXPECT that the unreachable IPs will be verified (and the reachable ones not)
    // Note: only the IPs of the applicable protocol family (ipv4 or ipv6) must be verified.
    //       Otherwise the lib_discoping instance that only has a socket of the other ip family
    //       won't see this IP address online, and report it incorrectly as being offline.
    if(protocol_family == AF_INET) {
        mock_discoping_expect_verify("192.168.1.10", "33:33:33:33:33:33");
        mock_discoping_expect_verify("192.168.1.12", "33:33:33:33:33:33");
    } else if(protocol_family == AF_INET6) {
        mock_discoping_expect_verify("fe80::211:11ff:fe01:2402", "33:33:33:33:33:33");
    } else {
        fail();
    }


    // WHEN the device appears in netdev
    test_util_parse_odl("netdev-mockdata-one-dev.odl");
    test_util_handle_events();

    // cleanup:
    amxc_var_delete(&myport_gmap);
    s_testhelper_delete_bridgetable();
}

void test_gmap_eth_dev_bridgetable_add_entry_triggers_discoping_ipv4(UNUSED void** state) {
    s_test_gmap_eth_dev_bridgetable_add_entry_triggers_discoping(AF_INET);
}

void test_gmap_eth_dev_bridgetable_add_entry_triggers_discoping_ipv6(UNUSED void** state) {
    s_test_gmap_eth_dev_bridgetable_add_entry_triggers_discoping(AF_INET);
}