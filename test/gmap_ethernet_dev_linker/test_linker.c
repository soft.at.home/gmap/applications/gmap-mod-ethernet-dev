/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

 #include <fcntl.h>
#include "gmap_eth_dev_linker.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include <string.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "test_linker.h"
#include "../common/mock_gmap.h"

#define NETDEV_NAME "mybridge"
#define NETDEV_MAC "60:50:40:30:20:10"

static gmap_eth_dev_linker_t* s_create_linker(gmap_query_t* query, gmap_eth_dev_bridge_t* bridge) {
    gmap_eth_dev_linker_t* linker = NULL;
    expect_string(__wrap_gmap_query_open_ext_2, expression, "self interface && (eth || wifi) && .NetDevIndex != 0 && .PhysAddress ^= \"" NETDEV_MAC "\"");
    will_return(__wrap_gmap_query_open_ext_2, query);
    gmap_eth_dev_linker_new(&linker, bridge);
    return linker;
}

void test_gmap_eth_dev_linker_link_eth_port_exists(UNUSED void** state) {
    // GIVEN a linker, a device, and a port
    const char* device_key = "mydev";
    amxc_var_t* port = test_util_create_port("myport", NULL, "Device.Ethernet.Interface.700.");
    amxc_var_add_key(uint32_t, port, "NetDevIndex", 1);
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 5,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);

    // EXPECT the device to be linked to the port in gmap
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, "mydev");
    expect_string(__wrap_gmap_device_setTag, tags, "eth");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // set active:
    mock_gmap_expect_setActive(device_key, true, true);
    // link:
    mock_gmap_expect_devices_linkReplace("myport", device_key, "ethernet", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "mydev");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.700."
    }));
    // WHEN linking the device with the port
    gmap_eth_dev_linker_link(linker, port, 1, device_key);

    amxc_var_delete(&port);
    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
}

void test_gmap_eth_dev_linker_link_wifi_port_exists(UNUSED void** state) {
    // GIVEN a linker, a device, and a port
    const char* device_key = "mydev";
    amxc_var_t* port = test_util_create_port("myport", "11:22:33:44:55:66", "Device.WiFi.SSID.1.");
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);

    // EXPECT the device to be linked to the port in gmap
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, "mydev");
    expect_string(__wrap_gmap_device_setTag, tags, "wifi");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // set active:
    mock_gmap_expect_setActive(device_key, true, true);
    // link:
    mock_gmap_expect_devices_linkReplace("myport", device_key, "wifi", "mod-eth-dev");
    expect_string(__wrap_gmap_device_set, key, "mydev");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.WiFi.SSID.1."
    }));
    // WHEN linking the device with the port
    gmap_eth_dev_linker_link(linker, port, 1, device_key);

    amxc_var_delete(&port);
    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
}

void test_gmap_eth_dev_linker_link__port_exists_and_not_cached(UNUSED void** state) {
    // GIVEN a linker, a device, and a port
    const char* device_key = "mydev";
    amxc_var_t* port = test_util_create_port("myport", NULL, "Device.Ethernet.Interface.800.");
    amxc_var_add_key(uint32_t, port, "NetDevIndex", 123);
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);

    // EXPECT the device to be linked to the port in gmap
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, "mydev");
    expect_string(__wrap_gmap_device_setTag, tags, "eth");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // set active:
    mock_gmap_expect_setActive("mydev", true, true);
    // find port name to link:
    test_util_expect_search_port("myport", port);
    // link:
    mock_gmap_expect_devices_linkReplace("myport", device_key, "ethernet", "mod-eth-dev");
    // update fields:
    expect_string(__wrap_gmap_device_set, key, "mydev");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.800."
    }));

    // WHEN linking the device with the port, but without passing a cached port
    gmap_eth_dev_linker_link(linker, NULL, 123, device_key);

    amxc_var_delete(&port);
    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
}

void test_gmap_eth_dev_linker_link__before_and_after_port_appears(UNUSED void** state) {
    amxc_var_t* empty_list_of_ports;

    // GIVEN a linker, a device, and no ports in gmap, but we already have its netdevidx
    const char* device_key = "mydev";
    amxc_var_t* gmap_port = NULL;
    uint32_t port_netdev_idx = 123;
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);

    // EXPECT port to not be found
    amxc_var_new(&empty_list_of_ports);
    amxc_var_set_type(empty_list_of_ports, AMXC_VAR_ID_LIST);
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 123");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, empty_list_of_ports);
    // EXPECT linking happens regarless of port not being found
    mock_gmap_expect_setActive("mydev", true, true);

    // WHEN linking the device with the non-existing port
    gmap_eth_dev_linker_link(linker, gmap_port, port_netdev_idx, device_key);

    // THEN nothing happens at this point (i.e. no call to gmap)

    // EXPECT the device to be linked to the port in gmap
    // link:
    mock_gmap_expect_devices_linkReplace("latePort", device_key, "wifi", "mod-eth-dev");
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, "mydev");
    expect_string(__wrap_gmap_device_setTag, tags, "wifi");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // update fields:
    expect_string(__wrap_gmap_device_set, key, "mydev");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "latePort",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.900."
    }));
    // set active:
    mock_gmap_expect_setActive("mydev", true, true);

    // WHEN the port appears and linking again
    gmap_port = test_util_create_port("latePort", "", "Device.Ethernet.Interface.900.");
    gmap_eth_dev_linker_link(linker, gmap_port, port_netdev_idx, device_key);

    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
    amxc_var_delete(&gmap_port);
}

void test_gmap_eth_dev_linker_link__automatic_late_link(UNUSED void** state) {
    amxc_var_t* empty_list_of_ports;

    // GIVEN a linker, a device, and no ports in gmap, but we already have its netdevidx
    const char* device_key = "mydev";
    amxc_var_t* gmap_port = NULL;
    uint32_t port_netdev_idx = 2;
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);

    // EXPECT no port to be found
    amxc_var_new(&empty_list_of_ports);
    amxc_var_set_type(empty_list_of_ports, AMXC_VAR_ID_LIST);
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 2");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, empty_list_of_ports);
    // EXPECT linking happens regarless of port not being found
    mock_gmap_expect_setActive("mydev", true, true);


    // WHEN linking the device with the non-existing port
    gmap_eth_dev_linker_link(linker, gmap_port, port_netdev_idx, device_key);

    // THEN nothing happens at this point (i.e. no call to link in gmap)

    // EXPECT the device to be linked to the port in gmap
    // find:
    mock_gmap_expect_findByMac("66:55:44:33:22:11", "mydev");
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, "mydev");
    expect_string(__wrap_gmap_device_setTag, tags, "wifi");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // link:
    mock_gmap_expect_devices_linkReplace("eth2", device_key, "wifi", "mod-eth-dev");
    // set active:
    mock_gmap_expect_setActive("mydev", true, true);
    // update fields:
    expect_string(__wrap_gmap_device_set, key, "mydev");
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "eth2",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.1000."
    }));

    // WHEN gmap reports that the port exists now and it also exists in netdev
    // netdev:
    assert_int_equal(0, amxo_parser_parse_file(get_dummy_parser(), "../common/dummy-netdev.odl", amxd_dm_get_root(get_dummy_dm())));
    test_util_handle_events();
    assert_int_equal(0, amxo_parser_parse_file(get_dummy_parser(), "../common/data/netdev-eth2.odl", amxd_dm_get_root(get_dummy_dm())));
    test_util_handle_events();
    // gmap:
    gmap_port = test_util_create_port("eth2", "11:22:33:44:55:66", "Device.Ethernet.Interface.1000.");
    amxc_var_add_key(uint32_t, gmap_port, "NetDevIndex", port_netdev_idx);
    query.fn(&query, "eth2", gmap_port, gmap_query_expression_start_matching);

    // cleanup:
    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
    amxc_var_delete(&gmap_port);
}

void test_gmap_eth_dev_linker_make_inactive__pending(UNUSED void** state) {
    amxc_var_t* empty_list_of_ports;

    // GIVEN a linker, no gmap port, and a linker with a pending link
    const char* device_key = "mydev";
    amxc_var_t* gmap_port = NULL;
    uint32_t port_netdev_idx = 123;
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);
    // `gmap_eth_dev_linker_link` will not find the port:
    amxc_var_new(&empty_list_of_ports);
    amxc_var_set_type(empty_list_of_ports, AMXC_VAR_ID_LIST);
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 123");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, empty_list_of_ports);
    mock_gmap_expect_setActive("mydev", true, true);
    gmap_eth_dev_linker_link(linker, NULL, port_netdev_idx, device_key);

    // EXPECT eth/wifi tag to be removed
    expect_string(__wrap_gmap_device_clearTag, key, device_key);
    expect_string(__wrap_gmap_device_clearTag, tags, "wifi eth");
    expect_value(__wrap_gmap_device_clearTag, expression, NULL);
    expect_value(__wrap_gmap_device_clearTag, mode, gmap_traverse_this);

    // EXPECT the device to be set as inactive in gmap
    mock_gmap_expect_setActive(device_key, false, true);

    // WHEN marking the device as inactive
    gmap_eth_dev_linker_make_inactive(linker, port_netdev_idx, device_key);

    // THEN no link is made when gmap reports that the port exists now
    gmap_port = test_util_create_port("portAppearingLateInGmap", "11:22:33:44:55:66", "Device.Ethernet.Interface.1100.");
    amxc_var_add_key(uint32_t, gmap_port, "NetDevIndex", port_netdev_idx);
    query.fn(&query, "portAppearingLateInGmap", gmap_port, gmap_query_expression_start_matching);

    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
    amxc_var_delete(&gmap_port);
}

void test_gmap_eth_dev_linker_make_inactive__was_linked(UNUSED void** state) {
    // GIVEN a linker, a linked gmap port, and a linker
    const char* device_key = "mydevForUnlinking";
    amxc_var_t* gmap_port = test_util_create_port("myport", NULL, "Device.Ethernet.Interface.1200.");
    uint32_t port_netdev_idx = 123;
    gmap_query_t query = {0};
    gmap_eth_dev_bridge_t bridge = {
        .netdev = (char*) NETDEV_NAME,
        .netdev_index = 1,
        .mac = (char*) NETDEV_MAC,
    };
    gmap_eth_dev_linker_t* linker = s_create_linker(&query, &bridge);
    // set tag:
    expect_string(__wrap_gmap_device_setTag, key, device_key);
    expect_string(__wrap_gmap_device_setTag, tags, "eth");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // link:
    mock_gmap_expect_devices_linkReplace("myport", device_key, "ethernet", "mod-eth-dev");
    // update fields:
    expect_string(__wrap_gmap_device_set, key, device_key);
    expect_check(__wrap_gmap_device_set, values, test_util_check_fields, (&(test_util_simple_fields_t) {
        .key1 = "InterfaceName", .value1string = "myport",
        .key2 = "Layer2Interface", .value2string = "someNetDevName",
        .key3 = "Layer1Interface", .value3string = "Device.Ethernet.Interface.1200."
    }));
    // set active:
    mock_gmap_expect_setActive("mydevForUnlinking", true, true);
    // actually link:
    gmap_eth_dev_linker_link(linker, gmap_port, port_netdev_idx, device_key);

    // EXPECT a gmap unlink to not happen
    // EXPECT the device to be set inactive
    mock_gmap_expect_setActive("mydevForUnlinking", false, true);
    // remove eth/wifi flag:
    expect_string(__wrap_gmap_device_clearTag, key, "mydevForUnlinking");
    expect_string(__wrap_gmap_device_clearTag, tags, "wifi eth");
    expect_value(__wrap_gmap_device_clearTag, expression, NULL);
    expect_value(__wrap_gmap_device_clearTag, mode, gmap_traverse_this);

    // WHEN marking the connection as inactive
    gmap_eth_dev_linker_make_inactive(linker, port_netdev_idx, device_key);

    // cleanup:
    expect_value(__wrap_gmap_query_close, query, &query);
    gmap_eth_dev_linker_delete(&linker);
    amxc_var_delete(&gmap_port);
}

void test_gmap_eth_dev_taglist_contains_tag(UNUSED void** state) {
    // Normal cases:
    assert_true(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac eth interface", "eth"));
    assert_true(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi interface", "wifi"));

    // Case: no other tags
    assert_true(gmap_eth_dev_util_taglist_contains_tag("eth", "eth"));
    assert_true(gmap_eth_dev_util_taglist_contains_tag("wifi", "wifi"));

    // Case: no tags
    assert_false(gmap_eth_dev_util_taglist_contains_tag("", "eth"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag(NULL, "eth"));

    // Case: tag substring of other tag
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac eth_wifi interface", "eth"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac eth_wifi interface", "wifi"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth interface", "eth"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth interface", "wifi"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth eth_wifi wifi wifi_eth eth_wifi ", "eth"));
    assert_true(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth eth_wifi wifi wifi_eth eth_wifi ", "wifi"));
    assert_true(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth eth_wifi eth wifi_eth eth_wifi", "eth"));
    assert_false(gmap_eth_dev_util_taglist_contains_tag("self lan protected mac wifi_eth eth_wifi eth wifi_eth eth_wifi", "wifi"));
}