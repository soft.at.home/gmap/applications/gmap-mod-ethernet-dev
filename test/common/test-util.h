/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__GMAP_ETH_DEV_TEST_UTIL_H__)
#define __GMAP_ETH_DEV_TEST_UTIL_H__

#include <amxc/amxc.h>

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include "gmap_eth_dev_bridge.h"
#include "gmap_eth_dev_bridge_priv.h"
#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev_dhcp_reader.h"
#include "gmap_eth_dev_neighbors.h"
#include "mock_discoping.h"
#include <gmap/gmap_query.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    const char* key1;
    const char* value1string;
    const char* key2;
    const char* value2string;
    const char* key3;
    const char* value3string;
} test_util_simple_fields_t;

void test_util_handle_events();
amxc_var_t* test_util_read_json_from_file(const char* fname);
amxc_var_t* test_util_create_port(const char* port_key, const char* bssid, const char* layer1interface);
void test_util_expect_search_port(const char* port_name, amxc_var_t* port);
void test_util_parse_odl(const char* odl_file_name);

/**
 * Checks if value in @ref amxc_var_t is as expected in a @ref expect_check
 *
 * @implements @ref CheckParameterValue
 *
 * @param value: of type @ref amxc_var_t
 * @param check_value_data: of type @ref test_util_simple_fields_t
 */
int test_util_check_fields(const LargestIntegralType value,
                           const LargestIntegralType check_value_data);

/**
 * Cmocka matcher for string comparison ignoring case
 *
 * Usage example:
 * ```
 * expect_check(__wrap_is_valid_month_name, month, test_util_equals_ignorecase, "JaNuArY");
 * ```
 */
int test_util_equals_ignorecase(LargestIntegralType fun_arg_untyped,
                                LargestIntegralType expected_value_untyped);

/** Helper function for cmocka validation functions */
int test_util_hasExpectedField(const char* field_name, LargestIntegralType fun_arg_untyped,
                               LargestIntegralType expected_value_untyped);

/** cmocka validation function for "values" argument of @ref __wrap_gmap_device_set */
int test_util_has_expected_dhcpv4client(LargestIntegralType fun_arg_untyped,
                                        LargestIntegralType expected_dhcpv4client_untyped);

gmap_eth_dev_discoping_t* test_util_new_discoping_ext(mock_discoping_t* mock_ext_discoping, bool has_mac);

gmap_eth_dev_discoping_t* test_util_new_discoping(void);

#ifdef __cplusplus
}
#endif

#endif
