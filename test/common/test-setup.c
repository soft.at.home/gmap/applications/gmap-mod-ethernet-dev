/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>

#include <setjmp.h>
#include <cmocka.h>
#include <debug/sahtrace.h>

#include "dummy_backend.h"
#include "dummy.h"
#include "test-setup.h"
#include "test-util.h"
#include "gmap_eth_dev.h"
#include "mock_discoping.h"
#include "mock_gmap.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
mock_discoping_t mock_ext_discoping;
gmap_query_t eth_dev_bridge_query = {0};

amxd_dm_t* get_dummy_dm(void) {
    return &dm;
}

amxo_parser_t* get_dummy_parser(void) {
    return &parser;
}

amxb_bus_ctx_t* get_dummy_bus_ctx(void) {
    return bus_ctx;
}

int test_setup_without_component_init(UNUSED void** main) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    amxp_sigmngr_add_signal(NULL, "wait:done");

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read,
                        "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);

    _dummy_main(0, &dm, &parser);

    test_util_handle_events();

    return 0;
}

void test_setup_component_init(UNUSED void** state) {
    expect_any_always(__wrap_amxb_be_who_has, object_path);
    will_return_always(__wrap_amxb_be_who_has, get_dummy_bus_ctx());
    expect_any(__wrap_gmap_query_open_ext_2, expression);
    will_return(__wrap_gmap_query_open_ext_2, &eth_dev_bridge_query);
    expect_function_call(__wrap_netmodel_initialize);
    expect_string(__wrap_amxb_wait_for_object, object_path, "DHCPv4Server.");

    assert_int_equal(0, gmap_eth_dev_init(get_dummy_dm(), get_dummy_parser()));

    test_util_handle_events();
}

int test_setup(void** state) {
    test_setup_without_component_init(state);
    test_setup_component_init(state);

    return 0;
}

int test_teardown(UNUSED void** state) {
    expect_function_call(__wrap_netmodel_cleanup);
    gmap_eth_dev_cleanup();
    test_util_handle_events();
    amxd_dm_clean(&dm);
    amxo_parser_clean(&parser);
    test_unregister_dummy_be();
    sahTraceClose();

    bus_ctx = NULL;

    return 0;
}

int test_teardown_query(UNUSED void** state) {
    expect_any(__wrap_gmap_query_close, query);
    test_teardown(state);

    bus_ctx = NULL;

    return 0;
}
