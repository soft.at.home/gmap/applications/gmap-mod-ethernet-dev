/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test-util.h"

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>

#include <stdarg.h>
#include <setjmp.h>
#include <unistd.h>
#include <cmocka.h>
#include "gmap_eth_dev_bridgetable.h"
#include "test-setup.h"
#include <string.h>
#include <gmap/gmap_devices_flags.h>
#include <gmap/gmap_query.h>
#include "mock_discoping.h"
#include <gmap/gmap_main.h>
#include "mock_gmap.h"

void test_util_handle_events() {
    while(amxp_signal_read() == 0) {
    }
}

/**
 * Read a json filename into a variant.
 *
 * This funciton is fail-fast (no silent failures, stops test on failure)
 */
amxc_var_t* test_util_read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;

    // create a json reader
    if(amxj_reader_new(&reader) != 0) {
        fail_msg("Failed to create json file reader");
    }

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        fail_msg("File open file %s - error 0x%8.8X", fname, errno);
    }

    // read the json file and parse the json text
    amxj_read(reader, fd);

    // get the variant
    data = amxj_reader_result(reader);

    // delete the reader and close the file
    amxj_reader_delete(&reader);
    close(fd);

    if(data == NULL) {
        fail_msg("Invalid JSON in file %s", fname);
    }

    return data;
}


amxc_var_t* test_util_create_port(const char* port_key, const char* bssid, const char* layer1interface) {
    amxc_var_t* port;
    amxc_var_new(&port);
    amxc_var_set_type(port, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, port, "Key", port_key);
    amxc_var_add_key(cstring_t, port, "NetDevName", "someNetDevName");
    amxc_var_add_key(cstring_t, port, "Layer1Interface", layer1interface);
    if(bssid) {
        amxc_var_add_key(cstring_t, port, "BSSID", bssid);
        amxc_var_add_key(ssv_string_t, port, "Tags", "self lan protected mac wifi interface");
    } else {
        amxc_var_add_key(ssv_string_t, port, "Tags", "self lan protected mac eth interface");
    }
    return port;
}

/**
 *
 * @param port NULL if port will not be found.
 */
void test_util_expect_search_port(const char* port_key, amxc_var_t* port) {
    assert_true((port_key == NULL) == (port == NULL));

    amxc_var_t* list_of_ports = NULL;
    amxc_var_new(&list_of_ports);
    // find:
    amxc_var_set_type(list_of_ports, AMXC_VAR_ID_LIST);
    if(port_key != NULL) {
        amxc_var_add(cstring_t, list_of_ports, port_key);
    }
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 123");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, list_of_ports);
    // get port:
    if(port_key != NULL) {
        expect_string(__wrap_gmap_device_get, key, port_key);
        will_return(__wrap_gmap_device_get, port);
    }
}

void test_util_parse_odl(const char* odl_file_name) {
    amxd_object_t* root_obj = amxd_dm_get_root(get_dummy_dm());
    if(0 != amxo_parser_parse_file(get_dummy_parser(), odl_file_name, root_obj)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&get_dummy_parser()->msg, 0));
    }
}

int test_util_check_fields(const LargestIntegralType value,
                           const LargestIntegralType check_value_data) {
    amxc_var_t* values = (void*) value;
    test_util_simple_fields_t* expected_values = (void*) check_value_data;

    const char* actual_value1 = GET_CHAR(values, expected_values->key1);
    const char* actual_value2 = GET_CHAR(values, expected_values->key2);
    const char* actual_value3 = GET_CHAR(values, expected_values->key3);
    if((0 == strcmp(actual_value1, expected_values->value1string))
       && (0 == strcmp(actual_value2, expected_values->value2string))
       && (0 == strcmp(actual_value3, expected_values->value3string))) {
        return 1;
    } else {
        return 0;
    }
}

int test_util_equals_ignorecase(LargestIntegralType fun_arg_untyped,
                                LargestIntegralType expected_value_untyped) {
    const char* actual_value = (void*) fun_arg_untyped;
    const char* expected_value = (void*) expected_value_untyped;

    if((actual_value == NULL) && (expected_value == NULL)) {
        return 1;
    }

    if(0 != strcasecmp(actual_value, expected_value)) {
        printf("%s: expected '%s' but got '%s'\n",
               __FUNCTION__, expected_value, actual_value);
        return 0;
    }

    return 1;
}

int test_util_hasExpectedField(const char* field_name, LargestIntegralType fun_arg_untyped,
                               LargestIntegralType expected_value_untyped) {
    amxc_var_t* values = (void*) fun_arg_untyped;
    const char* expected_value = (void*) expected_value_untyped;

    if(GET_ARG(values, field_name) == NULL) {
        printf("%s: '%s' missing\n", __FUNCTION__, field_name);
        return 0;
    }

    const char* actual_value = GET_CHAR(values, field_name);

    if(0 != strcmp(actual_value, expected_value)) {
        printf("%s: '%s' field expected '%s' but got '%s'\n",
               __FUNCTION__, field_name, expected_value, actual_value);
        return 0;
    }

    return 1;
}

int test_util_has_expected_dhcpv4client(LargestIntegralType fun_arg_untyped,
                                        LargestIntegralType expected_dhcpv4client_untyped) {
    return test_util_hasExpectedField("DHCPv4Client", fun_arg_untyped, expected_dhcpv4client_untyped);
}

gmap_eth_dev_discoping_t* test_util_new_discoping() {
    gmap_eth_dev_discoping_t* discoping = NULL;
    gmap_eth_dev_discoping_new(&discoping, get_dummy_parser());
    assert_non_null(discoping);
    return discoping;
}

gmap_eth_dev_discoping_t* test_util_new_discoping_ext(mock_discoping_t* mock_ext_discoping, bool has_mac) {
    gmap_eth_dev_discoping_t* discoping = NULL;
    will_return(__wrap_discoping_new, mock_ext_discoping);
    gmap_client_init(get_dummy_bus_ctx());
    discoping = test_util_new_discoping();
    test_util_parse_odl("../common/dummy-gmap.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge.odl");
    if(has_mac) {
        test_util_parse_odl("../common/gmap-mockdata-bridge-mac.odl");
    }
    test_util_handle_events();

    return discoping;
}