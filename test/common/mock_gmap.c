/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_gmap.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <amxb/amxb_types.h>
#include <unistd.h>
#include <malloc.h>
#include "test-util.h"

#define GMAP_CONFIG_MODULE_NAME "mod-eth-dev"

amxc_var_t* __wrap_gmap_device_get(const char* key,
                                   UNUSED uint32_t flags) {
    amxc_var_t* fields = (amxc_var_t*) mock();
    check_expected(key);
    if(fields == NULL) {
        return NULL;
    } else {
        amxc_var_t* retval = NULL;
        const amxc_htable_t* fields_htable = amxc_var_constcast(amxc_htable_t, fields);
        assert_non_null(fields_htable);
        amxc_var_new(&retval);
        amxc_var_set_type(retval, AMXC_VAR_ID_LIST);
        amxc_var_add(amxc_htable_t, retval, fields_htable);
        return retval;
    }
}

bool __wrap_gmap_device_set(const char* key,
                            amxc_var_t* values) {

    check_expected(key);
    check_expected(values);
    return true;
}

bool __wrap_gmap_devices_createDevice(UNUSED const char* key,
                                      UNUSED const char* discovery_source,
                                      UNUSED const char* tags, UNUSED bool persistent,
                                      UNUSED const char* default_name) {
    fail_msg("should not use mac as key anymore, should use createDeviceOrGetKey instead");
    return false;
}

bool __wrap_gmap_devices_createDevice_ext(UNUSED const char* key,
                                          UNUSED const char* discovery_source,
                                          UNUSED const char* tags, UNUSED bool persistent,
                                          UNUSED const char* default_name, UNUSED amxc_var_t* values) {
    fail_msg("should not use mac as key anymore, should use createDeviceOrGetKey instead");
    return false;
}

static bool s_check_create_device_args(mock_gmap_devices_create_mockdata_t* mockdata,
                                       char** target_key,
                                       bool* target_exists,
                                       UNUSED const char* mac,
                                       UNUSED const char* discovery_source,
                                       const char* tags, UNUSED bool persistent,
                                       UNUSED const char* default_name, amxc_var_t* values) {
    bool will_return_success = false;
    assert_non_null(mockdata);
    assert_non_null(target_key);

    if(mockdata->expected_tags != NULL) {
        assert_string_equal(tags, mockdata->expected_tags);
    }
    if(target_exists) {
        *target_exists = mockdata->will_return_already_exists;
    }
    *target_key = strdup(mockdata->will_return_key);
    if(mockdata->expected_value_string1 != NULL) {
        const char* actual_value = GET_CHAR(values, mockdata->expected_key1);
        assert_non_null(actual_value);
        assert_string_equal(mockdata->expected_value_string1, actual_value);
    }

    assert_non_null(discovery_source);
    if(mockdata->expected_discovery_source != NULL) {
        assert_string_equal(discovery_source, mockdata->expected_discovery_source);
    }

    will_return_success = mockdata->will_return_success;
    free(mockdata);

    return will_return_success;
}

bool __wrap_gmap_devices_createDeviceOrGetKey(char** target_key,
                                              bool* target_exists,
                                              const char* mac,
                                              const char* discovery_source,
                                              const char* tags, bool persistent,
                                              const char* default_name, amxc_var_t* values) {


    mock_gmap_devices_create_mockdata_t* mockdata =
        (mock_gmap_devices_create_mockdata_t*) mock();

    check_expected(mac);
    return s_check_create_device_args(mockdata, target_key, target_exists, mac, discovery_source, tags, persistent, default_name, values);
}

bool __wrap_gmap_devices_createIfActive(
    char** tgt_dev_key,
    bool* target_exists,
    const char* mac,
    const char* discovery_source,
    const char* tags, bool persistent,
    const char* default_name, amxc_var_t* values, bool active) {


    mock_gmap_devices_create_mockdata_t* mockdata =
        (mock_gmap_devices_create_mockdata_t*) mock();


    check_expected(mac);

    check_expected(active);
    return s_check_create_device_args(mockdata, tgt_dev_key, target_exists, mac, discovery_source, tags, persistent, default_name, values);
}

void mock_gmap_expect_createDeviceOrGetKey(const char* mac, const mock_gmap_devices_create_mockdata_t* mockdata) {
    assert_non_null(mockdata);
    mock_gmap_devices_create_mockdata_t* mockdata_copy = calloc(1, sizeof(mock_gmap_devices_create_mockdata_t));
    *mockdata_copy = *mockdata;
    will_return(__wrap_gmap_devices_createDeviceOrGetKey, mockdata_copy);
    expect_check(__wrap_gmap_devices_createDeviceOrGetKey, mac, test_util_equals_ignorecase, mac);
}

void mock_gmap_expect_createIfActive(const char* mac, const mock_gmap_devices_create_mockdata_t* mockdata, bool active) {
    assert_non_null(mockdata);
    mock_gmap_devices_create_mockdata_t* mockdata_copy = calloc(1, sizeof(mock_gmap_devices_create_mockdata_t));
    *mockdata_copy = *mockdata;
    will_return(__wrap_gmap_devices_createIfActive, mockdata_copy);
    expect_check(__wrap_gmap_devices_createIfActive, mac, test_util_equals_ignorecase, mac);
    expect_value(__wrap_gmap_devices_createIfActive, active, active);
}

bool __wrap_gmap_devices_find(const char* expression, uint32_t flags, amxc_var_t* ret) {
    check_expected(expression);
    check_expected(flags);
    amxc_var_t* devices_var_list = (amxc_var_t*) mock();
    if(devices_var_list == NULL) {
        return false;
    } else {
        const amxc_llist_t* devices_list = amxc_var_constcast(amxc_llist_t, devices_var_list);
        assert_non_null(devices_list);
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_add(amxc_llist_t, ret, devices_list);
        amxc_var_delete(&devices_var_list);
        return true;
    }
}

char* __wrap_gmap_devices_findByMac(const char* mac) {
    check_expected(mac);
    return (char*) mock();
}

void mock_gmap_expect_findByMac(const char* mac, const char* key) {
    assert_non_null(mac);
    expect_check(__wrap_gmap_devices_findByMac, mac, test_util_equals_ignorecase, mac);
    will_return(__wrap_gmap_devices_findByMac, key ? strdup(key) : NULL);
}

char* __wrap_gmap_devices_findByIp(const char* ip) {
    check_expected(ip);
    return (char*) mock();
}

bool __wrap_gmap_device_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    check_expected(key);
    check_expected(tags);
    check_expected(expression);
    check_expected(mode);
    return true;
}

void mock_gmap_expect_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    expect_string(__wrap_gmap_device_setTag, key, key);
    expect_string(__wrap_gmap_device_setTag, tags, tags);
    expect_value(__wrap_gmap_device_setTag, expression, expression);
    expect_value(__wrap_gmap_device_setTag, mode, mode);
}

bool __wrap_gmap_devices_linkAdd(const char* upper_device,
                                 const char* lower_device,
                                 const char* type,
                                 const char* datasource,
                                 UNUSED uint32_t priority) {
    check_expected(upper_device);
    check_expected(lower_device);
    check_expected(type);
    check_expected(datasource);
    return true;
}

void mock_gmap_expect_devices_linkAdd(const char* upper_device, const char* lower_device, const char* type, const char* datasource) {
    expect_string(__wrap_gmap_devices_linkAdd, upper_device, upper_device);
    expect_string(__wrap_gmap_devices_linkAdd, lower_device, lower_device);
    assert_true(type != NULL && (0 == strcmp("ethernet", type) || 0 == strcmp("wifi", type)));
    expect_string(__wrap_gmap_devices_linkAdd, type, type);
    expect_string(__wrap_gmap_devices_linkAdd, datasource, datasource);
}

bool __wrap_gmap_devices_linkReplace(const char* upper_device,
                                     const char* lower_device,
                                     const char* type,
                                     const char* datasource,
                                     UNUSED uint32_t priority) {
    check_expected(upper_device);
    check_expected(lower_device);
    check_expected(type);
    check_expected(datasource);
    return true;
}

void mock_gmap_expect_devices_linkReplace(const char* upper_device, const char* lower_device, const char* type, const char* datasource) {
    expect_string(__wrap_gmap_devices_linkReplace, upper_device, upper_device);
    expect_string(__wrap_gmap_devices_linkReplace, lower_device, lower_device);
    assert_true(type != NULL && (0 == strcmp("ethernet", type) || 0 == strcmp("wifi", type)));
    expect_string(__wrap_gmap_devices_linkReplace, type, type);
    expect_string(__wrap_gmap_devices_linkReplace, datasource, datasource);
}

bool __wrap_gmap_devices_linkRemove(const char* upper,
                                    const char* lower,
                                    const char* datasource) {
    check_expected(upper);
    check_expected(lower);
    check_expected(datasource);
    return false;
}

gmap_query_t* __wrap_gmap_query_open_ext(const char* expression, UNUSED const char* name, gmap_query_cb_t fn, void* user_data) {
    gmap_query_t* retval = NULL;
    __wrap_gmap_query_open_ext_2(&retval, expression, name, 0, fn, user_data);
    return retval;
}

bool __wrap_gmap_query_open_ext_2(gmap_query_t** query,
                                  const char* expression,
                                  UNUSED const char* name,
                                  UNUSED gmap_query_flags_t flags,
                                  gmap_query_cb_t fn,
                                  void* user_data) {
    gmap_query_t* retval = (gmap_query_t*) mock();
    assert_non_null(query);
    check_expected(expression);
    if(retval != NULL) {
        retval->fn = fn;
        retval->data = user_data;
    }
    *query = retval;
    return retval != NULL;
}

void __wrap_gmap_query_close(gmap_query_t* query) {
    check_expected(query);
}

amxd_status_t __wrap_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address) {
    check_expected(key);
    check_expected(address);
    check_expected(family);
    return mock();
}

void mock_gmap_expect_ip_device_delete_address(const char* key, uint32_t family, const char* address, amxd_status_t will_return_status) {
    expect_string(__wrap_gmap_ip_device_delete_address, key, key);
    expect_value(__wrap_gmap_ip_device_delete_address, family, family);
    expect_string(__wrap_gmap_ip_device_delete_address, address, address);
    will_return(__wrap_gmap_ip_device_delete_address, will_return_status);
}

bool __wrap_gmap_device_setActive(const char* key, bool active, const char* source, uint32_t priority) {
    bool rv = mock();
    check_expected(key);
    check_expected(active);
    check_expected(source);
    check_expected(priority);
    return rv;
}

void mock_gmap_expect_setActive(const char* expect_key, bool expect_active, bool return_value) {

    expect_string(__wrap_gmap_device_setActive, key, expect_key);
    expect_value(__wrap_gmap_device_setActive, active, expect_active);
    expect_string(__wrap_gmap_device_setActive, source, "mod-eth-from-netdev");
    expect_value(__wrap_gmap_device_setActive, priority, 50);

    will_return(__wrap_gmap_device_setActive, return_value);
}

bool __wrap_gmap_device_clearTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    check_expected(key);
    check_expected(tags);
    check_expected(expression);
    check_expected(mode);
    return true;
}

bool __wrap_gmap_device_setName(const char* key,
                                const char* name,
                                const char* source) {
    check_expected(key);
    check_expected(name);
    check_expected(source);
    return true;
}

amxd_status_t __wrap_gmap_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                                const char* scope, const char* status_value, const char* address_source, bool reserved) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    check_expected(scope);
    check_expected(status_value);
    check_expected(address_source);
    check_expected(reserved);
    return mock();
}

void mock_gmap_expect_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value) {

    expect_string(__wrap_gmap_ip_device_add_address, key, key);
    expect_value(__wrap_gmap_ip_device_add_address, family, family);
    expect_string(__wrap_gmap_ip_device_add_address, address, address);
    expect_string(__wrap_gmap_ip_device_add_address, scope, scope);
    if(status_value == NULL) {
        expect_value(__wrap_gmap_ip_device_add_address, status_value, status_value);
    } else {
        expect_string(__wrap_gmap_ip_device_add_address, status_value, status_value);
    }
    expect_string(__wrap_gmap_ip_device_add_address, address_source, address_source);
    expect_value(__wrap_gmap_ip_device_add_address, reserved, reserved);

    will_return(__wrap_gmap_ip_device_add_address, return_value);
}

amxd_status_t __wrap_gmap_ip_device_set_address(const char* key, uint32_t family,
                                                const char* address, const char* scope, const char* status_value, const char* address_source,
                                                bool reserved) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    check_expected(scope);
    check_expected(status_value);
    check_expected(address_source);
    check_expected(reserved);
    return mock();
}

void mock_gmap_expect_ip_device_set_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value) {

    expect_string(__wrap_gmap_ip_device_set_address, key, key);
    expect_value(__wrap_gmap_ip_device_set_address, family, family);
    expect_string(__wrap_gmap_ip_device_set_address, address, address);
    if(scope == NULL) {
        expect_value(__wrap_gmap_ip_device_set_address, scope, scope);
    } else {
        expect_string(__wrap_gmap_ip_device_set_address, scope, scope);
    }
    if(status_value == NULL) {
        expect_value(__wrap_gmap_ip_device_set_address, status_value, status_value);
    } else {
        expect_string(__wrap_gmap_ip_device_set_address, status_value, status_value);
    }
    if(address_source == NULL) {
        expect_value(__wrap_gmap_ip_device_set_address, address_source, address_source);
    } else {
        expect_string(__wrap_gmap_ip_device_set_address, address_source, address_source);
    }
    expect_value(__wrap_gmap_ip_device_set_address, reserved, reserved);

    will_return(__wrap_gmap_ip_device_set_address, return_value);
}


amxd_status_t __wrap_gmap_ip_device_get_address(const char* key, uint32_t family,
                                                const char* address, amxc_var_t* const ret_object) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    amxc_var_t* ret = (amxc_var_t*) mock();
    if(ret != NULL) {
        amxc_var_copy(ret_object, ret);
        amxc_var_delete(&ret);
        return amxd_status_ok;
    } else {
        return amxd_status_object_not_found;
    }
}

// Note: not activated for all test groups
//       (gmap_ip_device_get_addresses not in MOCK_WRAP in all test makefiles)
void mock_gmap_expect_ip_device_get_address(const char* key, uint32_t family,
                                            const char* address, amxc_var_t* ret_object) {
    expect_string(__wrap_gmap_ip_device_get_address, key, key);
    expect_value(__wrap_gmap_ip_device_get_address, family, family);
    expect_string(__wrap_gmap_ip_device_get_address, address, address);
    will_return(__wrap_gmap_ip_device_get_address, ret_object);
}

// Note: not activated for all test groups
//       (gmap_ip_device_get_addresses not in MOCK_WRAP in all test makefiles)
amxd_status_t __wrap_gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses) {
    check_expected(key);
    const char* addresses_json_file = (const char*) mock();
    if(addresses_json_file != NULL) {
        amxc_var_t* ret = test_util_read_json_from_file(addresses_json_file);
        assert_non_null(ret);
        amxc_var_copy(target_addresses, ret);
        amxc_var_delete(&ret);
        return amxd_status_ok;
    } else {
        return amxd_status_object_not_found;
    }
}

void mock_gmap_ip_device_get_addresses(const char* key, const char* gmap_addresses_json_file) {
    expect_string(__wrap_gmap_ip_device_get_addresses, key, key);
    will_return(__wrap_gmap_ip_device_get_addresses, gmap_addresses_json_file);
}

amxc_var_t* __wrap_gmap_config_get(const char* module, const char* option) {
    assert_string_equal(module, GMAP_CONFIG_MODULE_NAME);
    assert_non_null(option);
    fail();

    return NULL;
}

void __wrap_gmap_eth_dev_bridgetable_new(gmap_eth_dev_bridgetable_t** instance, const gmap_eth_dev_bridge_t* bridge) {
    assert_non_null(instance);
    assert_non_null(bridge);
    assert_null(*instance);
    *instance = (void*) mock();
}

void __wrap_gmap_eth_dev_dhcp_reader_new(gmap_eth_dev_dhcp_reader_t** instance, const gmap_eth_dev_bridge_t* bridge) {
    assert_non_null(instance);
    assert_non_null(bridge);
    assert_null(*instance);
    *instance = (void*) mock();
}

void __wrap_gmap_eth_dev_neighbors_new(gmap_eth_dev_neighbors_t** instance, const gmap_eth_dev_bridge_t* bridge) {
    assert_non_null(instance);
    assert_non_null(bridge);
    assert_null(*instance);
    *instance = (void*) mock();
}
