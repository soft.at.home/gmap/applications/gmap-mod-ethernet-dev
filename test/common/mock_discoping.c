/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_discoping.h"
#include "gmap_eth_dev_discoping.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>
#include "test-setup.h"
#include <malloc.h>
#include <string.h>

#include "test-util.h"

bool __wrap_discoping_new(discoping_t** discoping, amxo_parser_t* parser) {
    assert_non_null(discoping);
    assert_ptr_equal(parser, get_dummy_parser());
    mock_discoping_t* mock = (mock_discoping_t*) mock();
    assert_null(mock->leak_canary);
    mock->leak_canary = calloc(1, 1);
    *discoping = (discoping_t*) mock;
    amxc_var_init(&mock->log);
    amxc_var_set_type(&mock->log, AMXC_VAR_ID_LIST);
    return true;
}

void __wrap_discoping_delete(discoping_t** discoping) {
    assert_non_null(discoping);
    mock_discoping_t* mock = (mock_discoping_t*) *discoping;
    assert_non_null(mock);
    assert_non_null(mock->leak_canary);
    free(mock->leak_canary);
    mock->leak_canary = NULL;
    free(mock->netdevname);
    mock->netdevname = NULL;
    free(mock->ip);
    mock->ip = NULL;
    free(mock->mac);
    mock->mac = NULL;
    if(!mock->keep_log) {
        amxc_var_clean(&mock->log);
    }
    *discoping = NULL;
}

bool __wrap_discoping_open(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    assert_non_null(netdev_name);
    assert_non_null(mac);
    assert_non_null(ip);

    amxc_var_t* log_item = amxc_var_add_new(&mock->log);
    amxc_var_set_type(log_item, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, log_item, "mac", mac);
    amxc_var_add_key(cstring_t, log_item, "ip", ip);
    amxc_var_add_key(cstring_t, log_item, "netdev_name", netdev_name);
    amxc_var_add_key(cstring_t, log_item, "type", "open");

    if(mock->netdevname == NULL) {
        mock->open_count++;
        assert_null(mock->mac);
        assert_null(mock->ip);
        assert_null(mock->netdevname);
        mock->netdevname = strdup(netdev_name);
        mock->mac = strdup(mac);
        mock->ip = strdup(ip);
    } else {
        // already something opened.
        assert_non_null(mock->mac);
        assert_non_null(mock->ip);
        if((0 == strcmp(mock->mac, mac)) && (0 == strcmp(mock->netdevname, netdev_name)) && (0 == strcmp(mock->ip, ip))) {
            // already the same thing opened -> it's a no-op.
            return false;
        } else {
            // something else opened
        }
    }
    return true;
}

bool __wrap_discoping_close(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    assert_non_null(netdev_name);
    assert_non_null(mac);
    assert_non_null(ip);

    amxc_var_t* log_item = amxc_var_add_new(&mock->log);
    amxc_var_set_type(log_item, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, log_item, "mac", mac);
    amxc_var_add_key(cstring_t, log_item, "ip", ip);
    amxc_var_add_key(cstring_t, log_item, "netdev_name", netdev_name);
    amxc_var_add_key(cstring_t, log_item, "type", "close");

    if(mock->netdevname == NULL) {
        return false;
    }
    mock->close_count++;
    assert_string_equal(mock->netdevname, netdev_name);
    free(mock->netdevname);
    mock->netdevname = NULL;
    free(mock->ip);
    mock->ip = NULL;
    free(mock->mac);
    mock->mac = NULL;
    return true;
}

void __wrap_discoping_set_on_up_cb(discoping_t* discoping, discoping_up_cb_t up_cb) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    mock->up_cb = up_cb;
}

void __wrap_discoping_set_on_down_cb(discoping_t* discoping, discoping_down_cb_t down_cb) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    mock->down_cb = down_cb;
}

void __wrap_discoping_set_on_collision_cb(discoping_t* discoping, discoping_collision_cb_t collision_cb) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    mock->collision_cb = collision_cb;
}

void __wrap_discoping_set_userdata(discoping_t* discoping, void* userdata) {
    mock_discoping_t* mock = (mock_discoping_t*) discoping;
    assert_non_null(mock);
    mock->userdata = userdata;
}

void mock_discoping_assert(mock_discoping_t* mock, int expected_opencount, int expected_closecount, const char* expected_netdevname, const char* expected_mac, const char* expected_ip) {
    assert_non_null(mock);
    if(expected_netdevname != NULL) {
        assert_non_null(mock->netdevname);
        assert_non_null(mock->mac);
        assert_non_null(mock->ip);
        assert_string_equal(mock->netdevname, expected_netdevname);
        assert_string_equal(mock->mac, expected_mac);
        assert_string_equal(mock->ip, expected_ip);
    } else {
        assert_null(expected_mac);
        assert_null(expected_ip);
        assert_null(mock->netdevname);
        assert_null(mock->mac);
        assert_null(mock->ip);
    }
    assert_int_equal(expected_opencount, mock->open_count);
    assert_int_equal(expected_closecount, mock->close_count);
}

void __wrap_discoping_verify(discoping_t* discoping, const char* ip, const char* mac) {
    assert_non_null(discoping);
    check_expected(ip);
    check_expected(mac);
}

void mock_discoping_expect_verify(const char* ip, const char* mac) {
    expect_string(__wrap_discoping_verify, ip, ip);
    expect_check(__wrap_discoping_verify, mac, test_util_equals_ignorecase, mac);
}

void __wrap_discoping_verify_once(discoping_t* discoping, const char* ip, const char* mac) {
    assert_non_null(discoping);
    check_expected(ip);
    check_expected(mac);
}

void mock_discoping_expect_verify_once(const char* ip, const char* mac) {
    expect_string(__wrap_discoping_verify_once, ip, ip);
    expect_check(__wrap_discoping_verify_once, mac, test_util_equals_ignorecase, mac);
}

void __wrap_discoping_mark_seen_network(discoping_t* discoping, const char* ip, const char* mac) {
    assert_non_null(discoping);
    check_expected(ip);
    check_expected(mac);
}

void mock_discoping_expect_mark_seen_network(const char* ip, const char* mac) {
    expect_string(__wrap_discoping_mark_seen_network, ip, ip);
    expect_check(__wrap_discoping_mark_seen_network, mac, test_util_equals_ignorecase, mac);
}

void __wrap_discoping_set_periodic_reverify_enabled(discoping_t* discoping, const char* ip, bool enabled) {
    assert_non_null(discoping);
    check_expected(ip);
    check_expected(enabled);
}

void mock_discoping_expect_set_periodic_reverify_enabled(const char* ip, bool enabled) {
    expect_string(__wrap_discoping_set_periodic_reverify_enabled, ip, ip);
    expect_value(__wrap_discoping_set_periodic_reverify_enabled, enabled, enabled);
}

amxc_var_t* __wrap_discoping_get_open_sockets(discoping_t* discoping) {
    assert_non_null(discoping);

    amxc_var_t* sockets = (amxc_var_t*) mock();
    return sockets;
}



void mock_discoping_expect_get_open_sockets_none(void) {
    amxc_var_t* list = NULL;
    amxc_var_new(&list);
    amxc_var_set_type(list, AMXC_VAR_ID_LIST);
    will_return(__wrap_discoping_get_open_sockets, list);
}

void mock_discoping_expect_get_open_sockets_one(const char* netdev_name, const char* ip, const char* mac) {
    amxc_var_t* list = NULL;
    amxc_var_new(&list);
    amxc_var_set_type(list, AMXC_VAR_ID_LIST);
    amxc_var_t* entry = amxc_var_add_new(list);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(entry, "mac", mac);
    amxc_var_add_new_key_cstring_t(entry, "ip", ip);
    amxc_var_add_new_key_cstring_t(entry, "netdev_name", netdev_name);
    will_return(__wrap_discoping_get_open_sockets, list);
}

void mock_discoping_assert_log(mock_discoping_t* mock_ext_discoping,
                               int64_t index,
                               const char* expected_type,
                               const char* expected_mac,
                               const char* expected_ip,
                               const char* expected_netdev_name) {
    amxc_var_t* log_entry = amxc_var_get_index(&mock_ext_discoping->log, index, AMXC_VAR_FLAG_DEFAULT);
    const char* type = GET_CHAR(log_entry, "type");
    const char* mac = GET_CHAR(log_entry, "mac");
    const char* ip = GET_CHAR(log_entry, "ip");
    const char* netdev_name = GET_CHAR(log_entry, "netdev_name");

    when_null(type, error);
    when_null(mac, error);
    when_null(ip, error);
    when_null(netdev_name, error);

    bool ok = 0 == strcmp(type, expected_type) && 0 == strcmp(mac, expected_mac) && 0 == strcmp(ip, expected_ip) && 0 == strcmp(netdev_name, expected_netdev_name);
    if(!ok) {
        amxc_var_dump(&mock_ext_discoping->log, STDERR_FILENO);
    }

    assert_string_equal(GET_CHAR(log_entry, "type"), expected_type);
    assert_string_equal(GET_CHAR(log_entry, "mac"), expected_mac);
    assert_string_equal(GET_CHAR(log_entry, "ip"), expected_ip);
    assert_string_equal(GET_CHAR(log_entry, "netdev_name"), expected_netdev_name);

    return;

error:
    assert_false(true);
}

gmap_eth_dev_discoping_t* mock_discoping_new(mock_discoping_t* discoping) {
    gmap_eth_dev_discoping_t* result = NULL;
    assert_non_null(discoping);
    assert_null(discoping->leak_canary);

    will_return(__wrap_discoping_new, discoping);
    gmap_eth_dev_discoping_new(&result, get_dummy_parser());
    assert_non_null(result);

    return result;
}

void mock_discoping_delete(gmap_eth_dev_discoping_t** discoping_gmap,
                           mock_discoping_t* discoping) {
    assert_non_null(discoping_gmap);
    assert_non_null(discoping);
    assert_non_null(discoping->leak_canary);

    gmap_eth_dev_discoping_delete(discoping_gmap);
    assert_null(*discoping_gmap);
    assert_null(discoping->leak_canary);
    assert_null(discoping->netdevname);
    assert_null(discoping->ip);
    assert_null(discoping->mac);
}