/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "../common/test-setup.h"

#include "test_dhcp_context.h"
#include "test_dhcp_reader.h"
#include "test_dhcp_option.h"
#include "test_dhcp_clientpath.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__client_id),
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__option55),
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__vendorclass),
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__userclass),
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__hostname),
        cmocka_unit_test(test_gmap_eth_dev_dhcp_option_to_dev_fields__specific_info),

        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader_is_static_address, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader_ip_of_mac__active_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader_ip_of_mac__not_leased, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader_ip_of_mac__inactive_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__new_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__new_lease_other_bridge, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__new_lease_ip_exists, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__new_lease_device_exists, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__new_static_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__vendorclass, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__userclassid, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__remove_nonstatic_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__static_lease_becomes_inactive, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__active_lease_becomes_static, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_inactive, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_active, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_delete, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_flag, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__static_lease_enabled, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__inactive_lease_becomes_static, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__deleted_lease_becomes_static, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__remove_static_lease, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_unreachable, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_reachable, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_unreachable, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_reachable, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__remove_ip, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__option_changes, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__option_changes_including_tag, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__ip_changes, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__ip_changes_inactive, test_setup_full, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__initial_fetch, test_setup_without_component_init, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__initial_fetch_with_options, test_setup_without_component_init, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__initial_fetch_other_bridge, test_setup_without_component_init, test_teardown_full),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_reader__bridge_iface_changes, test_setup_without_component_init, test_teardown_full),

        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__device_existed, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__device_did_not_exist, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__device_creation_failed, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__unset_normal_via_NULL, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__unset_normal_via_empty, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath_set__unset_with_no_device, test_setup_without_component_init, test_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_eth_dev_dhcp_clientpath__scenario, test_setup_full, test_teardown_full),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
