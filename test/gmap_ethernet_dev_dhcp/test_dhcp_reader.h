/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_TEST_DHCP_H__)
#define __GMAP_ETH_DEV_TEST_DHCP_H__

#ifdef __cplusplus
extern "C" {
#endif

void test_gmap_eth_dev_dhcp_reader_is_static_address(void** state);
void test_gmap_eth_dev_dhcp_reader_ip_of_mac__active_lease(void** state);
void test_gmap_eth_dev_dhcp_reader_ip_of_mac__not_leased(void** state);
void test_gmap_eth_dev_dhcp_reader_ip_of_mac__inactive_lease(void** state);
void test_gmap_eth_dev_dhcp_reader__new_lease(void** state);
void test_gmap_eth_dev_dhcp_reader__new_lease_other_bridge(void** state);
void test_gmap_eth_dev_dhcp_reader__new_lease_ip_exists(void** state);
void test_gmap_eth_dev_dhcp_reader__new_lease_device_exists(void** state);
void test_gmap_eth_dev_dhcp_reader__new_static_lease(void** state);
void test_gmap_eth_dev_dhcp_reader__vendorclass(void** state);
void test_gmap_eth_dev_dhcp_reader__userclassid(void** state);
void test_gmap_eth_dev_dhcp_reader__remove_nonstatic_lease(void** state);
void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_inactive(void** state);
void test_gmap_eth_dev_dhcp_reader__active_lease_becomes_static  (void** state);
void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_delete(void** state);
void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_flag(void** state);
void test_gmap_eth_dev_dhcp_reader__static_lease_enabled(void** state);
void test_gmap_eth_dev_dhcp_reader__inactive_lease_becomes_static(void** state);
void test_gmap_eth_dev_dhcp_reader__deleted_lease_becomes_static(void** state);
void test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_inactive(void** state);
void test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_active(void** state);
void test_gmap_eth_dev_dhcp_reader__remove_static_lease(void** state);
void test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_unreachable(void** state);
void test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_reachable(void** state);
void test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_unreachable(void** state);
void test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_reachable(void** state);
void test_gmap_eth_dev_dhcp_reader__remove_ip(void** state);
void test_gmap_eth_dev_dhcp_reader__option_changes(void** state);
void test_gmap_eth_dev_dhcp_reader__option_changes_including_tag(void** state);
void test_gmap_eth_dev_dhcp_reader__ip_changes(void** state);
void test_gmap_eth_dev_dhcp_reader__ip_changes_inactive(void** state);
void test_gmap_eth_dev_dhcp_reader__initial_fetch(void** state);
void test_gmap_eth_dev_dhcp_reader__initial_fetch_with_options(void** state);
void test_gmap_eth_dev_dhcp_reader__initial_fetch_other_bridge(void** state);
void test_gmap_eth_dev_dhcp_reader__bridge_iface_changes(void** state);

#ifdef __cplusplus
}
#endif

#endif
