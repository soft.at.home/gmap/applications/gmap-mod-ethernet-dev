/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_dhcp_reader.h"
#include "test_dhcp_context.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev_dhcp_reader.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>
#include <amxc/amxc.h> // needed for <amxp/amxp.h>
#include <amxp/amxp.h> // needed for <amxd/amxd_object.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <malloc.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "../common/mock_discoping.h"
#include "../common/mock_netmodel.h"

/**
 * cmocka callback that checks @ref __wrap_gmap_device_set `value` argument
 *
 * Checks if it contains expected vendor class id field value.
 *
 * @param fun_arg_untyped: the `value` argument as passed to @ref __wrap_gmap_device_set
 * @param expected_vendor_class_untyped: the expected vendor class id field value
 */
static int s_has_expected_vendor_class(LargestIntegralType fun_arg_untyped,
                                       LargestIntegralType expected_vendor_class_untyped) {
    return test_util_hasExpectedField("VendorClassID", fun_arg_untyped, expected_vendor_class_untyped);
}

static int s_has_expected_user_class_id(LargestIntegralType fun_arg_untyped,
                                        LargestIntegralType expected_user_class_untyped) {
    return test_util_hasExpectedField("UserClassID", fun_arg_untyped, expected_user_class_untyped);
}

void test_gmap_eth_dev_dhcp_reader_is_static_address(UNUSED void** state) {
    // GIVEN a dhcp server with some static leases (that are not actualy leased)
    mock_gmap_expect_findByMac("01:02:03:04:05:01", NULL);
    mock_gmap_expect_findByMac("01:02:03:04:04:04", NULL);
    mock_gmap_expect_findByMac("01:02:03:04:05:03", NULL);
    mock_gmap_expect_findByMac("01:02:03:04:05:04", NULL);
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-static-no-lease.odl");
    test_util_handle_events();

    // case: normal
    assert_true(gmap_eth_dev_dhcp_reader_is_static_address(GMAP_DHCPV4SERVER_PATH "Pool.1.", "192.168.1.1", "01:02:03:04:05:01"));
    // case: static lease is for another mac, so current lease is not yet dropped in favor of new static lease
    assert_false(gmap_eth_dev_dhcp_reader_is_static_address(GMAP_DHCPV4SERVER_PATH "Pool.1.", "192.168.1.2", "01:02:03:04:05:02"));
    // case: static lease is disabled
    assert_false(gmap_eth_dev_dhcp_reader_is_static_address(GMAP_DHCPV4SERVER_PATH "Pool.1.", "192.168.1.4", "01:02:03:04:05:04"));
    // case: static lease is not configured at all
    assert_false(gmap_eth_dev_dhcp_reader_is_static_address(GMAP_DHCPV4SERVER_PATH "Pool.1.", "192.168.1.5", "01:02:03:04:05:05"));
}

static void s_testhelper_create_active_lease(void) {
    test_util_handle_events();
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("id1", "ipv4 dhcp", NULL, gmap_traverse_this);
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.183", NULL);
    mock_gmap_expect_ip_device_add_address("id1", AF_INET, "192.168.1.183", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

static void s_testhelper_create_inactive_lease(void) {
    test_util_handle_events();
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("id1", "ipv4 dhcp", NULL, gmap_traverse_this);
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.183", NULL);
    mock_gmap_expect_ip_device_add_address("id1", AF_INET, "192.168.1.183", "global", "not reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-inactive.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader_ip_of_mac__active_lease(UNUSED void** state) {
    bool ok = false;
    char* ip = NULL;
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // WHEN querying the DHCPv4 server for the ip of the mac of the lease
    ok = gmap_eth_dev_dhcp_reader_ip_of_mac(&ip, GMAP_DHCPV4SERVER_PATH "Pool.1.", "00:E0:4c:36:10:e3");

    // THEN the ip is found
    assert_true(ok);
    assert_string_equal("192.168.1.183", ip);
    free(ip);
}

void test_gmap_eth_dev_dhcp_reader_ip_of_mac__inactive_lease(UNUSED void** state) {
    bool ok = false;
    char* ip = NULL;
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // WHEN querying the DHCPv4 server for the ip of the mac of the lease
    ok = gmap_eth_dev_dhcp_reader_ip_of_mac(&ip, GMAP_DHCPV4SERVER_PATH "Pool.1.", "00:e0:4c:36:10:E3");

    // THEN the ip is found
    assert_true(ok);
    assert_string_equal("192.168.1.183", ip);
    free(ip);
}

void test_gmap_eth_dev_dhcp_reader_ip_of_mac__not_leased(UNUSED void** state) {
    bool ok = false;
    char* ip = NULL;
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // WHEN querying the DHCPv4 server for the ip of another mac
    ok = gmap_eth_dev_dhcp_reader_ip_of_mac(&ip, GMAP_DHCPV4SERVER_PATH "Pool.1.", "00:11:22:33:44:55");

    // THEN no IP is found
    assert_true(ok);
    assert_null(ip);
}

void test_gmap_eth_dev_dhcp_reader__new_lease(UNUSED void** state) {
    // GIVEN a dhcp server and gmap_eth_dev_dhcp_reader (latter is by test_setup_full)
    test_util_handle_events();

    // EXPECT device to be created
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("id1", "ipv4 dhcp", NULL, gmap_traverse_this);
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.183", NULL);
    // EXPECT that the ip address is added
    mock_gmap_expect_ip_device_add_address("id1", AF_INET, "192.168.1.183", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN new lease (and dhcp client) appears:
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__new_lease_other_bridge(UNUSED void** state) {
    // GIVEN a dhcp server and gmap_eth_dev_dhcp_reader (latter is by test_setup_full)
    test_util_handle_events();

    // EXPECT nothing to happen
    // WHEN new lease (and dhcp client) appears for a different bridge:
    test_util_parse_odl("dhcpv4-mockdata-pool-2.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__new_lease_device_exists(UNUSED void** state) {
    // GIVEN a dhcp server and gmap_eth_dev_dhcp_reader (by test-setup):
    test_util_handle_events();

    // EXPECT device to already exists
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id2",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("id2", "ipv4 dhcp", NULL, gmap_traverse_this);
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id2",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_setTag, key, "id2");
    expect_string(__wrap_gmap_device_setTag, tags, "ipv4 dhcp");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // EXPECT that the ip address initially does not exist in gmap
    mock_gmap_expect_ip_device_get_address("id2", AF_INET, "192.168.1.183", NULL);
    // EXPECT that the ip address is added
    mock_gmap_expect_ip_device_add_address("id2", AF_INET, "192.168.1.183", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "id2");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN new lease appears:
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__new_lease_ip_exists(UNUSED void** state) {
    amxc_var_t* ip_in_dhcp;
    // GIVEN a dhcp server, gmap_eth_dev_dhcp_reader (by test-setup), and an ip in dhcp
    test_util_handle_events();
    amxc_var_new(&ip_in_dhcp);
    amxc_var_set_type(ip_in_dhcp, AMXC_VAR_ID_HTABLE);

    // EXPECT device to already exists
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id3",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_setTag, key, "id3");
    expect_string(__wrap_gmap_device_setTag, tags, "ipv4 dhcp");
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id3",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("id3", "ipv4 dhcp", NULL, gmap_traverse_this);
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // EXPECT that the ip address already exists in gmap
    mock_gmap_expect_ip_device_get_address("id3", AF_INET, "192.168.1.183", ip_in_dhcp);
    // EXPECT that the ip address is updated
    mock_gmap_expect_ip_device_set_address("id3", AF_INET, "192.168.1.183", "global", NULL, "DHCP", false, amxd_status_ok);
    // EXPECT that the IP is verified
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "id3");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN new lease appears:
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__new_static_lease(UNUSED void** state) {
    // GIVEN a static address
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", NULL);
    test_util_parse_odl("dhcpv4-mockdata-static-address.odl");
    test_util_handle_events();

    // EXPECT device to be created
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.183", NULL);
    // EXPECT that the ip address is added, as reserved
    mock_gmap_expect_ip_device_add_address("id1", AF_INET, "192.168.1.183", "global", "reachable", "DHCP",
                                           true, // <-- reserved
                                           amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "id1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN new lease appears for the same mac-ip as the static address configuration
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();
}

static void s_testhelper_make_lease_static(void) {
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", NULL);
    // Note: this helper function is also used in the context that the device doesn't exist in gmap
    //       so no mock_gmap_expect_ip_device_set_address() here.
    test_util_parse_odl("dhcpv4-mockdata-static-address.odl");
    test_util_handle_events();
}

static void s_expect_made_reserved(bool is_gmap_reachable) {
    amxc_var_t* address_data;
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", is_gmap_reachable ? "reachable" : "not reachable");
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", address_data);
    mock_gmap_expect_ip_device_set_address("key-of-my-device", AF_INET, "192.168.1.183", "global", NULL, "DHCP",
                                           true, // <-- marked as reserved
                                           amxd_status_ok);
}

void test_gmap_eth_dev_dhcp_reader__active_lease_becomes_static(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECT the lease to be marked as reserved
    s_expect_made_reserved(true);

    // WHEN the lease is made static
    test_util_parse_odl("dhcpv4-mockdata-static-address.odl");
    test_util_handle_events();
}

static void s_expect_made_nonreserved(void) {
    amxc_var_t* ip_data = NULL;
    // mac will be found:
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    // ip will initially be reserved
    amxc_var_new(&ip_data);
    amxc_var_set_type(ip_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ip_data, "Status", "reachable");
    amxc_var_add_key(bool, ip_data, "Reserved", true);
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", ip_data);
    // ip will be marked as non-reserved
    mock_gmap_expect_ip_device_set_address("key-of-my-device", AF_INET, "192.168.1.183", "global", NULL, "DHCP",
                                           false, // <-- marked as non-reserved
                                           amxd_status_ok);
}

void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_delete(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();
    s_testhelper_make_lease_static();

    // EXPECT the lease to be marked as non-reserved
    s_expect_made_nonreserved();

    // WHEN the lease is made non-static by object removal
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.StaticAddress.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_nonstatic_through_flag(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();
    s_testhelper_make_lease_static();

    // EXPECT the lease to be marked as non-reserved
    s_expect_made_nonreserved();

    // WHEN the lease is made non-reserved by setting "Enable" flag on "StaticAddress"
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.StaticAddress.1.");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__static_lease_enabled(UNUSED void** state) {
    // GIVEN an active lease with disabled static configuration:
    s_testhelper_create_active_lease();
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", NULL);
    test_util_parse_odl("dhcpv4-mockdata-static-address-disabled.odl");
    test_util_handle_events();

    // EXPECT the lease to be marked as static
    s_expect_made_reserved(true);

    // WHEN the lease is made non-static by setting "Enable" flag on "StaticAddress"
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.StaticAddress.1.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();
}



static void s_expect_delete_ip(void) {
    amxc_var_t* address_data;
    // expect mark as not reachable:
    // expect ip found
    expect_string(__wrap_gmap_devices_findByIp, ip, "192.168.1.183");
    will_return(__wrap_gmap_devices_findByIp, strdup("key-of-my-device"));
    // expect IP has DHCP as discovery source ("AddressSource"):
    amxc_var_new(&address_data);
    amxc_var_set_type(address_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_data, "Status", "reachable");
    amxc_var_add_key(cstring_t, address_data, "AddressSource", "DHCP");
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", address_data);
    // expect ip delete
    mock_gmap_expect_ip_device_delete_address("key-of-my-device", AF_INET, "192.168.1.183", amxd_status_ok);
}

void test_gmap_eth_dev_dhcp_reader__inactive_lease_becomes_static(UNUSED void** state) {
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // EXPECT the lease to be marked as reserved (conform the specs of "Reserved" parameter in ip.odl in gmap-mibs-common))
    s_expect_made_reserved(false);

    // WHEN the ip-mac combination (for which there is a lease for an inactive client) is made static
    test_util_parse_odl("dhcpv4-mockdata-static-address.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__deleted_lease_becomes_static(UNUSED void** state) {
    amxd_trans_t trans;
    // GIVEN a deleted lease
    s_testhelper_create_active_lease();
    // delete ip:
    s_expect_delete_ip();
    // clear dhcp client path:
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "dev1");
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");
    // delete dhcp client:
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();

    // EXPECT the lease to be *not* be marked as static (conform the specs of "Reserved" parameter in ip.odl in gmap-mibs-common))
    mock_gmap_expect_ip_device_get_address("key-of-my-device", AF_INET, "192.168.1.183", NULL);
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");

    // WHEN the ip-mac combination (for which there is no active lease) is made static
    test_util_parse_odl("dhcpv4-mockdata-static-address.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__vendorclass(UNUSED void** state) {
    // GIVEN a dummy dhcp server and a gmap-mod-ethernet-dev
    // a dummy dhcp server:
    test_util_handle_events();
    // a gmap-mod-ethernet-dev:
    // (already initialized by @ref test_setup_full)

    // EXPECT the device to be created
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    // EXPECT that the ip address initially does not exist in gmap
    mock_gmap_expect_ip_device_get_address("the_id", AF_INET, "192.168.1.123", NULL);
    // EXPECT that the ip address is added
    mock_gmap_expect_ip_device_add_address("the_id", AF_INET, "192.168.1.123", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.123", "12:ab:34:cd:56:ef");
    // EXPECT that DHCPv4Client is set
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    // EXPECT that vendorclass of the device will be set to "very fancy smartphone".
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_vendor_class, "very fancy smartphone");
    mock_gmap_expect_setTag("the_id", "ipv4 dhcp", NULL, gmap_traverse_this);

    // WHEN a lease happens with vendor class option set
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-vendorclass.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__userclassid(UNUSED void** state) {
    // GIVEN a dummy dhcp server and a gmap-mod-ethernet-dev
    // a dummy dhcp server:
    test_util_handle_events();
    // a gmap-mod-ethernet-dev:
    // (already initialized by @ref test_setup_full)

    // EXPECT the device to be created
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    // EXPECT that the ip address initially does not exist in gmap
    mock_gmap_expect_ip_device_get_address("the_id", AF_INET, "192.168.1.123", NULL);
    // EXPECT that the ip address is added
    mock_gmap_expect_ip_device_add_address("the_id", AF_INET, "192.168.1.123", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.123", "12:ab:34:cd:56:ef");
    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    // EXPECTED that userclassid of the device will be set to "myuserclass".
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_user_class_id, "myuserclass");
    mock_gmap_expect_setTag("the_id", "ipv4 dhcp", NULL, gmap_traverse_this);
    // EXPECT that the gmap device name is set
    expect_string(__wrap_gmap_device_setName, key, "the_id");
    expect_string(__wrap_gmap_device_setName, name, "myuserclass");
    expect_string(__wrap_gmap_device_setName, source, "UserClassID");

    // WHEN a lease happens with user class id option set
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-userclassid.odl");
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__remove_nonstatic_lease(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECTED that the IP address will be removed
    s_expect_delete_ip();

    // EXPECT that the dhcp client path will be cleared
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "mykey");
    expect_string(__wrap_gmap_device_set, key, "mykey");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");

    // WHEN the lease object is removed
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

static void s_set_lease_active(bool active) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.");
    amxd_trans_set_bool(&trans, "Active", active);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_inactive(UNUSED void** state) {
    // GIVEN an active non-static lease
    s_testhelper_create_active_lease();

    // EXPECTED that the IP address will become marked as not reachable
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    mock_gmap_expect_ip_device_set_address("key-of-my-device", AF_INET, "192.168.1.183", "global",
                                           "not reachable", // <-- marked as not reachable
                                           "DHCP", false, amxd_status_ok);

    // WHEN the lease becomes inactive
    s_set_lease_active(false);
}

void test_gmap_eth_dev_dhcp_reader__nonstatic_lease_becomes_active(UNUSED void** state) {
    // GIVEN an inactive non-static lease
    s_testhelper_create_inactive_lease();

    // EXPECTED that the IP address will become marked as reachable
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    mock_gmap_expect_ip_device_set_address("key-of-my-device", AF_INET, "192.168.1.183", "global",
                                           "reachable", // <-- marked as reachable
                                           "DHCP", false, amxd_status_ok);

    // WHEN the lease becomes active
    s_set_lease_active(true);
}

void test_gmap_eth_dev_dhcp_reader__static_lease_becomes_inactive(UNUSED void** state) {
    // GIVEN an active static lease
    s_testhelper_create_active_lease();
    s_testhelper_make_lease_static();

    // EXPECTED that the IP address will become marked as not reachable and as not reserved
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "key-of-my-device");
    mock_gmap_expect_ip_device_set_address("key-of-my-device", AF_INET, "192.168.1.183", "global",
                                           "not reachable", // <-- marked as not reachable
                                           "DHCP",
                                           false,           // <-- marked as not reserved
                                           amxd_status_ok);

    // WHEN the lease becomes inactive
    s_set_lease_active(false);
}

void test_gmap_eth_dev_dhcp_reader__remove_static_lease(UNUSED void** state) {
    // GIVEN an active static lease
    s_testhelper_create_active_lease();
    s_testhelper_make_lease_static();

    // EXPECTED that the IP address will become marked as not reachable and as not-reserved
    s_expect_delete_ip();

    // EXPECT that the dhcp client path will be cleared
    mock_gmap_expect_findByMac("00:e0:4c:36:10:e3", "dev1");
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");

    // WHEN the lease object is removed
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_unreachable(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECTED nothing happens, especially the IP will not be set as not-reachable

    // WHEN a field of the lease object changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.");
    amxd_trans_set_cstring_t(&trans, "FutureField", __func__);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__lease_fieldchange_does_not_make_reachable(UNUSED void** state) {
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // EXPECTED nothing happens

    // WHEN a field of the lease object changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.");
    amxd_trans_set_cstring_t(&trans, "FutureField", __func__);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_unreachable(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECTED nothing happens, especially the IP will not be set as not-reachable

    // WHEN a field of the ip object changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.IPv4Address.1.");
    amxd_trans_set_cstring_t(&trans, "FutureField", __func__);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__ip_fieldchange_does_not_make_reachable(UNUSED void** state) {
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // EXPECTED nothing happens

    // WHEN a field of the ip object changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.IPv4Address.1.");
    amxd_trans_set_cstring_t(&trans, "FutureField", __func__);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__remove_ip(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECTED that the IP address will be removed in gmap
    s_expect_delete_ip();

    // WHEN the IP is removed in DHCPv4
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.IPv4Address");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__option_changes(UNUSED void** state) {
    // GIVEN a lease with a DHCP option set
    test_util_handle_events();
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_ip_device_get_address("the_id", AF_INET, "192.168.1.123", NULL);
    mock_gmap_expect_ip_device_add_address("the_id", AF_INET, "192.168.1.123", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.123", "12:ab:34:cd:56:ef");
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_setTag("the_id", "ipv4 dhcp", NULL, gmap_traverse_this);
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_vendor_class, "very fancy smartphone");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-vendorclass.odl");
    test_util_handle_events();

    // EXPECT the gmap value corresponding to the option to change
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_setTag("the_id", "ipv4 dhcp", NULL, gmap_traverse_this);
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_vendor_class, "a normal phone");

    // WHEN the DHCP option changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.Option.1.");
    amxd_trans_set_cstring_t(&trans, "Value", "61206e6f726d616c2070686f6e65");
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__option_changes_including_tag(UNUSED void** state) {
    // GIVEN a lease with a DHCP option set
    test_util_handle_events();
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_ip_device_get_address("the_id", AF_INET, "192.168.1.123", NULL);
    mock_gmap_expect_ip_device_add_address("the_id", AF_INET, "192.168.1.123", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.123", "12:ab:34:cd:56:ef");
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "the_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");
    expect_string(__wrap_gmap_device_set, key, "the_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_vendor_class, "very fancy smartphone");
    mock_gmap_expect_setTag("the_id", "ipv4 dhcp", NULL, gmap_traverse_this);
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-vendorclass.odl");
    test_util_handle_events();

    // EXPECT the gmap value corresponding to the option to change
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "test_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp"
    }));
    mock_gmap_expect_setTag("test_id", "ipv4 dhcp", NULL, gmap_traverse_this);
    expect_string(__wrap_gmap_device_set, key, "test_id");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_user_class_id, "testuserclass");
    // EXPECT that the gmap device name is set
    expect_string(__wrap_gmap_device_setName, key, "test_id");
    expect_string(__wrap_gmap_device_setName, name, "testuserclass");
    expect_string(__wrap_gmap_device_setName, source, "UserClassID");

    // WHEN the DHCP option changes from vendorclass to userclass
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.Option.1.");
    amxd_trans_set_uint32_t(&trans, "Tag", 77);
    amxd_trans_set_cstring_t(&trans, "Value", "7465737475736572636c617373");
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__ip_changes(UNUSED void** state) {
    // GIVEN an active lease
    s_testhelper_create_active_lease();

    // EXPECT the new IP to be added and the old deleted
    // expect device found:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id_with_changing_ip",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    // expect old ip verified:
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // expect tag set:
    expect_string(__wrap_gmap_device_setTag, key, "id_with_changing_ip");
    expect_string(__wrap_gmap_device_setTag, tags, "ipv4 dhcp");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // expect ip added:
    mock_gmap_expect_ip_device_get_address("id_with_changing_ip", AF_INET, "192.168.1.2", NULL);
    mock_gmap_expect_ip_device_add_address("id_with_changing_ip", AF_INET, "192.168.1.2", "global", "reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.2", "00:e0:4c:36:10:e3");

    // WHEN the IP changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.IPv4Address.1.");
    amxd_trans_set_cstring_t(&trans, "IPAddress", "192.168.1.2");
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__ip_changes_inactive(UNUSED void** state) {
    // GIVEN an inactive lease
    s_testhelper_create_inactive_lease();

    // EXPECT the new IP to be added and the old deleted
    // expect device found:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "id_with_changing_ip",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    // expect ip verified to be potentially/probably deleted:
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");
    // expect tag set:
    expect_string(__wrap_gmap_device_setTag, key, "id_with_changing_ip");
    expect_string(__wrap_gmap_device_setTag, tags, "ipv4 dhcp");
    expect_value(__wrap_gmap_device_setTag, expression, NULL);
    expect_value(__wrap_gmap_device_setTag, mode, gmap_traverse_this);
    // expect ip added and set as not reachable:
    mock_gmap_expect_ip_device_get_address("id_with_changing_ip", AF_INET, "192.168.1.2", NULL);
    mock_gmap_expect_ip_device_add_address("id_with_changing_ip", AF_INET, "192.168.1.2", "global", "not reachable", "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.2", "00:e0:4c:36:10:e3");

    // WHEN the IP changes
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.1.IPv4Address.1.");
    amxd_trans_set_cstring_t(&trans, "IPAddress", "192.168.1.2");
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

void test_gmap_eth_dev_dhcp_reader__initial_fetch(void** state) {
    // GIVEN a dhcp server with one lease
    test_util_parse_odl("dhcpv4-dm.odl");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();

    // EXPECT the lease to be recorded in gmap:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_ip_device_get_address("dev1", AF_INET, "192.168.1.183", NULL);
    mock_gmap_expect_ip_device_add_address("dev1", AF_INET, "192.168.1.183", "global",
                                           "not reachable",
                                           "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");

    // EXPECT dhcp client path to be set:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    test_setup_dhcp_reader_default();
}

void test_gmap_eth_dev_dhcp_reader__initial_fetch_with_options(void** state) {
    // GIVEN a dhcp server with one lease that has DHCP options
    test_util_parse_odl("dhcpv4-dm.odl");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-userclassid.odl");
    test_util_handle_events();

    // EXPECT the lease to be recorded in gmap:
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_ip_device_get_address("dev1", AF_INET, "192.168.1.123", NULL);
    mock_gmap_expect_ip_device_add_address("dev1", AF_INET, "192.168.1.123", "global",
                                           "not reachable",
                                           "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.123", "12:ab:34:cd:56:ef");

    // EXPECT dhcp client path to be set:
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // EXPECT that the DHCP options propagate to the gmap device
    mock_gmap_expect_createDeviceOrGetKey("12:ab:34:cd:56:ef", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = true,
        .will_return_success = true,
    }));
    mock_gmap_expect_setTag("dev1", "ipv4 dhcp", NULL, gmap_traverse_this);
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, s_has_expected_user_class_id, "myuserclass");
    expect_string(__wrap_gmap_device_setName, key, "dev1");
    expect_string(__wrap_gmap_device_setName, name, "myuserclass");
    expect_string(__wrap_gmap_device_setName, source, "UserClassID");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    test_setup_dhcp_reader_default();
}

void test_gmap_eth_dev_dhcp_reader__initial_fetch_other_bridge(void** state) {
    // GIVEN a dhcp server with one lease on a different pool
    test_util_parse_odl("dhcpv4-dm.odl");
    test_util_parse_odl("dhcpv4-mockdata-pool-2.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_handle_events();

    // EXPECT nothing to happen

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    test_setup_dhcp_reader_default();
}

void test_gmap_eth_dev_dhcp_reader__bridge_iface_changes(void** state) {
    amxc_var_t pool;

    amxc_var_init(&pool);
    amxc_var_set(cstring_t, &pool, "Device.IP.Interface.2.");

    // GIVEN a dhcp server with one lease on two different pools
    test_util_parse_odl("dhcpv4-dm.odl");
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_parse_odl("dhcpv4-mockdata-pool-2.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options.odl");
    test_util_parse_odl("dhcpv4-mockdata-no-options-2.odl");
    test_util_handle_events();

    // EXPECT the lease to be recorded in gmap:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_ip_device_get_address("dev1", AF_INET, "192.168.1.183", NULL);
    mock_gmap_expect_ip_device_add_address("dev1", AF_INET, "192.168.1.183", "global",
                                           "not reachable",
                                           "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.183", "00:e0:4c:36:10:e3");

    // EXPECT dhcp client path to be set:
    mock_gmap_expect_createDeviceOrGetKey("00:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    test_setup_dhcp_reader_default();

    // EXPECT the other lease to be recorded in gmap:
    mock_gmap_expect_createDeviceOrGetKey("01:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_ip_device_get_address("dev1", AF_INET, "192.168.1.184", NULL);
    mock_gmap_expect_ip_device_add_address("dev1", AF_INET, "192.168.1.184", "global",
                                           "not reachable",
                                           "DHCP", false, amxd_status_ok);
    mock_discoping_expect_verify("192.168.1.184", "01:e0:4c:36:10:e3");

    // EXPECT dhcp client path to be set:
    mock_gmap_expect_createDeviceOrGetKey("01:e0:4c:36:10:e3", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.2.Client.1.");

    // WHEN the interface path changes
    mock_netmodel_set_data(NETMODEL_DATA_KEY, &pool);

    // cleanup
    amxc_var_clean(&pool);
}
