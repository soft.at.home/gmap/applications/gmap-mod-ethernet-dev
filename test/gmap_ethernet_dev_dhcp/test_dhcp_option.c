/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_dhcp_option.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev.h"
#include "gmap_eth_dev_dhcp_option.h"
#include <string.h>
#include <malloc.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"

static void s_testhelper_to_dev_fields_char(uint32_t dhcp_flag, const char* dhcp_raw_option,
                                            const char* expected_dev_field_name,
                                            const char* expected_dev_field_value,
                                            const char* expected_dev_name_source,
                                            const char* expected_dev_name_value,
                                            const char* expected_dev_tags
                                            ) {
    char* actual_gmap_field_value = NULL;
    const char* actual_dev_tags = NULL;
    amxc_var_t actual_dev_fields;
    amxc_var_t actual_dev_names;
    amxc_var_init(&actual_dev_fields);
    amxc_var_init(&actual_dev_names);
    amxc_var_set_type(&actual_dev_names, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&actual_dev_fields, AMXC_VAR_ID_HTABLE);

    // WHEN converting to gmap-fields
    gmap_eth_dev_dhcp_option_to_dev_fields(&actual_dev_fields, &actual_dev_tags, &actual_dev_names, dhcp_flag, dhcp_raw_option);

    // THEN the expected gmap field is found (or no gmap fields if no expected)
    if(expected_dev_field_name != NULL) {
        assert_int_equal(1, amxc_htable_size(amxc_var_constcast(amxc_htable_t, &actual_dev_fields)));
        actual_gmap_field_value = amxc_var_dyncast(cstring_t, GET_ARG(&actual_dev_fields, expected_dev_field_name));
        assert_non_null(actual_gmap_field_value);
        assert_string_equal(expected_dev_field_value, actual_gmap_field_value);
    } else {
        assert_null(expected_dev_field_value);
        assert_int_equal(0, amxc_htable_size(amxc_var_constcast(amxc_htable_t, &actual_dev_fields)));
    }
    // THEN gmap tags as expected
    if(expected_dev_tags != NULL) {
        assert_non_null(actual_dev_tags);
        assert_string_equal(expected_dev_tags, actual_dev_tags);
    } else {
        assert_null(actual_dev_tags);
    }
    // THEN name is as expected
    if(expected_dev_name_source != NULL) {
        amxc_htable_it_t* hit = amxc_htable_get_first(amxc_var_constcast(amxc_htable_t, &actual_dev_names));
        assert_non_null(hit);
        assert_non_null(expected_dev_name_value);
        assert_string_equal(expected_dev_name_source, amxc_htable_it_get_key(hit));
        assert_string_equal(expected_dev_name_value, amxc_var_constcast(cstring_t, amxc_htable_it_get_data(hit, amxc_var_t, hit)));
    } else {
        // No names set:
        assert_null(expected_dev_name_value);
        assert_int_equal(0, amxc_htable_size(amxc_var_constcast(amxc_htable_t, &actual_dev_names)));
    }

    free(actual_gmap_field_value);
    amxc_var_clean(&actual_dev_fields);
    amxc_var_clean(&actual_dev_names);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__client_id(UNUSED void** state) {
    s_testhelper_to_dev_fields_char(61, "0100e04c3610e3",
                                    "ClientID", "00E04C3610E3",
                                    NULL, NULL,
                                    NULL);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__option55(UNUSED void** state) {
    s_testhelper_to_dev_fields_char(55, "0102060c0f1a1c79032128292a77f9fc11",
                                    "DHCPOption55", "1,2,6,12,15,26,28,121,3,33,40,41,42,119,249,252,17",
                                    NULL, NULL,
                                    NULL);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__vendorclass(UNUSED void** state) {
    s_testhelper_to_dev_fields_char(60, "766572792066616e637920736d61727470686f6e65",
                                    "VendorClassID", "very fancy smartphone",
                                    NULL, NULL,
                                    NULL);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__userclass(UNUSED void** state) {
    // Small HOWTO to make dhclient send option77:
    // Add `send user-class "myuserclass";` to /etc/dhcp/dhclient.conf
    // Run `sudo dhclient -v -d`

    s_testhelper_to_dev_fields_char(77, "6d7975736572636c617373",
                                    "UserClassID", "myuserclass",
                                    "UserClassID", "myuserclass",
                                    NULL);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__hostname(UNUSED void** state) {
    s_testhelper_to_dev_fields_char(12, "6162636465666768",
                                    NULL, NULL,
                                    "dhcp", "abcdefgh",
                                    NULL);
}

void test_gmap_eth_dev_dhcp_option_to_dev_fields__specific_info(UNUSED void** state) {
    // GIVEN nothing worthmentioning
    amxc_var_t dev_fields;
    amxc_var_t dev_names;
    amxc_var_init(&dev_fields);
    amxc_var_init(&dev_names);
    amxc_var_set_type(&dev_fields, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&dev_names, AMXC_VAR_ID_HTABLE);
    const char* dev_tags = NULL;

    // WHEN converting DHCP option 125 to to gmap-fields
    gmap_eth_dev_dhcp_option_to_dev_fields(&dev_fields, &dev_tags, &dev_names, 125, "00000de92501063138383342460210312e30573137313941303030303131350309574c2d5354572d3030");

    // THEN it contains the OUI
    assert_string_equal("1883BF", GET_CHAR(&dev_fields, "OUI"));
    // THEN it contains serialnumber
    assert_null(GET_CHAR(&dev_fields, "just to be sure nonexisting fields are NULL and not empty"));
    assert_string_equal("1.0W1719A0000115", GET_CHAR(&dev_fields, "SerialNumber"));
    // THEN it contains ProductClass
    assert_string_equal("WL-STW-00", GET_CHAR(&dev_fields, "ProductClass"));
    // THEN it contains no other fields
    assert_int_equal(3, amxc_htable_size(amxc_var_constcast(amxc_htable_t, &dev_fields)));
    // THEN the tag is set to "manageable"
    assert_string_equal(dev_tags, "manageable");
    // THEN no names are set
    assert_int_equal(0, amxc_htable_size(amxc_var_constcast(amxc_htable_t, &dev_names)));

    amxc_var_clean(&dev_fields);
    amxc_var_clean(&dev_names);
}


