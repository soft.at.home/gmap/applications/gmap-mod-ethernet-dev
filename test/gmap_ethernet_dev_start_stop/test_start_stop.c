/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_start_stop.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev_entrypoint.h"
#include "gmap_eth_dev.h"

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_discoping.h"


void __wrap_gmap_eth_dev_bridge_collection_dhcpserver_available(gmap_eth_dev_bridge_collection_t* collection, amxb_bus_ctx_t* dhcp) {
    function_called();
    assert_non_null(collection);
    assert_non_null(dhcp);
}

void test_gmap_eth_dev__start_stop(void** state) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;

    // GIVEN an environment with ambiorix etc.
    test_setup_without_component_init(state);
    bus_ctx = get_dummy_bus_ctx();
    assert_ptr_not_equal(bus_ctx, NULL);
    dm = get_dummy_dm();
    parser = get_dummy_parser();

    // WHEN starting the component
    expect_any_always(__wrap_amxb_be_who_has, object_path);
    will_return_always(__wrap_amxb_be_who_has, get_dummy_bus_ctx());
    expect_any(__wrap_gmap_query_open_ext_2, expression);
    will_return(__wrap_gmap_query_open_ext_2, &eth_dev_bridge_query);
    expect_function_call(__wrap_netmodel_initialize);
    expect_string(__wrap_amxb_wait_for_object, object_path, "DHCPv4Server.");

    assert_int_equal(0, _gmap_eth_dev_main(AMXO_START, get_dummy_dm(), get_dummy_parser()));

    test_util_handle_events();

    // THEN globals that we unfortunately are still stuck with are correct
    assert_ptr_equal(gmap_eth_dev_get_ctx_netdev(), bus_ctx);
    assert_ptr_equal(gmap_eth_dev_get_ctx_dhcp(), NULL);

    expect_function_call(__wrap_gmap_eth_dev_bridge_collection_dhcpserver_available);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    assert_ptr_equal(gmap_eth_dev_get_ctx_dhcp(), bus_ctx);

    expect_any(__wrap_gmap_query_close, query);
    expect_function_call(__wrap_netmodel_cleanup);

    assert_int_equal(_gmap_eth_dev_main(AMXO_STOP, dm, parser), 0);
    test_teardown(state);
}
