/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

 #include <fcntl.h>
#include "gmap_eth_dev_util.h"
#include "test_util.h"
#include <yajl/yajl_gen.h> // required by amxj/amxj_variant.h
#include "amxj/amxj_variant.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include "amxd/amxd_object.h"
#include <string.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "test_util.h"

static void s_check_split_path(const char* path, uint32_t items, const char* expect_left, const char* expect_right) {
    char* actual_left = "a trap";
    char* actual_right = "another trap";
    bool actual_ok = gmap_eth_dev_util_split_path(&actual_left, &actual_right, path, items);
    bool expect_ok = expect_left != NULL && expect_right != NULL;
    if(expect_ok) {
        assert_true(actual_ok);
        assert_string_equal(expect_right, actual_right);
        assert_string_equal(expect_left, actual_left);
        free(actual_left);
        free(actual_right);
    } else {
        assert_false(actual_ok);
        assert_null(expect_right);
        assert_null(actual_right);
    }
}

void test_gmap_eth_dev_util_split_path(UNUSED void** state) {
    s_check_split_path("abc.def.uvw.xyz", 2, "abc.def.", "uvw.xyz");
    s_check_split_path("abc.def.uvw.xyz", 1, "abc.", "def.uvw.xyz");
    s_check_split_path("abc.def.uvw.xyz", 0, NULL, NULL);
    s_check_split_path("abc.def.uvw.xyz", 0, NULL, NULL);
    s_check_split_path(NULL, 0, NULL, NULL);
    s_check_split_path(NULL, 2, NULL, NULL);
    s_check_split_path("abc", 2, NULL, NULL);
    s_check_split_path("abc.def", 2, NULL, NULL);
    s_check_split_path("abc.def.xyz", 2, "abc.def.", "xyz");
    s_check_split_path(".", 1, ".", "");
    s_check_split_path("abc.def.", 1, "abc.", "def.");
    s_check_split_path("abc.def.", 2, "abc.def.", "");
}

void test_gmap_eth_dev_util_split_path__null_left_part(UNUSED void** state) {
    // GIVEN only a pointer to the right part
    char* actual_right = NULL;

    // WHEN asking only the right part and not the left
    bool actual_ok = gmap_eth_dev_util_split_path(NULL, &actual_right, "abc.def.uvw.xyz", 2);

    // THEN we got the right part (left is ignored)
    assert_true(actual_ok);
    assert_string_equal(actual_right, "uvw.xyz");

    free(actual_right);
}

void test_gmap_eth_dev_util_split_path__null_right_part(UNUSED void** state) {
    // GIVEN only a pointer to the left part
    char* actual_left = NULL;

    // WHEN asking only the left part and not the right
    bool actual_ok = gmap_eth_dev_util_split_path(&actual_left, NULL, "abc.def.uvw.xyz", 2);

    // THEN we got the left part (right is ignored)
    assert_true(actual_ok);
    assert_string_equal(actual_left, "abc.def.");

    free(actual_left);
}

void test_gmap_eth_dev_util_split_path__null_parts(UNUSED void** state) {
    // GIVEN nothing

    // WHEN asking neither the left part nor the right
    bool actual_splittable = gmap_eth_dev_util_split_path(NULL, NULL, "abc.def.uvw.xyz", 2);
    bool actual_splittable1 = gmap_eth_dev_util_split_path(NULL, NULL, NULL, 2);
    bool actual_splittable2 = gmap_eth_dev_util_split_path(NULL, NULL, "abc.def.uvw.xyz", 5);

    // THEN we get correct reply if it is splittable
    assert_true(actual_splittable);
    assert_false(actual_splittable1);
    assert_false(actual_splittable2);
}


void test_gmap_eth_dev_util_get_gmap_device_fields__normal(UNUSED void** state) {
    amxc_var_t* port_fields_output = NULL;

    // GIVEN a netdev index and a port
    uint32_t port_netdev_idx = 123;
    amxc_var_t* port_fields_input = test_util_create_port("myport", "11:22:33:44:55:66", "Device.Ethernet.Interface.1300.");
    amxc_var_add_key(uint32_t, port_fields_input, "NetDevIndex", port_netdev_idx);
    amxc_var_add_key(int32_t, port_fields_input, "MysteriousField", -512);

    // EXPECT device to be searched and its fields retrieved
    test_util_expect_search_port("myport", port_fields_input);

    // WHEN asking the fields of the gmap port with that index
    port_fields_output = gmap_eth_dev_util_get_gmap_device_fields(port_netdev_idx);

    // THEN we get the fields
    assert_non_null(port_fields_output);
    assert_int_equal(-512, GET_INT32(port_fields_output, "MysteriousField"));

    amxc_var_delete(&port_fields_output);
    amxc_var_delete(&port_fields_input);
}

void test_gmap_eth_dev_util_get_gmap_device_fields__notFound(UNUSED void** state) {
    amxc_var_t* port_fields_output = NULL;

    // GIVEN a netdev index but no port
    uint32_t port_netdev_idx = 123;

    // EXPECT port to be searched and not found
    test_util_expect_search_port(NULL, NULL);

    // WHEN asking the fields of the gmap port with that index
    port_fields_output = gmap_eth_dev_util_get_gmap_device_fields(port_netdev_idx);

    // THEN we get nothing back
    assert_null(port_fields_output);
}

void test_gmap_eth_dev_util_get_gmap_device_fields__error_in_find(UNUSED void** state) {
    amxc_var_t* port_fields_output = NULL;

    // GIVEN a netdev index but no port
    uint32_t port_netdev_idx = 124;

    // EXPECT device to be searched which yields an error
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 124");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, NULL);  // <-- the error

    // WHEN asking the fields of the gmap port with that index
    port_fields_output = gmap_eth_dev_util_get_gmap_device_fields(port_netdev_idx);

    // THEN we get nothing back
    assert_null(port_fields_output);
}

void test_gmap_eth_dev_util_get_gmap_device_fields__error_in_get(UNUSED void** state) {
    amxc_var_t* list_of_ports;
    amxc_var_t* port_fields_output = NULL;

    // GIVEN a netdev index but no port
    uint32_t port_netdev_idx = 777;

    // EXPECT port to be searched and retrieved, where the latter yields an error
    amxc_var_new(&list_of_ports);
    amxc_var_set_type(list_of_ports, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, list_of_ports, "myport");
    expect_string(__wrap_gmap_devices_find, expression, "self interface && (eth || wifi) && .NetDevIndex == 777");
    expect_value(__wrap_gmap_devices_find, flags, GMAP_NO_ACTIONS);
    will_return(__wrap_gmap_devices_find, list_of_ports);
    expect_string(__wrap_gmap_device_get, key, "myport");
    will_return(__wrap_gmap_device_get, NULL); // <-- the error

    // WHEN asking the fields of the gmap port with that index
    port_fields_output = gmap_eth_dev_util_get_gmap_device_fields(port_netdev_idx);

    // THEN we get nothing back
    assert_null(port_fields_output);
}

void test_gmap_eth_dev_util_str_starts_with(UNUSED void** state) {
    assert_true(gmap_eth_dev_util_str_starts_with("abcdefg", "abc"));
    assert_true(gmap_eth_dev_util_str_starts_with("abcdefg", ""));
    assert_true(gmap_eth_dev_util_str_starts_with("abc", "abc"));
    assert_true(gmap_eth_dev_util_str_starts_with("", ""));
    assert_false(gmap_eth_dev_util_str_starts_with("abc", "bc"));
    assert_false(gmap_eth_dev_util_str_starts_with("abc", "abcdefg"));
    assert_false(gmap_eth_dev_util_str_starts_with("abcdefg", "ABC"));
    assert_false(gmap_eth_dev_util_str_starts_with("abcdefg", NULL));
    assert_false(gmap_eth_dev_util_str_starts_with(NULL, "abc"));
}


void test_gmap_eth_dev_util_who_has_with_retries__all_fail(UNUSED void** state) {
    expect_string_count(__wrap_amxb_be_who_has, object_path, "DHCPv4Server.", 11);
    will_return_count(__wrap_amxb_be_who_has, NULL, 11);
    assert_null(gmap_eth_dev_util_who_has_with_retries("DHCPv4Server.", 10));
}

void test_gmap_eth_dev_util_who_has_with_retries__immediate_success(UNUSED void** state) {
    amxb_bus_ctx_t ctx = {};
    expect_string(__wrap_amxb_be_who_has, object_path, "DHCPv4Server.");
    will_return(__wrap_amxb_be_who_has, &ctx);
    assert_ptr_equal(gmap_eth_dev_util_who_has_with_retries("DHCPv4Server.", 20), &ctx);
}

void test_gmap_eth_dev_util_who_has_with_retries__intermediate_success(UNUSED void** state) {
    amxb_bus_ctx_t ctx = {};
    expect_string_count(__wrap_amxb_be_who_has, object_path, "DHCPv4Server.", 6);
    will_return_count(__wrap_amxb_be_who_has, NULL, 5);
    will_return(__wrap_amxb_be_who_has, &ctx);
    assert_ptr_equal(gmap_eth_dev_util_who_has_with_retries("DHCPv4Server.", 30), &ctx);
}

void test_gmap_eth_dev_util_who_has_with_retries__success_in_last_possible_attempt(UNUSED void** state) {
    amxb_bus_ctx_t ctx = {};
    expect_string_count(__wrap_amxb_be_who_has, object_path, "DHCPv4Server.", 41);
    will_return_count(__wrap_amxb_be_who_has, NULL, 40);
    will_return(__wrap_amxb_be_who_has, &ctx);
    assert_ptr_equal(gmap_eth_dev_util_who_has_with_retries("DHCPv4Server.", 40), &ctx);
}

void test_gmap_eth_dev_util_who_has_with_retries__invalid_argument(UNUSED void** state) {
    assert_null(gmap_eth_dev_util_who_has_with_retries(NULL, 40));
}