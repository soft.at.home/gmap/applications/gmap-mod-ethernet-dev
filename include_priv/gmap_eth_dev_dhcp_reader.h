/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_DHCP_READER_H__)
#define __GMAP_ETH_DEV_DHCP_READER_H__

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @file Reads from TR-181 DHCPv4 datamodel and translates that to C function calls
 *
 * The responsibility of this file is to fetch and listen to changes under `DHCPv4Server.Pool.`
 * and to translate that information to appropriate C function calls like
 * @ref gmap_eth_dev_dhcp_ip_on_delete(), @ref gmap_eth_dev_dhcp_option_on_add_or_change(), etc.
 *
 * Note that the responsibility is strictly limited to performing this translation, i.e.
 * interpreting what is going on in TR-181 DHCPv4's datamodel. Any decision making or
 * calls to lib_gmap-client / gmap-server is therefore not a responsibility of this file.
 * That is the responsibility of `gmap_eth_dev_dhcp_ip.c` and `gmap_eth_dev_dhcp_option.c`.
 *
 * Also note that the functionality provided by `gmap_eth_dev_dhcp_*` is strictly
 * limited to DHCPv4. DHCPv6 is not supported.
 */


#include <amxc/amxc_macros.h>
#include <stdbool.h>
#include "gmap_eth_dev_bridge.h"

typedef struct gmap_eth_dev_dhcp_reader gmap_eth_dev_dhcp_reader_t;

void PRIVATE gmap_eth_dev_dhcp_reader_new(gmap_eth_dev_dhcp_reader_t** dhcp, gmap_eth_dev_bridge_t* bridge);
void PRIVATE gmap_eth_dev_dhcp_reader_delete(gmap_eth_dev_dhcp_reader_t** dhcp);

bool PRIVATE gmap_eth_dev_dhcp_reader_is_static_address(const char* lease_path, const char* ip, const char* mac);

/**
 * Gets the IP that the DHCP server has assigned to given MAC.
 *
 * This is unrelated to whether the client is inactive or unreachable.
 */
bool PRIVATE gmap_eth_dev_dhcp_reader_ip_of_mac(char** tgt_ip, const char* pool_path, const char* mac);

#ifdef __cplusplus
}
#endif

#endif