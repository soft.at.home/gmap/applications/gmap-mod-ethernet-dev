/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_LINKER_H__)
#define __GMAP_ETH_DEV_LINKER_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <stdlib.h>
#include "gmap_eth_dev_bridge.h"
#include "gmap_eth_dev_util.h"

/**
 * @file Links the devices (laptop, ...) to the port (ethernet port or wifi access point).
 *
 * This creates the links in gmap and removes them as well.
 *
 * It supports:
 * - the case where, because of no guarantee on timings, the port does not exist yet in gmap
 *   while netlink/NetDev already knows which port a device is for
 * - the case where the port did not exist at startup but now it does
 * - the case where the port exists in gmap but its netdevindex is not populated yet
 * - the case where the port exists in gmap but not in netlink/NetDev yet
 *   (but nothing special needs to be done for this case)
 */

typedef struct gmap_eth_dev_linker gmap_eth_dev_linker_t;

void PRIVATE gmap_eth_dev_linker_new(gmap_eth_dev_linker_t** linker,
                                     const gmap_eth_dev_bridge_t* bridge);
void PRIVATE gmap_eth_dev_linker_delete(gmap_eth_dev_linker_t** linker);

/**
 * Links device<->port in gmap now, or later if port appears later in gmap.
 *
 * @param port_fields_cached: of type @ref AMXC_VAR_ID_HTABLE with entries of type @ref amxc_var_t.
 *   Fields of port in gmap if caller happens to already know these upfront (to avoid lookups).
 *   If not NULL, must have entry with key "Key".
 *   If not NULL, must have entry with key "BSSID" if and only if it's a wifi port (i.e. an access point).
 * @param port_netdev_idx: not 0.
 */
void PRIVATE gmap_eth_dev_linker_link(gmap_eth_dev_linker_t* linker,
                                      const amxc_var_t* port_fields_cached, uint32_t port_netdev_idx, const char* dev_key);

/**
 * Consider a device<->port link obsolete.
 *
 * This does not actually remove the link in gmap (so the device still shows up in the topology when
 * navigating the topology from the root node).
 *
 * @param port_netdev_idx: not 0.
 */
void PRIVATE gmap_eth_dev_linker_make_inactive(gmap_eth_dev_linker_t* linker, uint32_t port_netdev_idx, const char* dev_key);

const char* PRIVATE gmap_eth_dev_linker_link_type_for_port(const amxc_var_t* port);

#ifdef __cplusplus
}
#endif
#endif
