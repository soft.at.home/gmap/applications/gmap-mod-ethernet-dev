/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_BRIDGE_PRIV_H__)
#define __GMAP_ETH_DEV_BRIDGE_PRIV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "gmap_eth_dev_bridgetable.h"
#include "gmap_eth_dev_dhcp_reader.h"
#include "gmap_eth_dev_neighbors.h"
#include "gmap_eth_dev_discoping.h"

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>
#include <amxb/amxb_types.h>
#include <gmap/gmap.h>


struct gmap_eth_dev_bridge_collection {
    amxo_parser_t* parser;      /**< The odl parser, not owned */
    gmap_query_t* bridge_query; /**< Receive notifications of new/modified/destroyed bridges*/
    /**
     * Set of bridges that we've encountered and started interacting with.
     *
     * Note that we cannot simply reuse the bookkeeping of opened sockets for this, since opening
     * a socket can fail. So a bridge for which opening the socket failed is not in the list of
     * opened sockets, but must be in the list of bridges that are seen.
     *
     * The key is the gmap name/key/alias of the bridge that is seen,
     * value is @ref gmap_eth_dev_bridge_t.
     */
    amxc_htable_t bridges;
};

struct gmap_eth_dev_bridge {
    amxc_htable_it_t it;

    gmap_eth_dev_bridge_collection_t* collection; /**< Back pointer to the owning collection, not owned */
    char* netdev;                                 /**< NetDev device name, owned */
    uint32_t netdev_index;                        /**< NetDev device index */
    char* mac;                                    /**< Device MAC address, owned */
    char* address;                                /**< IP address, owned */
    uint32_t proto_family;                        /**< Protocol family, typically one of {AF_INET, AF_INET6} */
    gmap_eth_dev_discoping_t* discoping;          /**< Discoping instance, owned */
    gmap_eth_dev_dhcp_reader_t* dhcp;             /**< DHCPv4Server.Pool.X discovery, owned, NULL if not ipv4 */
    gmap_eth_dev_neighbors_t* neighbors;          /**< NetDev.Link.x.Neigh discovery, owned */
    gmap_eth_dev_bridgetable_t* bridgetable;      /**< NetDev.Link.[bridge_port].BridgeTable discovery, owned */
};

#ifdef __cplusplus
}
#endif
#endif
