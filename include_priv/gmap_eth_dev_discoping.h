/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_DISCOPING_H__)
#define __GMAP_ETH_DEV_DISCOPING_H__
#ifdef __cplusplus
extern "C"
{
#endif

/**
 * Discovery source of IP (not device!) of non-link-local non-dhcp addresses.
 *
 * This also marks who manages deletion of the IP entry in gmap:
 * Only IPs with this discovery source, or @ref GMAP_DISCOVERY_SOURCE_IP_AUTO, are deleted by
 * discoping.
 * WARNING: because of the above, if you change the discovery source, you have to check/adapt how
 *          deletion of IPs works!
 */
#define GMAP_DISCOVERY_SOURCE_IP_STATIC "Static"

/**
 * Discovery source of IP (not device!) of link-local (and therefore non-dhcp) address.
 *
 * This also marks who manages deletion of the IP entry in gmap:
 * Only IPs with this discovery source, or @ref GMAP_DISCOVERY_SOURCE_IP_STATIC, are deleted by
 * discoping.
 * WARNING: because of the above, if you change the discovery source, you have to check/adapt how
 *          deletion of IPs works!
 */
#define GMAP_DISCOVERY_SOURCE_IP_AUTO "AutoIP"

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <stdbool.h>
#include <stdint.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

typedef struct gmap_eth_dev_discoping gmap_eth_dev_discoping_t;

void PRIVATE gmap_eth_dev_discoping_new(gmap_eth_dev_discoping_t** discoping, amxo_parser_t* parser);

void PRIVATE gmap_eth_dev_discoping_delete(gmap_eth_dev_discoping_t** discoping);

void PRIVATE gmap_eth_dev_discoping_open_socket(gmap_eth_dev_discoping_t* discoping, const char* netdev_name, const char* ip, const char* mac);

void PRIVATE gmap_eth_dev_discoping_close_socket(gmap_eth_dev_discoping_t* discoping, const char* netdev_name, const char* ip, const char* mac);

void PRIVATE gmap_eth_dev_discoping_verify(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac);

void gmap_eth_dev_discoping_verify_once(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac);

void PRIVATE gmap_eth_dev_discoping_verify_if_unknown(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac);

const char* PRIVATE gmap_eth_dev_discoping_ip_disco_source(uint32_t family, const char* dev_ip);

void PRIVATE gmap_eth_dev_discoping_set_periodic_reverify_enabled(gmap_eth_dev_discoping_t* discoping, const char* ip, bool enabled);

void PRIVATE gmap_eth_dev_discoping_mark_seen_network(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac);

void PRIVATE gmap_eth_dev_discoping_verify_all_unreachable_ips(gmap_eth_dev_discoping_t* discoping, const char* dev_mac, const char* key, uint32_t protocol_family);

#ifdef __cplusplus
}
#endif
#endif
