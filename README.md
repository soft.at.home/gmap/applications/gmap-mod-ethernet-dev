# gMap Mod Ethernet Device

[[_TOC_]]

## Introduction
This gMap module is responsible for detecting neighbour devices and adding them to the datamodel

## Implementation documentation

See [./doc/README.md](./doc/README.md)

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />

    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/amx/gmap
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/amx/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the ambiorix project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libgmap-client](https://gitlab.com/prpl-foundation/components/gmap/libraries/libgmap-client)
- [libsahtrace](https://gitlab.com/soft.at.home/logging/libsahtrace)

#### Build gmap-mod-ethernet-dev

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the gMap project libraries and clone this library in it.

    ```bash
    mkdir -p ~/workspace/amx/gmap/applications
    cd ~/workspace/amx/gmap/applications
    git clone git@gitlab.com:prpl-foundation/components/gmap/applications/gmap-mod-ethernet-dev.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building `gmap-mod-ethernet-dev`. To be able to build `gmap-mod-ethernet-dev` you need `libamxc`, `libamxj`, `libamxd`, `libamxp`, `libamxb`, `libamxo`, `libgmap-client` and `libsahtrace`. These libraries can be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxc
    sudo apt install libamxd
    sudo apt install libamxp
    sudo apt install libamxb
    sudo apt install libamxo
    sudo apt install libgmap-client
    sudo apt install sah-lib-sahtrace-dev
    ```

1. Build it

   In the container:

    ```bash
    cd ~/workspace/amx/gmap/applications/gmap-mod-ethernet-dev
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/amx/gmap/applications/gmap-mod-ethernet-dev
sudo -E make install
```
## Startup script
gmap-mod-ethernet-dev will be started by the startup script of gmap-client
