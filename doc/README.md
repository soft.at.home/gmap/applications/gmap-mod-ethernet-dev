# gmap-mod-ethernet-dev

[[_TOC_]]


## Basic overview

We give a high-level overview of what gmap-mod-ethernet-dev does. This leaves out many many many details in order to be able to serve as an overview.

From NetDev, gmap-mod-ethernet-dev can see something like this:

```
NetDev.Link.eth1.BridgeTable.mac-00_E0_4C_36_10_E3
NetDev.Link.eth1.BridgeTable.mac-00_E0_4C_36_10_E3.MACAddress=00:E0:4C:36:10:E3
NetDev.Link.eth1.Index=11
```

With this information, gmap-mod-ethernet-dev knows a device exists with mac address 00:E0:4C:36:10:E3, which is on the port with netdev index 11.

So gmap-mod-ethernet-dev can create a gmap device with mac address 00:E0:4C:36:10:E3:

```
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.DiscoverySource=bridge
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.PhysAddress=00:E0:4C:36:10:E3
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.Tags=lan edev mac physical eth
```

In gmap, gmap-mod-ethernet-dev can look up which gmap device corresponds to which netdev index:

```
Devices.Device.ethIntf-ETH1.NetDevIndex=11
```

So gmap-mod-ethernet-dev knows that the mac address 00:E0:4C:36:10:E3 belongs to gmap device with key "ethIntf-ETH1".
So gmap-mod-ethernet-dev can link in gmap the device with the port:

```
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.UDevice
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.UDevice.ethIntf-ETH1
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.UDevice.ethIntf-ETH1.Alias=ethIntf-ETH1
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.UDevice.ethIntf-ETH1.Type=ethernet
```

From DHCPv4, gmap-mod-ethernet-dev can learn the IP address and some DHCP options:

```
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.Chaddr=00:e0:4c:36:10:e3
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.IPv4Address
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.IPv4Address.1
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.IPv4Address.1.IPAddress=192.168.1.184
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.Option.5.Tag=55
DHCPv4.Server.Pool.lan.Client.client-00:e0:4c:36:10:e3.Option.5.Value=0102060c0f1a1c79032128292a77f9fc11
```

Based on this information, gmap-mod-ethernet-dev can set the IP address in gmap and fill in some info learned from the DHCP options:
```
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.Tags=lan edev mac physical eth dhcp ipv4 ipv6
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.IPv4Address.1.Address=192.168.1.184
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.IPv4Address.1.AddressSource=DHCP
Devices.Device.ID-b6c70c11-47d6-48bc-889e-e33677a09426.DHCPOption55=1,2,6,12,15,26,28,121,3,33,40,41,42,119,249,252,17
```
The above information can also arrive in any order. For example, the port (ethIntf-ETH1) might not exist while the NetDev BridgeTable entry already exists. In these cases, gmap-mod-ethernet-dev will fill in the information in gmap when it arrives.

## Relation to other components

![relation to other components](./diagrams/gmap-mod-ethernet-dev-relation-to-other-components.drawio.svg)

[gmap-client](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-client) starts the gmap modules such as [gmap-mod-self](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-mod-self/-/blob/main/doc/README.md), gmap-mod-ethernet-dev, etc.
gmap-mod-ethernet-dev retrieves all its data from other components:

- [NetModel](https://gitlab.com/prpl-foundation/components/core/plugins/netmodel/)
- [dhcpv4-manager](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-dhcpv4/) (also called tr181-dhcpv4)
- [libdiscoping](https://gitlab.com/prpl-foundation/components/core/libraries/lib_discoping/README.md)
- [netdev](https://gitlab.com/prpl-foundation/components/core/plugins/netdev/)

gmap-mod-ethernet-dev does not have a datamodel of its own. Instead, it feeds the datamodel of [gmap-server](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-server), who publishes its datamodel under the path `Devices.`. gmap-mod-ethernet-dev does not write to gmap-server's datamodel directly. Instead, uses [libgmap-client](https://gitlab.com/soft.at.home/ambx/gmap/libraries/libgmap-client) which provides a C API to update gmap-server's datamodel, and [libgmap-ext](https://gitlab.com/prpl-foundation/components/gmap/libraries/lib_gmap-ext) which provides a C API to read/write/update IP addresses stored in gmap-server's datamodel as defined in mib-ip.

The data that gmap-mod-ethernet-dev gathers from netmodel, is not read from the datamodel of netmodel directly. Instead, it uses [libnetmodel](https://gitlab.com/prpl-foundation/components/core/libraries/lib_netmodel/) which provides a C API to read and query NetModel's datamodel.



## IPv4 reachability
gmap-mod-ethernet-dev does not determine by itself whether an IP is reachable or not, it lets 
libdiscoping do that. See [libdiscoping](https://gitlab.com/prpl-foundation/components/core/libraries/lib_discoping/README.md) for more information.

```mermaid
sequenceDiagram
    autonumber
    activate libdiscoping
    libdiscoping->>+gmap-mod-ethernet-dev: reachable<br>192.168.1.5<br>1a:2b:3c:4d:5e:6f
    gmap-mod-ethernet-dev->>+gmap-server: Devices.Device.[thedev].<br>IPv4Address.[theip].Status="reachable"
    gmap-server->>+mib-ip: notification
    mib-ip->>+gmap-server: Devices.Device.[thedev].<br>Actives.[mib-ip].Active=true
    gmap-server->>+gmap-server: Devices.Device.[thedev].Active =true
    deactivate gmap-server

    deactivate gmap-server
    deactivate mib-ip
    deactivate gmap-server
    deactivate gmap-mod-ethernet-dev
    deactivate libdiscoping
```

- ❶ gmap-mod-ethernet-dev receives callbacks from libdiscoping when libdiscoping sees than an IP comes online. This also happens when libdiscoping sees that an IP comes offline, and when an IP is colliding (multiple MACs claiming the same IP). When an IP stays the same state according to libdiscoping, no callback is called to reduce system load.
- ❷ gmap-mod-ethernet-dev looks up which gmap device this is about and whether the IP already exists in the gmap device. If needed it creates a new device, because this is one of the ways to discover new devices. gmap-mod-ethernet-dev creates or updates the IP in the gmap device, and sets the status of the IP to "reachable" (or "unreachable" if not reachable, or "error" if colliding).
- ❸ gmap-server updates its datamodel, and tells mib-ip the datamodel is updated
- ❹ mib-ip looks at the IP addresses of the gmap device (there can be many Ip addresses for one device), and updates the "Actives" field for "mib-ip" based on this information.
- ❺ gmap-server looks at the "Actives" field in the datamodel (there can be many more "Actives" fields than just "mib-ip"), and updates the "Active" field for the device based this information.


## IPv6 reachability

The mechanism for for IPv6 reachability is mostly the same, so we only discuss the differences.

For tracking whether an IPv6 address is reachable, gmap-mod-ethernet-dev lets NetDev track it as long as NetDev thinks
it's reachable. As soon as NetDev is not sure it's reachable, we let
[libdiscoping](https://gitlab.com/prpl-foundation/components/core/libraries/lib_discoping/)
take over tracking reachable.
libdiscoping will then periodically send NDP ICMPv6 Neighbor Discovery packets and listens
for replies (NDP ICMPv6 Neighbor Advertisement). When NetDev picks up again that an IPv6 address
is reachable, gmap-mod-ethernet-dev stops libdiscoping's tracking of the IPv6 address again.
Note that final decision that an IPv6 address is down, is done by
libdiscoping and not by NetDev.

This is illustrated in the sequence diagram below:

```mermaid
sequenceDiagram
    participant amxp_timer
    participant linux
    participant NetDev
    participant gmap-mod-eth-dev
    participant libdiscoping
    participant gmap-server

    Note over NetDev: NetDev sees new IP
    linux->>+NetDev: ip reachable
    NetDev->>+gmap-mod-eth-dev: notify ip reachable
    deactivate NetDev
    gmap-mod-eth-dev->>+libdiscoping: mark_seen_network(ip)
    libdiscoping->>+gmap-mod-eth-dev: callback ip is up
    gmap-mod-eth-dev->>+gmap-server: set reachable(ip, true)
    deactivate gmap-server
    deactivate gmap-mod-eth-dev
    deactivate libdiscoping
    gmap-mod-eth-dev->>+libdiscoping: disable periodic reverification of ip
    deactivate libdiscoping
    deactivate gmap-mod-eth-dev

    Note over NetDev: NetDev stops seeing IP, so libdiscoping takes over
    linux->>+NetDev: ip stale
    NetDev->>+gmap-mod-eth-dev: notify ip stale
    deactivate NetDev
    gmap-mod-eth-dev->>+libdiscoping: enable periodic reverification of ip
    deactivate libdiscoping
    deactivate gmap-mod-eth-dev

    amxp_timer->>+libdiscoping: timeblock timer timeout
    libdiscoping->>+linux: send neighbor discovery
    deactivate linux
    deactivate libdiscoping

    linux->>+libdiscoping: received neighbor advertisement packet
    deactivate libdiscoping

    Note over NetDev: NetDev sees IP again
    linux->>+NetDev: ip reachable
    NetDev->>+gmap-mod-eth-dev: notify ip reachable
    deactivate NetDev

    gmap-mod-eth-dev->>+libdiscoping: mark_seen_network(ip)
    deactivate libdiscoping

    gmap-mod-eth-dev->>+libdiscoping: disable periodic reverification of ip
    deactivate libdiscoping
    deactivate gmap-mod-eth-dev

    Note over NetDev: IP goes down
    linux->>+NetDev: ip stale
    NetDev->>+gmap-mod-eth-dev: notify ip stale
    deactivate NetDev

    gmap-mod-eth-dev->>+libdiscoping: enable periodic reverification of ip
    deactivate libdiscoping
    deactivate gmap-mod-eth-dev

    amxp_timer->>+libdiscoping: timeblock timer timeout
    libdiscoping->>+linux: send neighbor discovery
    deactivate linux
    deactivate libdiscoping

    amxp_timer->>+libdiscoping: request timer timeout
    libdiscoping->>+linux: send neighbor discovery
    deactivate linux
    deactivate libdiscoping

    amxp_timer->>+libdiscoping: request timer timeout
    libdiscoping->>+linux: send neighbor discovery
    deactivate linux
    deactivate libdiscoping

    amxp_timer->>+libdiscoping: request timer timeout
    libdiscoping->>+linux: send neighbor discovery
    deactivate linux
    deactivate libdiscoping

    amxp_timer->>+libdiscoping: request timer timeout
    libdiscoping->>+gmap-mod-eth-dev: callback ip is down
    gmap-mod-eth-dev->>+gmap-server: set reachable(ip, false)
    deactivate gmap-server

    deactivate libdiscoping
    deactivate gmap-mod-eth-dev
```

## Detailed design

![Class Diagram](./diagrams/class-diagram.drawio.svg)

|File      |Responsibility|
|----------|--------------|
|entrypoint|glue startup with amxrt|
|gmap_eth_dev|startup and shutdown|
|bridge_collection|Listen when bridges (dis)appear in gmap, per IP  family (ipv4/ipv6)|
|bridge|Represents a bridge for one IP family. Start the rest of gmap-mod-ethernet-dev when a new bridge appears.|
|dhcp_reader| Parses data and notifications from one DHCP pool of DHCPv4Server. Delegates the actual work to dhcp_ip|
|dhcp_ip| Creates/remove/updates Devices.Device.[].IPv4Address entries based on data from DHCPv4server. Triggers discoping if needed.|
|neighbors| Reads from NetDev Linux's IP-MAC mapping. Creates Devices.Device.[].IPv{4,6}Address entries based on this data, and triggers discoping if needed.|
|bridgetable| Reads from NetDev which mac (dis)appears under which port (ethernet port, wifi accesspoint, ...). Based on this data, put the device under the right port in the topology (deleted to linker).|
|linker| Links devices to their port, i.e. creates Devices.Device.[].Link.[].UDevice instances. Deals with the situations when this data is known but cannot be entered into gmap yet because the port does not exist in gmap yet.|
|discoping|Delegates to lib_discoping and contains discoping-related utility functions that are too gmap-specific to belong to lib_discoping.|


