/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_util.h"
#include "gmap_eth_dev_includes.h"
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <string.h>
#include <ctype.h>

static char* s_netdev_index_to_gmap_key(uint32_t netdev_index) {
    amxc_var_t ret;
    bool ok = false;
    int snprintf_ret = -1;
    char query[64] = {0};
    const char* key_const = NULL;
    char* key = NULL;
    amxc_var_init(&ret);

    when_false(netdev_index != 0, exit);

    snprintf_ret = snprintf(query, sizeof(query), "self interface && (eth || wifi) && .NetDevIndex == %" PRIu32, netdev_index);
    ok = snprintf_ret >= 0 && snprintf_ret < (int) sizeof(query);
    when_false_trace(ok, exit, ERROR, "Error formatting query");

    ok = gmap_devices_find(query, GMAP_NO_ACTIONS, &ret);
    when_false_trace(ok, exit, ERROR, "Gmap function invocation failed");

    key_const = amxc_var_constcast(cstring_t, GETI_ARG(GETI_ARG(&ret, 0), 0));
    when_null(key_const, exit);
    key = strdup(key_const);

exit:
    amxc_var_clean(&ret);
    return key;
}

amxc_var_t* gmap_eth_dev_util_get_gmap_device_fields(uint32_t netdev_index) {
    char* key = NULL;
    amxc_var_t* devlist = NULL;
    amxc_var_t* dev_params_var = NULL;
    when_false_trace(netdev_index > 0, exit, ERROR, "Invalid netdevidx %" PRIu32, netdev_index)

    key = s_netdev_index_to_gmap_key(netdev_index);
    if(key == NULL) {
        return NULL;
    }

    devlist = gmap_device_get(key,
                              GMAP_INCLUDE_LINKS |
                              GMAP_NO_ACTIONS |
                              GMAP_INCLUDE_ALTERNATIVES);
    when_null_trace(devlist, exit, ERROR, "Error retrieving device fields. netdevidx=%" PRIu32, netdev_index)

    dev_params_var = amxc_var_get_index(devlist, 0, AMXC_VAR_FLAG_COPY);

exit:
    amxc_var_delete(&devlist);
    free(key);
    return dev_params_var;
}

/**
 * Example:
 * ```
 * path="abc.def.ghi.jkl" nb_items=2, so target_pos will be 8, i.e. index of "ghi.jkl".
 *               ^
 * ```
 */
static bool s_find_right_part_pos(size_t* target_right_pos, const char* path, size_t nb_items) {
    const char* part = path;
    when_null_trace(target_right_pos, error, ERROR, "NULL");
    when_null_trace(path, error, ERROR, "NULL");
    when_false_trace(nb_items != 0, error, ERROR, "Invalid argument");

    for(size_t i = 0; i < nb_items; i++) {
        part = strchr(part, '.');
        when_null(part, error);
        part++;
    }
    *target_right_pos = part - path;
    return true;

error:
    return false;
}

bool gmap_eth_dev_util_split_path(char** tgt_left, char** tgt_right, const char* path, size_t nb_items) {
    bool ok = false;
    size_t right_idx = 0;
    if(tgt_left != NULL) {
        *tgt_left = NULL;
    }
    if(tgt_right != NULL) {
        *tgt_right = NULL;
    }
    when_str_empty_trace(path, error, ERROR, "EMPTY");
    when_false_trace(nb_items != 0, error, ERROR, "Invalid argument");

    ok = s_find_right_part_pos(&right_idx, path, nb_items);
    when_false(ok, error);

    if(tgt_left != NULL) {
        *tgt_left = strdup(path);
        when_null_trace(*tgt_left, error, ERROR, "Out of mem");
        (*tgt_left)[right_idx] = '\0';
    }

    if(tgt_right != NULL) {
        *tgt_right = strdup(path + right_idx);
        when_null_trace(*tgt_right, error, ERROR, "Out of mem");
    }

    return true;

error:
    if(tgt_left != NULL) {
        free(*tgt_left);
        *tgt_left = NULL;
    }
    if(tgt_right != NULL) {
        free(*tgt_right);
        *tgt_right = NULL;
    }
    return false;
}

void gmap_eth_dev_util_str_to_upper(char* const cstr) {
    if(cstr && *cstr) {
        char* str = cstr;
        for(; *str; str++) {
            *str = toupper(*str);
        }
    }
}

bool gmap_eth_dev_util_str_starts_with(const char* string, const char* potential_prefix) {
    when_null_trace(string, error, ERROR, "NULL");
    when_null_trace(potential_prefix, error, ERROR, "NULL");

    return 0 == strncmp(string, potential_prefix, strlen(potential_prefix));

error:
    return false;
}

bool gmap_eth_dev_util_does_ip_exists_in_gmap(const char* dev_key, const char* ip) {
    bool ip_exists_in_gmap = false;
    amxc_var_t old_ip_var;
    amxc_var_init(&old_ip_var);

    gmap_ip_device_get_address(dev_key, gmap_ip_family(ip), ip, &old_ip_var);
    ip_exists_in_gmap = !amxc_var_is_null(&old_ip_var);

    amxc_var_clean(&old_ip_var);
    return ip_exists_in_gmap;
}

const char* gmap_eth_dev_util_scope_of_ip(const char* ip) {
    amxd_status_t status = amxd_status_unknown_error;
    gmap_ip_address_scope_t ip_scope = scope_unknown;
    const char* ip_scope_str = NULL;

    status = gmap_ip_addressScope(gmap_ip_family(ip), ip, &ip_scope);
    when_failed_trace(status, exit, ERROR, "Error on gmap_ip_addressScope()");
    ip_scope_str = gmap_ip_address_scope2string(ip_scope);
exit:
    return ip_scope_str;
}

/**
 *
 * Preconditions:
 *   - `tag` and 'taglist' only contains "normal" characters (i.e. a-zA-Z0-9_- ), so no '%' or '\''.
 *   - `tag` is not empty
 */
bool gmap_eth_dev_util_taglist_contains_tag(const ssv_string_t taglist, const char* tag) {
    amxc_string_t haystack_str;
    amxc_llist_t list;
    bool retval = false;
    amxc_string_init(&haystack_str, 0);
    amxc_llist_init(&list);
    when_null_trace(taglist, exit, ERROR, "NULL");
    when_str_empty_trace(tag, exit, ERROR, "EMPTY");

    amxc_string_set(&haystack_str, taglist);
    amxc_string_split_to_llist(&haystack_str, &list, ' ');
    amxc_llist_for_each(it, &list) {
        const char* candidate = amxc_string_get(amxc_string_from_llist_it(it), 0);
        if(0 == strcmp(candidate, tag)) {
            retval = true;
            goto exit;
        }
    }
exit:
    amxc_llist_clean(&list, amxc_string_list_it_free);
    amxc_string_clean(&haystack_str);
    return retval;
}


/**
 * HACK / workaround - like amxb_be_who_has, but retries a number of times
 *
 * Sometimes @ref amxb_be_who_has timesout.
 * To workaround, this function retries a couple of times.
 *
 * This function should be removed when not needed anymore.
 */
amxb_bus_ctx_t* gmap_eth_dev_util_who_has_with_retries(const char* object_path, uint32_t max_nb_retries) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    when_null_trace(object_path, exit, ERROR, "NULL argument");

    bus_ctx = amxb_be_who_has(object_path);
    uint32_t nb_retries = 0;
    while(bus_ctx == NULL && nb_retries < max_nb_retries) {
        SAH_TRACEZ_ERROR(ME, "Error on amxb_be_who_has(\"%s\") - retrying", object_path);
        bus_ctx = amxb_be_who_has(object_path);
        nb_retries++;
    }
    if(bus_ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "Giving up amxb_be_who_has(\"%s\") after %" PRIu32 " retries", object_path, max_nb_retries);
    }

exit:
    return bus_ctx;
}