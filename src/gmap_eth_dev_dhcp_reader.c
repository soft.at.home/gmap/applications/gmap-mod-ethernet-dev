/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * @file Please read documentation in `gmap_eth_dev_dhcp_reader.h`.
 */

#include "gmap_eth_dev_dhcp_reader.h"
#include "gmap_eth_dev_dhcp_option.h"
#include "gmap_eth_dev_dhcp_ip.h"
#include "gmap_eth_dev_dhcp_clientpath.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"
#include <amxb/amxb_subscribe.h>
#include <amxd/amxd_object_event.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxb/amxb.h>
#include <amxd/amxd_path.h>
#include <netmodel/common_api.h>
#include <netmodel/client.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>

#define GMAP_DHCPV4_CLIENT_SUBPATH "Client."
#define GMAP_DHCPV4_STATIC_ADDRESS_SUBPATH "StaticAddress."

struct gmap_eth_dev_dhcp_reader {
    netmodel_query_t* netmodel_query;
    amxb_subscription_t* subscription_client;
    amxb_subscription_t* subscription_static;
    gmap_eth_dev_discoping_t* discoping;
};

static bool s_get_client_info(char** tgt_mac, bool* tgt_dhcp_client_active, const char* client_path) {
    bool success = false;
    int status = -1;
    amxc_var_t ret;
    const amxc_var_t* parameters = NULL;
    when_null_trace(client_path, exit, ERROR, "NULL");

    amxc_var_init(&ret);
    status = amxb_get(gmap_eth_dev_get_ctx_dhcp(), client_path, 0, &ret, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(status, exit, ERROR, "amxb_get() failed (%d)", status);
    parameters = amxc_var_get_first(amxc_var_get_first(&ret));

    if(tgt_mac != NULL) {
        *tgt_mac = amxc_var_dyncast(cstring_t, GET_ARG(parameters, "Chaddr"));
    }
    if(tgt_dhcp_client_active != NULL) {
        *tgt_dhcp_client_active = GETP_BOOL(parameters, "Active");
    }

    success = true;
exit:
    amxc_var_clean(&ret);
    return success;
}

static bool s_ip_of_expression(char** tgt_ip, const char* expression) {
    const amxc_var_t* ips = NULL;
    bool success = false;
    amxc_var_t ret;
    int retval = -1;
    amxc_var_init(&ret);
    when_null_trace(tgt_ip, exit, ERROR, "NULL");

    retval = amxb_get(gmap_eth_dev_get_ctx_dhcp(), expression, 0, &ret,
                      GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(retval, exit, ERROR, "Error when getting with expression %s", expression);
    ips = amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT);

    amxc_var_for_each(ip_item, ips) {
        const char* ip_candidate = GETP_CHAR(ip_item, "IPAddress");
        if((ip_candidate != NULL) && (ip_candidate[0] != '\0')) {
            if(*tgt_ip != NULL) {
                // TR-181 DHCPv4 says "At most one entry in this table can exist with a given value
                //                     for IPAddress."
                SAH_TRACEZ_ERROR(ME, "TR-181 DHCPv4 specification violated ('%s','%s' for '%s')",
                                 ip_candidate, *tgt_ip, expression);
                break;
            } else {
                *tgt_ip = strdup(ip_candidate);
            }
        }
    }

    success = true;
exit:
    amxc_var_clean(&ret);
    return success;
}

static bool s_dhcp_ip_of_client_path(char** tgt_ip, const char* client_path) {
    amxc_string_t expression;
    bool success = false;
    amxc_string_init(&expression, 128);
    when_null_trace(tgt_ip, exit, ERROR, "NULL");

    amxc_string_appendf(&expression, "%sIPv4Address.", client_path);
    success = s_ip_of_expression(tgt_ip, amxc_string_get(&expression, 0));
exit:
    amxc_string_clean(&expression);
    return success;
}


bool gmap_eth_dev_dhcp_reader_ip_of_mac(char** tgt_ip, const char* pool_path, const char* mac) {
    amxp_expr_t expression;
    amxc_string_t path;
    amxp_expr_status_t status_expr = amxp_expr_status_ok;
    bool success = false;
    int status = -1;
    amxc_string_init(&path, 0);
    when_null_trace(tgt_ip, exit, ERROR, "NULL");
    when_null_trace(pool_path, exit, ERROR, "NULL");

    status_expr = amxp_expr_buildf(&expression, "Chaddr ^= '%s'", mac);
    when_failed_trace(status_expr, exit_expr, ERROR, "Error building expression");

    status = amxc_string_setf(&path, "%sClient.[%s].IPv4Address.[IPAddress != ''].", pool_path, amxp_expr_get_string(&expression));
    when_failed_trace(status, exit_expr, ERROR, "Error building path");

    success = s_ip_of_expression(tgt_ip, amxc_string_get(&path, 0));

exit_expr:
    amxp_expr_clean(&expression);
exit:
    amxc_string_clean(&path);
    return success;
}

static void s_on_dhcp_ip_remove(const amxc_var_t* const notification_data) {
    const char* ip = GETP_CHAR(notification_data, "parameters.IPAddress");

    gmap_eth_dev_dhcp_ip_on_delete(ip);
}

bool gmap_eth_dev_dhcp_reader_is_static_address(const char* client_path, const char* ip, const char* mac) {
    char* pool_path = NULL;
    bool is_static_address = false;
    amxc_var_t ret;
    const amxc_htable_t* ret_ht;
    amxp_expr_t expression;
    amxc_string_t path;
    amxp_expr_status_t status_expr = amxp_expr_status_unknown_error;
    int status = -1;
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    // e.g. client_path = "DHCPv4Server.Pool.1.Client.1."
    //        pool_path = "DHCPv4Server.Pool.1."
    gmap_eth_dev_util_split_path(&pool_path, NULL, client_path, 3);

    status_expr = amxp_expr_buildf(&expression, "Chaddr ^= '%s' and Yiaddr ^= '%s' and Enable==true", mac, ip);
    when_failed_trace(status_expr, exit_expr, ERROR, "Error building expression");

    status = amxc_string_setf(&path, "%sStaticAddress.[%s]", pool_path, amxp_expr_get_string(&expression));
    when_failed_trace(status, exit, ERROR, "Error building path");

    status = amxb_get(gmap_eth_dev_get_ctx_dhcp(), amxc_string_get(&path, 0), 0, &ret, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(status, exit_expr, ERROR, "Error on amxb_get()");

    ret_ht = amxc_var_constcast(amxc_htable_t, amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT));
    is_static_address = !amxc_htable_is_empty(ret_ht);

exit_expr:
    amxp_expr_clean(&expression);
exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    free(pool_path);
    return is_static_address;
}

static void s_on_dhcp_ip_add_or_change(gmap_eth_dev_dhcp_reader_t* dhcp, const char* client_path,
                                       const amxc_var_t* const notification_data) {

    char* mac = NULL;
    const char* ip_old = NULL;
    const char* ip = NULL;
    bool dhcp_client_active = false;
    bool dhcp_is_static = false;
    when_null_trace(dhcp, exit, ERROR, "NULL argument");
    when_null_trace(client_path, exit, ERROR, "NULL argument");
    when_null_trace(notification_data, exit, ERROR, "NULL argument");

    ip_old = GETP_CHAR(notification_data, "parameters.IPAddress.from");
    if(ip_old != NULL) {
        ip = GETP_CHAR(notification_data, "parameters.IPAddress.to");
    } else {
        ip = GETP_CHAR(notification_data, "parameters.IPAddress");
    }

    // This can legitimately happen when a field changes (and that field is not `IPAddress`)
    // Fortunately we are not interested in other fields.
    when_null(ip, exit);

    s_get_client_info(&mac, &dhcp_client_active, client_path);
    when_null_trace(mac, exit, ERROR, "Error getting mac for %s", client_path);

    dhcp_is_static = gmap_eth_dev_dhcp_reader_is_static_address(client_path, ip, mac);

    gmap_eth_dev_dhcp_ip_on_add_or_update(dhcp->discoping, mac, dhcp_client_active, dhcp_is_static, ip_old, ip, true);

exit:
    free(mac);
}

static void s_on_dhcp_option_add_or_change(const char* option_path, const char* client_path, const amxc_var_t* const notification_data) {
    char* mac = NULL;
    const char* option_raw_text = NULL;
    uint32_t option_tag = 0;
    const char* option_raw_text_to = GETP_CHAR(notification_data, "parameters.Value.to");
    bool has_option_tag_to = GETP_ARG(notification_data, "parameters.Tag.to") != NULL;
    amxc_var_t ret;
    amxc_var_init(&ret);

    if((option_raw_text_to == NULL) && !has_option_tag_to) {
        // It's not a change.
        option_raw_text = GETP_CHAR(notification_data, "parameters.Value");
        option_tag = GETP_UINT32(notification_data, "parameters.Tag");
    } else if((option_raw_text_to == NULL) || !has_option_tag_to) {
        // It's a change but one of the two remained the same. So fetch the other.
        // Note: this can also happen if if DHCPv4 changes data without using transactions, but we
        //       ignore this because we cannot even detect whether our input is incorrect.
        //       It's DHCPv4's responsibility to expose its state correctly.
        // Note: the data can be changed again in the meantime. In that case we missed it but
        //       it's not up2date data anyway.
        int status = amxb_get(gmap_eth_dev_get_ctx_netdev(), option_path, 0, &ret, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
        const amxc_var_t* parameters = NULL;
        when_failed_trace(status, exit, ERROR, "amxb_get() failed (%d)", status);
        parameters = amxc_var_get_first(amxc_var_get_first(&ret));
        option_tag = GETP_UINT32(parameters, "Tag");
        option_raw_text = GETP_CHAR(parameters, "Value");
    } else {
        // It's a change of both fields
        option_raw_text = option_raw_text_to;
        option_tag = GETP_UINT32(notification_data, "parameters.Tag.to");
    }
    when_null_trace(option_raw_text, exit, ERROR, "Error getting option value for %s", option_path);

    s_get_client_info(&mac, NULL, client_path);
    when_null_trace(mac, exit, ERROR, "Error getting mac for %s", client_path);

    gmap_eth_dev_dhcp_option_on_add_or_change(mac, option_tag, option_raw_text);

exit:
    free(mac);
    amxc_var_clean(&ret);
}

static void s_on_dhcp_active_change(const char* client_path, const amxc_var_t* const notification_data) {
    char* mac = NULL;
    bool active_from = GETP_BOOL(notification_data, "parameters.Active.from");
    bool active_to = GETP_BOOL(notification_data, "parameters.Active.to");
    char* dev_key = NULL;
    char* ip = NULL;
    when_true(active_from == active_to, exit);
    when_false_trace(s_dhcp_ip_of_client_path(&ip, client_path), exit, ERROR, "Error getting IP");

    s_get_client_info(&mac, NULL, client_path);
    when_null_trace(mac, exit, WARNING, "DHCP client disappeared immediately after changing");

    gmap_eth_dev_dhcp_ip_on_active_change(mac, ip, active_to);

exit:
    free(mac);
    free(dev_key);
    free(ip);
}

/** @param template_path of the form "DHCPv4Server.Pool.<X>.Client." */
static void s_on_new_dhcp_client(const char* template_path, const amxc_var_t* const notification_data) {
    amxc_string_t client_path;
    const char* mac = GETP_CHAR(notification_data, "parameters.Chaddr");
    uint32_t index = GETP_UINT32(notification_data, "index");
    amxc_string_init(&client_path, 64);
    amxc_string_setf(&client_path, "%s%" PRIu32 ".", template_path, index);
    gmap_eth_dev_dhcp_clientpath_set(mac, amxc_string_get(&client_path, 0));
    amxc_string_clean(&client_path);
}

/**
 * Called when instances under `DHCPv4Server.Pool.*.Client.` are created, modified, deleted.
 */
static void s_subscription_client_callback(const char* const sig_name UNUSED,
                                           const amxc_var_t* const notification_data,
                                           void* const priv) {
    const char* path = GETP_CHAR(notification_data, "path");
    char* client_path = NULL;
    char* subpath = NULL;
    const char* notification = GETP_CHAR(notification_data, "notification");
    bool is_instance_removed = notification != NULL && 0 == strcmp(notification, "dm:instance-removed");
    bool is_instance_added = notification != NULL && 0 == strcmp(notification, "dm:instance-added");
    gmap_eth_dev_dhcp_reader_t* dhcp = (gmap_eth_dev_dhcp_reader_t*) priv;
    when_null_trace(dhcp, exit, ERROR, "NULL argument");
    when_null_trace(notification_data, exit, ERROR, "NULL argument");
    when_str_empty_trace(path, exit, ERROR, "Empty path");
    when_null_trace(notification, exit, ERROR, "NULL argument");

    // example: path = "DHCPv4Server.Pool.1.Client.1.Option."
    //   client_path = "DHCPv4Server.Pool.1.Client.1."
    //       subpath = "Option."
    gmap_eth_dev_util_split_path(&client_path, &subpath, path, 5);

    if((subpath == NULL) && is_instance_added) {
        s_on_new_dhcp_client(path, notification_data);

    } else if((subpath == NULL) && is_instance_removed) {
        const char* mac = GETP_CHAR(notification_data, "parameters.Chaddr");
        gmap_eth_dev_dhcp_clientpath_set(mac, NULL);

    } else if(subpath == NULL) {
        // Do nothing (cases below are for subpath not being NULL)

    } else if((0 == strcmp(subpath, "IPv4Address.")) && is_instance_removed) {
        s_on_dhcp_ip_remove(notification_data);

    } else if(gmap_eth_dev_util_str_starts_with(subpath, "IPv4Address.") && !is_instance_removed) {
        s_on_dhcp_ip_add_or_change(dhcp, client_path, notification_data);

    } else if(gmap_eth_dev_util_str_starts_with(subpath, "Option.") && !is_instance_removed) {
        s_on_dhcp_option_add_or_change(path, client_path, notification_data);

    } else if((0 == strcmp(subpath, ""))
              && (0 == strcmp(notification, "dm:object-changed"))
              && (NULL != GETP_ARG(notification_data, "parameters.Active"))) {
        s_on_dhcp_active_change(client_path, notification_data);

    }

exit:
    free(client_path);
    free(subpath);
    return;
}

static void s_subscription_static_callback(const char* const sig_name UNUSED,
                                           const amxc_var_t* const notification_data,
                                           void* const priv UNUSED) {
    const char* notification;
    bool is_instance_removed = false;
    const char* mac = NULL;
    const char* ip = NULL;
    bool static_enabled = NULL;
    amxc_var_t ret;
    bool ok = false;
    const amxc_var_t* parameters = NULL;
    char* dhcp_ip_assigned_to_mac = NULL;
    bool dhcp_is_ip_assigned_to_mac = false;
    char* pool_path = NULL;
    const char* path = NULL;
    amxc_var_init(&ret);
    when_null_trace(notification_data, exit, ERROR, "NULL");

    notification = GETP_CHAR(notification_data, "notification");
    when_null_trace(notification, exit, ERROR, "NULL");
    is_instance_removed = 0 == strcmp(notification, "dm:instance-removed");
    path = GETP_CHAR(notification_data, "path");

    if(0 == strcmp(notification, "dm:object-changed")) {
        // We only received the changed parameter, so fetch the rest.
        // Note: it's currently not supported to deal with removal of static IP configuration in
        //       DHCPv4.
        //       Background is the problem illustrated by the following scenario:
        //       - Static address "ip1"<->"mac1" added
        //       - Lease "ip1"<->"mac1" added
        //       - static configuration changed into "ip2"<->"mac1"
        //       - gmap receives notification "ip1 changed into ip2" (but this notification does
        //         not contain "mac1")
        //       - gmap starts amxb_get() to know which mac this is about
        //       - static configuration changed into "ip2"<->"mac2"
        //       - gmap's amxb_get() finished and returns "ip2"<->"mac2"
        //       - Information that "ip1"<->"mac1" was the old state is lost
        //       - gmap is unable to unmark in gmap's datamodel for "mac1" that "ip1" was static,
        //         because it does not know that the old configuration was "mac1"
        int status = amxb_get(gmap_get_bus_ctx(), path, 0, &ret, gmap_get_timeout());
        const amxc_var_t* parameters_with_path = amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT);
        // Note: amxc_var_get_key instead of GETP_ARG because it's "a.b."=>"value" and
        //       not "a"=>("b"=>"value"))
        parameters = amxc_var_get_key(parameters_with_path, path, AMXC_VAR_FLAG_DEFAULT);
        when_failed_trace(status, exit, ERROR, "Error on amxb_get %s", path);
        when_null_trace(parameters, exit, ERROR, "No data on amxb_get %s", path);
    } else {
        parameters = GET_ARG(notification_data, "parameters");
    }

    mac = GETP_CHAR(parameters, "Chaddr");
    ip = GETP_CHAR(parameters, "Yiaddr");
    static_enabled = GETP_BOOL(parameters, "Enable");

    gmap_eth_dev_util_split_path(&pool_path, NULL, path, 3);
    when_null_trace(pool_path, exit, ERROR, "Error splitting path %s", path);
    ok = gmap_eth_dev_dhcp_reader_ip_of_mac(&dhcp_ip_assigned_to_mac, pool_path, mac);
    when_false_trace(ok, exit, ERROR, "Error checking ip<->mac assignedness");
    dhcp_is_ip_assigned_to_mac = dhcp_ip_assigned_to_mac != NULL && 0 == strcasecmp(dhcp_ip_assigned_to_mac, ip);

    gmap_eth_dev_dhcp_ip_on_static(mac, ip, static_enabled, is_instance_removed, dhcp_is_ip_assigned_to_mac);

exit:
    amxc_var_clean(&ret);
    free(pool_path);
    free(dhcp_ip_assigned_to_mac);
    return;
}

static void s_subscribe(gmap_eth_dev_dhcp_reader_t* dhcp, amxc_string_t* pool_expression) {
    int rv = AMXB_ERROR_UNKNOWN;
    const size_t expression_size = amxc_string_text_length(pool_expression);

    rv = amxc_string_append(pool_expression, GMAP_DHCPV4_CLIENT_SUBPATH, strlen(GMAP_DHCPV4_CLIENT_SUBPATH));
    when_failed_trace(rv, static_subscribe, ERROR, "Error building DHCPv4 expression. Device info from DHCP won't appear in gMap.");
    rv = amxb_subscription_new(&dhcp->subscription_client, gmap_eth_dev_get_ctx_dhcp(), amxc_string_get(pool_expression, 0),
                               "notification in ['dm:instance-added', 'dm:instance-removed', 'dm:object-changed']", s_subscription_client_callback, dhcp);
    when_failed_trace(rv, static_subscribe, ERROR, "Error subscibing to DHCPv4. Device info from DHCP won't appear in gMap.");

static_subscribe:
    amxc_string_remove_at(pool_expression, expression_size, SIZE_MAX);
    rv = amxc_string_append(pool_expression, GMAP_DHCPV4_STATIC_ADDRESS_SUBPATH, strlen(GMAP_DHCPV4_STATIC_ADDRESS_SUBPATH));
    when_failed_trace(rv, exit, ERROR, "Error building DHCPv4 static addresses expression.");
    rv = amxb_subscription_new(&dhcp->subscription_static, gmap_eth_dev_get_ctx_dhcp(), amxc_string_get(pool_expression, 0),
                               "notification in ['dm:instance-added', 'dm:instance-removed', 'dm:object-changed']", s_subscription_static_callback, NULL);
    when_failed_trace(rv, static_subscribe, ERROR, "Error subscribing to DHCPv4 static addresses.");

exit:
    amxc_string_remove_at(pool_expression, expression_size, SIZE_MAX);
}

static void s_init_fetch(gmap_eth_dev_dhcp_reader_t* reader, amxc_string_t* pool_expression) {
    int status = -1;
    size_t expression_size = amxc_string_text_length(pool_expression);
    amxc_var_t ret;
    const amxc_var_t* dhcp_dm = NULL;
    amxc_var_init(&ret);

    status = amxc_string_append(pool_expression, GMAP_DHCPV4_CLIENT_SUBPATH, strlen(GMAP_DHCPV4_CLIENT_SUBPATH));
    when_failed_trace(status, exit, ERROR, "Error building DHCPv4 expression.");
    status = amxb_get(gmap_eth_dev_get_ctx_dhcp(), amxc_string_get(pool_expression, 0), 3, &ret, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(status, exit, ERROR, "amxb_get() failed (%d)", status);
    dhcp_dm = amxc_var_get_first(&ret);
    amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, dhcp_dm)) {
        const char* path = amxc_htable_it_get_key(it);
        char* subpath = NULL;
        char* client_path = NULL;
        const char* mac = NULL;
        const amxc_var_t* params = amxc_htable_it_get_data(it, amxc_var_t, hit);

        gmap_eth_dev_util_split_path(&client_path, &subpath, path, 5);
        if(client_path == NULL) {
            continue;
        }
        mac = GET_CHAR(GET_ARG(dhcp_dm, client_path), "Chaddr");

        if(0 == strcmp("", subpath)) {
            gmap_eth_dev_dhcp_clientpath_set(mac, client_path);

        } else if((0 != strcmp("IPv4Address.", subpath)) && gmap_eth_dev_util_str_starts_with(subpath, "IPv4Address.")) {
            bool dhcp_client_active = GET_BOOL(GET_ARG(dhcp_dm, client_path), "Active");
            const char* ip = GET_CHAR(params, "IPAddress");
            bool dhcp_is_static = gmap_eth_dev_dhcp_reader_is_static_address(client_path, ip, mac);
            gmap_eth_dev_dhcp_ip_on_add_or_update(reader->discoping, mac, dhcp_client_active, dhcp_is_static, NULL, ip, false);

        } else if((0 != strcmp("Option.", subpath)) && gmap_eth_dev_util_str_starts_with(subpath, "Option.")) {
            uint32_t option_tag = GET_UINT32(params, "Tag");
            const char* option_raw_text = GET_CHAR(params, "Value");
            gmap_eth_dev_dhcp_option_on_add_or_change(mac, option_tag, option_raw_text);
        }

        free(subpath);
        free(client_path);
    }
exit:
    amxc_string_remove_at(pool_expression, expression_size, SIZE_MAX);
    amxc_var_clean(&ret);
}

static void s_deferred_init(gmap_eth_dev_dhcp_reader_t* reader, const char* const iface) {
    amxc_string_t pool_expression;
    int status = -1;

    amxc_string_init(&pool_expression, 0);

    when_null_trace(reader, exit, ERROR, "NULL argument");
    when_null_trace(iface, exit, ERROR, "NULL argument");

    status = amxc_string_setf(&pool_expression, GMAP_DHCPV4SERVER_PATH "Pool.[Interface == '%s'].", iface);
    when_failed_trace(status, exit, ERROR, "Failed to build pool expression. Device info from DHCP won't appear in gMap.");

    amxb_subscription_delete(&reader->subscription_client);
    amxb_subscription_delete(&reader->subscription_static);

    s_subscribe(reader, &pool_expression);
    s_init_fetch(reader, &pool_expression);

exit:
    amxc_string_clean(&pool_expression);
    return;
}

static void s_query_iface_callback(const char* sig_name, const amxc_var_t* data, void* priv) {
    gmap_eth_dev_dhcp_reader_t* reader = (gmap_eth_dev_dhcp_reader_t*) priv;
    const char* iface = NULL;

    when_null_trace(sig_name, exit, ERROR, "NULL argument");
    when_null_trace(data, exit, ERROR, "NULL argument");
    when_null_trace(reader, exit, ERROR, "NULL argument");

    iface = amxc_var_constcast(cstring_t, data);
    when_str_empty_trace(iface, exit, ERROR, "Null or non-string argument");

    s_deferred_init(reader, iface);

exit:
    return;
}

static void s_query_iface(gmap_eth_dev_dhcp_reader_t* reader, gmap_eth_dev_bridge_t* bridge) {
    const char* intf = gmap_eth_dev_bridge_get_key(bridge);
    uint32_t proto = gmap_eth_dev_bridge_get_protocol_family(bridge);
    amxc_string_t subscriber;

    amxc_string_init(&subscriber, 0);
    when_not_null_trace(reader->netmodel_query, exit, ERROR,
                        "Double netmodel query for %s protocol %d",
                        intf, proto);

    when_str_empty_trace(intf, exit, ERROR, "Null bridge key");
    amxc_string_setf(&subscriber, "gmap-mod-eth-dev_%s_%d", intf, proto);

    reader->netmodel_query = netmodel_openQuery_getFirstParameter(intf,
                                                                  amxc_string_get(&subscriber, 0),
                                                                  "InterfacePath",
                                                                  "ip",
                                                                  "up",
                                                                  s_query_iface_callback,
                                                                  reader);
    when_null_trace(reader->netmodel_query, exit, ERROR,
                    "Failed to create netmodel query for %s protocol %d",
                    intf, proto);

exit:
    amxc_string_clean(&subscriber);
}

void gmap_eth_dev_dhcp_reader_new(gmap_eth_dev_dhcp_reader_t** reader, gmap_eth_dev_bridge_t* bridge) {
    gmap_eth_dev_discoping_t* discoping = NULL;
    uint32_t protocol_family = 0;

    when_null_trace(reader, exit, ERROR, "NULL argument");
    when_null_trace(bridge, exit, ERROR, "NULL argument");

    protocol_family = gmap_eth_dev_bridge_get_protocol_family(bridge);
    when_false_trace(protocol_family == AF_INET, exit, ERROR, "Invalid protocol family %d, expected %d", protocol_family, AF_INET);

    discoping = gmap_eth_dev_bridge_get_discoping(bridge);
    when_null_trace(discoping, exit, ERROR, "NULL");

    *reader = (gmap_eth_dev_dhcp_reader_t*) calloc(1, sizeof(gmap_eth_dev_dhcp_reader_t));
    when_null_trace(*reader, exit, ERROR, "Out of mem");

    (*reader)->discoping = discoping;
    s_query_iface(*reader, bridge);

exit:
    return;
}

void gmap_eth_dev_dhcp_reader_delete(gmap_eth_dev_dhcp_reader_t** reader) {
    if((reader == NULL) || (*reader == NULL)) {
        return;
    }

    if((*reader)->netmodel_query != NULL) {
        netmodel_closeQuery((*reader)->netmodel_query);
    }
    amxb_subscription_delete(&(*reader)->subscription_client);
    amxb_subscription_delete(&(*reader)->subscription_static);

    free(*reader);
    *reader = NULL;
}

