/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev.h"
#include "gmap_eth_dev_bridge.h"
#include "gmap_eth_dev_bridge_priv.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"

#include <amxb/amxb.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <amxo/amxo.h>
#include <amxp/amxp_slot.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_devices_flags.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>


static void s_bridge_delete_internal(gmap_eth_dev_bridge_t* bridge) {
    when_null(bridge, exit);

    SAH_TRACEZ_INFO(ME, "Deleting bridge for device %s with MAC %s on protocol %d with address %s",
                    bridge->netdev, bridge->mac, bridge->proto_family, bridge->address);

    gmap_eth_dev_discoping_close_socket(bridge->discoping, bridge->netdev, bridge->address, bridge->mac);
    gmap_eth_dev_bridgetable_delete(&(bridge->bridgetable));
    gmap_eth_dev_neighbors_delete(&(bridge->neighbors));
    gmap_eth_dev_dhcp_reader_delete(&(bridge->dhcp));
    gmap_eth_dev_discoping_delete(&(bridge->discoping));
    free(bridge->address);
    free(bridge->mac);
    free(bridge->netdev);

    free(bridge);
exit:
    return;
}

/** @implements amxc_htable_it_delete_t */
static void s_bridge_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    when_null(it, exit);

    s_bridge_delete_internal(amxc_htable_it_get_data(it, gmap_eth_dev_bridge_t, it));
exit:
    return;
}

static gmap_eth_dev_bridge_t* s_bridge_create(gmap_eth_dev_bridge_collection_t* collection,
                                              const char* key,
                                              const char* netdev_name,
                                              uint32_t netdev_index,
                                              const char* mac,
                                              const char* address
                                              ) {
    int status = -1;
    gmap_eth_dev_bridge_t* bridge = NULL;

    when_str_empty_trace(key, error, ERROR, "NULL/empty argument");
    when_str_empty_trace(netdev_name, error, ERROR, "NULL/empty argument");
    when_str_empty_trace(mac, error, ERROR, "NULL/empty argument");
    when_str_empty_trace(address, error, ERROR, "NULL/empty argument");

    bridge = (gmap_eth_dev_bridge_t*) calloc(1, sizeof(gmap_eth_dev_bridge_t));
    when_null_trace(bridge, error, ERROR, "Out of mem");
    bridge->collection = collection;

    status = amxc_htable_insert(&collection->bridges, key, &bridge->it);
    when_true_trace(status != 0, error, ERROR, "Failed to store bridge");

    /* fill in the fixed bridge data */
    bridge->netdev = strdup(netdev_name);
    when_null_trace(bridge->netdev, error, ERROR, "Out of mem");
    bridge->netdev_index = netdev_index;
    bridge->mac = strdup(mac);
    when_null_trace(bridge->mac, error, ERROR, "Out of mem");
    bridge->address = strdup(address);
    when_null_trace(bridge->address, error, ERROR, "Out of mem");
    bridge->proto_family = gmap_ip_family(address);

    /* create discoping socket */
    gmap_eth_dev_discoping_new(&(bridge->discoping), collection->parser);
    when_null_trace(bridge->discoping, error, ERROR, "Failed to create discoping instance");
    gmap_eth_dev_discoping_open_socket(bridge->discoping, netdev_name, address, mac);

    gmap_eth_dev_neighbors_new(&(bridge->neighbors), bridge);
    gmap_eth_dev_bridgetable_new(&(bridge->bridgetable), bridge);

    SAH_TRACEZ_INFO(ME, "Created %zuth bridge %s for device %s with MAC %s on protocol %d with address %s",
                    amxc_htable_size(&collection->bridges), key, bridge->netdev, bridge->mac, bridge->proto_family, bridge->address);

    return bridge;

error:
    s_bridge_delete_internal(bridge);
    return NULL;
}

static void s_bridge_on_dhcpserver(gmap_eth_dev_bridge_t* bridge, amxb_bus_ctx_t* dhcp) {
    if((bridge != NULL) &&
       (bridge->proto_family == AF_INET) &&
       (bridge->dhcp == NULL) &&
       (dhcp != NULL)) {
        gmap_eth_dev_dhcp_reader_new(&(bridge->dhcp), bridge);
    }
}

/** @brief Removes all bridge instances for the given bridge device key. */
static void s_kill_bridge(gmap_eth_dev_bridge_collection_t* collection, const char* dev_key) {
    when_null_trace(collection, exit, ERROR, "NULL argument");
    when_str_empty_trace(dev_key, exit, ERROR, "Empty argument");

    amxc_htable_for_each(it, &collection->bridges) {
        const char* key = amxc_htable_it_get_key(it);
        if(strcmp(key, dev_key) == 0) {
            SAH_TRACEZ_INFO(ME, "Killing bridge %s for address %s", dev_key,
                            amxc_htable_it_get_data(it, gmap_eth_dev_bridge_t, it)->address);
            amxc_htable_it_clean(it, s_bridge_delete);
        }
    }

exit:
    return;
}

/**
 * @brief
 * Updates the bridge collection for the bridge with the given device key.
 * After this function returns,
 * - all previous entries are removed on netdev or mac changes, and
 * - the entry for all old addresses of this bridge will be removed, and
 * - a new entry will be created for each new bridge address, and
 * - known entries that are still valid are left untouched.
 */
static void s_update_bridge(gmap_eth_dev_bridge_collection_t* collection,
                            const char* dev_key,
                            const char* netdev_name,
                            uint32_t netdev_index,
                            const char* mac,
                            amxc_var_t* ips) {
    amxb_bus_ctx_t* dhcp = gmap_eth_dev_get_ctx_dhcp();

    /* Check the known bridge addresses for this device */
    for(amxc_htable_it_t* it = amxc_htable_get(&collection->bridges, dev_key),
        * it_next = amxc_htable_it_get_next_key(it);
        it; it = it_next, it_next = amxc_htable_it_get_next_key(it)) {

        gmap_eth_dev_bridge_t* bridge = amxc_htable_it_get_data(it, gmap_eth_dev_bridge_t, it);
        bool same_netdev = netdev_index == bridge->netdev_index && strcmp(netdev_name, bridge->netdev) == 0;
        bool same_mac = strcmp(mac, bridge->mac) == 0;

        if(!same_netdev || !same_mac) {
            /* Network device or MAC address changed. Destroy the bridge with
             * all its known addresses here and recreate them afterwards. */
            amxc_htable_it_clean(it, s_bridge_delete);
            goto next_bridge;
        }

        /* Verify this address is still in use */
        amxc_var_for_each(ip_var, ips) {
            const char* ip = GET_CHAR(ip_var, "Address");
            bool same_addr = strcmp(ip, bridge->address) == 0;
            if(same_addr) {
                /* Known address, don't touch it and discard the address from the todo list. */
                amxc_var_delete(&ip_var);
                goto next_bridge;
            }
        }

        /* Address is no longer available, remove it from the bridge table. */
        amxc_htable_it_clean(it, s_bridge_delete);

next_bridge:
        continue;
    }

    /* All remaining IP addresses are new addresses for the bridge.*/
    amxc_var_for_each(ip_var, ips) {
        const char* ip = GET_CHAR(ip_var, "Address");
        gmap_eth_dev_bridge_t* bridge = s_bridge_create(collection, dev_key, netdev_name, netdev_index, mac, ip);
        s_bridge_on_dhcpserver(bridge, dhcp);
    }
}

/** @implements gmap_query_cb_t */
static void s_on_bridge_query(gmap_query_t* query,
                              const char* dev_key,
                              amxc_var_t* dev_fields,
                              gmap_query_action_t action) {
    amxd_status_t status = amxd_status_unknown_error;
    gmap_eth_dev_bridge_collection_t* collection = NULL;
    const char* mac = NULL;         /* NULL if bridge is destroyed. */
    const char* netdev_name = NULL; /* NULL if bridge is destroyed. */
    uint32_t netdev_index = 0;
    amxc_var_t ips;
    amxc_var_init(&ips);

    when_null_trace(query, exit, ERROR, "%s NULL argument", dev_key);
    when_null_trace(dev_key, exit, ERROR, "NULL argument");

    collection = (gmap_eth_dev_bridge_collection_t*) query->data;
    when_null_trace(collection, exit, ERROR, "%s NULL collection", dev_key);
    when_true_trace(action == gmap_query_expression_stop_matching, incomplete_device,
                    WARNING, "Device removed for bridge %s", dev_key);

    when_null_trace(dev_fields, exit, ERROR, "%s NULL argument", dev_key);

    mac = GET_CHAR(dev_fields, "PhysAddress");
    when_str_empty_trace(mac, incomplete_device,
                         ERROR, "NULL/empty field PhysAddress for %s", dev_key);
    netdev_name = GET_CHAR(dev_fields, "NetDevName");
    when_str_empty_trace(netdev_name, incomplete_device,
                         ERROR, "NetDevName not found for %s", dev_key);
    netdev_index = GET_UINT32(dev_fields, "NetDevIndex");
    when_true_trace(netdev_index == 0, incomplete_device,
                    ERROR, "NetDevIndex not found for %s", dev_key);

    status = gmap_ip_device_get_addresses(dev_key, &ips);
    when_failed_trace(status, exit,
                      ERROR, "Error getting IPs of bridge %s", dev_key);

    s_update_bridge(collection, dev_key, netdev_name, netdev_index, mac, &ips);

    goto exit;

incomplete_device:
    s_kill_bridge(collection, dev_key);

exit:
    amxc_var_clean(&ips);
    return;
}

static gmap_query_t* s_subscribe_bridges(gmap_eth_dev_bridge_collection_t* collection) {
    gmap_query_t* query = NULL;
    amxc_string_t expr;
    amxc_string_init(&expr, 0);

    amxc_string_setf(&expr, "bridge mac interface lan && .PhysAddress != '' && .NetDevName != '' && (%s || %s)",
                     gmap_ip_family_id2string(AF_INET), gmap_ip_family_id2string(AF_INET6));

    query = gmap_query_open_ext(amxc_string_get(&expr, 0), "eth_dev_bridge", s_on_bridge_query, collection);

    amxc_string_clean(&expr);

    return query;
}

void gmap_eth_dev_bridge_collection_new(gmap_eth_dev_bridge_collection_t** collection,
                                        amxo_parser_t* parser) {
    int status = -1;

    when_null_trace(collection, error, ERROR, "NULL argument");
    when_null_trace(parser, error, ERROR, "NULL argument");
    *collection = (gmap_eth_dev_bridge_collection_t*) calloc(1, sizeof(gmap_eth_dev_bridge_collection_t));
    when_null_trace(*collection, error, ERROR, "Out of mem");

    (*collection)->parser = parser;

    status = amxc_htable_init(&(*collection)->bridges, 4);
    when_failed_trace(status, error, ERROR, "Out of mem");

    (*collection)->bridge_query = s_subscribe_bridges(*collection);
    when_null_trace((*collection)->bridge_query, error, ERROR, "Error initializing query");

    return;

error:
    gmap_eth_dev_bridge_collection_delete(collection);
}

void gmap_eth_dev_bridge_collection_delete(gmap_eth_dev_bridge_collection_t** collection) {
    if((collection == NULL) || (*collection == NULL)) {
        return;
    }

    gmap_query_close((*collection)->bridge_query);

    amxc_htable_clean(&(*collection)->bridges, s_bridge_delete);

    free(*collection);
    *collection = NULL;
}

void PRIVATE gmap_eth_dev_bridge_collection_dhcpserver_available(gmap_eth_dev_bridge_collection_t* collection, amxb_bus_ctx_t* dhcp) {
    when_null(collection, exit);
    when_null(dhcp, exit);

    amxc_htable_for_each(it, &collection->bridges) {
        gmap_eth_dev_bridge_t* bridge = amxc_htable_it_get_data(it, gmap_eth_dev_bridge_t, it);
        s_bridge_on_dhcpserver(bridge, dhcp);
    }

exit:
    return;
}


const char* PRIVATE gmap_eth_dev_bridge_get_key(const gmap_eth_dev_bridge_t* bridge) {
    const char* result = NULL;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = amxc_htable_it_get_key(&(bridge->it));
exit:
    return result;
}

gmap_eth_dev_discoping_t* PRIVATE gmap_eth_dev_bridge_get_discoping(const gmap_eth_dev_bridge_t* bridge) {
    gmap_eth_dev_discoping_t* result = NULL;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->discoping;
exit:
    return result;
}

const char* gmap_eth_dev_bridge_get_netdev_name(const gmap_eth_dev_bridge_t* bridge) {
    const char* result = NULL;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->netdev;
exit:
    return result;
}

uint32_t gmap_eth_dev_bridge_get_netdev_index(const gmap_eth_dev_bridge_t* bridge) {
    uint32_t result = 0;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->netdev_index;
exit:
    return result;
}

const char* gmap_eth_dev_bridge_get_mac_address(const gmap_eth_dev_bridge_t* bridge) {
    const char* result = NULL;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->mac;
exit:
    return result;
}

const char* gmap_eth_dev_bridge_get_ip_address(const gmap_eth_dev_bridge_t* bridge) {
    const char* result = NULL;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->address;
exit:
    return result;
}

uint32_t gmap_eth_dev_bridge_get_protocol_family(const gmap_eth_dev_bridge_t* bridge) {
    uint32_t result = 0;
    when_null_trace(bridge, exit, ERROR, "NULL argument");
    result = bridge->proto_family;
exit:
    return result;
}
