/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_dhcp_ip.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"
#include <amxb/amxb_subscribe.h>
#include <amxd/amxd_object_event.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxb/amxb.h>
#include <amxd/amxd_path.h>
#include <gmap/gmap_devices.h>
#include <gmap/extensions/ip/gmap_ext_ip_devices.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <string.h>
#include <stdlib.h>

/** Value for `Devices.Device.<key>.IPv4Address.<ip>.AddressSource` if IP discovered through DHCP */
#define GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP "DHCP"

void gmap_eth_dev_dhcp_ip_on_delete(const char* ip) {
    amxc_var_t ip_data;
    char* dev_key = gmap_devices_findByIp(ip);
    const char* current_address_source = NULL;
    amxc_var_init(&ip_data);
    when_null_trace(ip, exit, ERROR, "NULL");
    when_null_trace(dev_key, exit, INFO, "No dev has to-be-removed IP %s", ip);

    gmap_ip_device_get_address(dev_key, gmap_ip_family(ip), ip, &ip_data);
    current_address_source = GETP_CHAR(&ip_data, "AddressSource");
    when_null_trace(current_address_source, exit, ERROR, "NULL");

    when_false_trace(0 == strcmp(current_address_source, GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP),
                     exit, WARNING, "Not removing ip %s because owned by other %s", ip, current_address_source);

    gmap_ip_device_delete_address(dev_key, gmap_ip_family(ip), ip);

exit:
    free(dev_key);
    amxc_var_clean(&ip_data);
    return;
}

/**
 * Implements the definition of "Reserved".
 *
 * According to ip.odl in gmap-mibs-common: "[Reserved] is only true when the IPv4 address is
 *   assigned to the device by the DHCPv4 server and the address is configured as a static lease".
 * @param dhcp_is_ip_assigned Whether the DHCP server has the IP assigned to a client.
 *   This is not related to whether it is reachable or not.
 * @param dhcp_is_static Whether the IP is configured in DHCP as static adddress.
 */
static bool s_is_reserved(bool dhcp_is_ip_assigned, bool dhcp_is_static) {
    return dhcp_is_ip_assigned && dhcp_is_static;
}

void gmap_eth_dev_dhcp_ip_on_active_change(const char* mac, const char* ip, bool dhcp_ip_active) {
    char* dev_key = gmap_devices_findByMac(mac);
    when_null_trace(mac, exit, ERROR, "NULL");
    when_null_trace(ip, exit, ERROR, "NULL");
    when_null_trace(dev_key, exit, ERROR, "No gmap dev for '%s'", mac);

    // Note: we ignore error on marking IP as (un)reachable when the IP does not exist in gmap
    gmap_ip_device_set_address(dev_key, gmap_ip_family(ip), ip, gmap_eth_dev_util_scope_of_ip(ip),
                               dhcp_ip_active ? "reachable" : "not reachable",
                               GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP, false);

exit:
    free(dev_key);
    return;
}

void gmap_eth_dev_dhcp_ip_on_add_or_update(gmap_eth_dev_discoping_t* discoping, const char* mac,
                                           bool dhcp_client_active, bool dhcp_is_static, const char* ip_old, const char* ip, bool recently) {

    bool ok = false;
    char* dev_key = NULL;
    bool dev_already_existed = false;
    bool ip_already_existed = false;
    bool reserved = s_is_reserved(true, dhcp_is_static);
    when_null_trace(mac, exit, ERROR, "NULL argument");
    when_null_trace(ip, exit, ERROR, "NULL argument");
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    when_false_trace(0 != strcmp("0.0.0.0", ip), exit, ERROR, "Invalid IP");

    ok = gmap_devices_createDeviceOrGetKey(&dev_key, &dev_already_existed, mac, GMAP_DISCOVERY_SOURCE_DEVICE_DHCP,
                                           "lan edev mac physical ipv4 dhcp", true, NULL, NULL);
    when_false_trace(ok, exit, ERROR, "Failed to create/get a gmap device");
    when_str_empty_trace(dev_key, exit, ERROR, "Device create/get did not yield key");

    if(dev_already_existed) {
        ok = gmap_device_setTag(dev_key, "ipv4 dhcp", NULL, gmap_traverse_this);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "%s %s Error setting tag!", dev_key, mac);
        }

        if((ip_old != NULL) && (0 != strcasecmp(ip_old, ip))) {
            // Delete if determined by discoping to be unreachable:
            gmap_eth_dev_discoping_verify(discoping, ip_old, mac);
        }
    }

    // To not create 2 entries with the same IP, check if it exists first.
    // Note that this is not robust against other components adding IPs in the timeframe
    // between call to gmap_ip_device_get_address() and call to gmap_ip_device_add_address() or
    // to gmap_ip_device_add_address().
    ip_already_existed = gmap_eth_dev_util_does_ip_exists_in_gmap(dev_key, ip);
    if(!ip_already_existed) {
        // If recently we know the status because we just talked to the IP for DHCP.
        // Otherwise, we don't know, and we put "not reachable" until ARP tells us whether it's
        // reachable.
        const char* dev_ip_status =
            dhcp_client_active && recently ? GMAP_IP_STATUS_REACHABLE
            : GMAP_IP_STATUS_NOT_REACHABLE;

        // Already create before discoping callback to fill in address discovery source.
        amxd_status_t status = gmap_ip_device_add_address(dev_key, gmap_ip_family(ip), ip, gmap_eth_dev_util_scope_of_ip(ip), dev_ip_status, GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP, reserved);
        when_failed_trace(status, exit, ERROR, "Failed to add IPAddress %s (mib_ip inactive?)", ip);
        SAH_TRACEZ_INFO(ME, "%s: Added IPAddress %s", dev_key, ip);

        gmap_eth_dev_discoping_verify(discoping, ip, mac);
    } else {
        // On update (i.e. `ip_exists_in_gmap`==true), `dev_ip_status` is NULL (and not "reachable") because it's not
        // because a field in DHCPv4 datamodel changes that the IP would become reachable, so
        // we do not mark it as reachable:
        const char* dev_ip_status = NULL;

        amxd_status_t status = gmap_ip_device_set_address(dev_key, gmap_ip_family(ip), ip, gmap_eth_dev_util_scope_of_ip(ip), dev_ip_status, GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP, reserved);
        when_failed_trace(status, exit, ERROR, "Failed to update IPAddress %s (mib_ip inactive?)", ip);
        SAH_TRACEZ_INFO(ME, "%s: Updated/Set IPAddress %s", dev_key, ip);

        gmap_eth_dev_discoping_verify_if_unknown(discoping, ip, mac);
    }

exit:
    free(dev_key);
}

/**
 *
 * @return false on error or not found, otherwise true.
 */
static bool s_get_ip_data(bool* tgt_reserved, const char* dev_key, const char* ip) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* is_reachable_str = NULL;
    amxc_var_t ip_fields;
    amxc_var_init(&ip_fields);
    bool ok = false;
    when_null_trace(dev_key, exit, ERROR, "NULL");
    when_null_trace(ip, exit, ERROR, "NULL");

    status = gmap_ip_device_get_address(dev_key, gmap_ip_family(ip), ip, &ip_fields);
    when_failed(status, exit);
    is_reachable_str = GET_CHAR(&ip_fields, "Status");
    when_null_trace(is_reachable_str, exit, ERROR, "'Status' field missing");
    if(tgt_reserved != NULL) {
        *tgt_reserved = GET_BOOL(&ip_fields, "Reserved");
    }
    ok = true;

exit:
    amxc_var_clean(&ip_fields);
    return ok;
}

void gmap_eth_dev_dhcp_ip_on_static(const char* mac, const char* ip, bool static_enabled,
                                    bool is_static_instance_removed, bool dhcp_is_ip_assigned_to_mac) {
    amxd_status_t status = amxd_status_unknown_error;
    bool old_reserved = false;
    bool new_reserved = false;
    char* dev_key = NULL;
    bool ok = false;
    when_null_trace(mac, exit, ERROR, "NULL");
    when_null_trace(ip, exit, ERROR, "NULL");

    dev_key = gmap_devices_findByMac(mac);
    when_null(dev_key, exit);

    ok = s_get_ip_data(&old_reserved, dev_key, ip);
    when_false(ok, exit);

    new_reserved = s_is_reserved(dhcp_is_ip_assigned_to_mac, static_enabled && !is_static_instance_removed);

    if(old_reserved != new_reserved) {
        status = gmap_ip_device_set_address(dev_key, gmap_ip_family(ip), ip, gmap_eth_dev_util_scope_of_ip(ip), NULL, GMAP_DISCOVERY_SOURCE_IPADDRESS_DHCP, new_reserved);
        when_failed_trace(status, exit, ERROR, "Error updating reserved of %s", ip);
    }
exit:
    free(dev_key);
}