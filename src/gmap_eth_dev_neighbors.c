/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_neighbors.h"
#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"

#include <gmap/gmap_devices.h>
#include <gmap/gmap_device.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_variant.h>
#include <amxd/amxd_path.h>
#include <amxb/amxb_subscription.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <arpa/inet.h>

/** Discovery source for devices (not for IP) */
#define GMAP_DISCOVERY_SOURCE_DEVICE_NEIGHBORS "neigh"

struct gmap_eth_dev_neighbors {
    amxb_subscription_t* subscription;
    gmap_eth_dev_discoping_t* discoping;
    uint32_t family;
};

typedef struct {
    /** Can be NULL. Not owned. */
    const char* dev_mac;
    /** Not NULL. Not owned. */
    const char* dev_ip;
    uint32_t family;
    bool reachable;
    bool prev_state_exists_and_was_reachable;
} neighbor_notif_t;

/**
 * Do not call on "ip unreachable" events because that can trigger the undesired scenario
 * of re-creating a deleted device. The undesired scenario in more detail is:
 * (1) device is inactive,
 * (2) device is deleted,
 * (3) ip unreachable event noticed,
 * (4) device created again (unwanted).
 */
static void s_create_device_and_verify_ip(gmap_eth_dev_discoping_t* discoping, const char* mac, const char* ip, uint32_t family) {
    bool ok = false;
    char* dev_key = NULL;
    bool dev_already_existed = false;
    bool ip_already_existed = false;
    const char* family_str = gmap_ip_family_id2string(family);
    amxc_string_t tags;

    amxc_string_init(&tags, 0);

    when_null_trace(mac, exit, ERROR, "NULL argument");
    when_null_trace(ip, exit, ERROR, "NULL argument");
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    when_false_trace(0 != strcmp("0.0.0.0", ip), exit, ERROR, "Invalid IP");
    when_false_trace(family == AF_INET || family == AF_INET6, exit, ERROR, "Unsupported family: %u", family);
    amxc_string_setf(&tags, "lan edev mac physical %s", family_str);

    ok = gmap_devices_createDeviceOrGetKey(&dev_key, &dev_already_existed, mac, GMAP_DISCOVERY_SOURCE_DEVICE_NEIGHBORS,
                                           amxc_string_get(&tags, 0), true, NULL, NULL);
    when_false_trace(ok, exit, ERROR, "Failed to create/get a gmap device");
    when_str_empty_trace(dev_key, exit, ERROR, "Device create/get did not yield key");

    if(dev_already_existed) {
        ok = gmap_device_setTag(dev_key, family_str, NULL, gmap_traverse_this);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "%s %s Error setting tag!", dev_key, mac);
        }
    }

    // Even when we know for sure the IP is reachable, we do not add the IP immediately
    // and instead let discoping run verification of the IP first. The reason is that the IP can be
    // in collision (another MAC claiming the same IP). By running discoping's verification,
    // discoping will also check for collision. If we would not run discoping's verification, we
    // would add the colliding IP with status "reachable" while it must have status "error".
    if(family == AF_INET) {
        ip_already_existed = gmap_eth_dev_util_does_ip_exists_in_gmap(dev_key, ip);
        when_true(ip_already_existed, exit);
        gmap_eth_dev_discoping_verify(discoping, ip, mac);
    } else if(family == AF_INET6) {
        // For ipv6, we don't let discoping do automatic reverification.
        // Instead, we re-verify when NetDev notifies us that it isn't sure anymore that the IP is
        // reachable.
        gmap_eth_dev_discoping_verify_once(discoping, ip, mac);
    }

exit:
    free(dev_key);
    amxc_string_clean(&tags);
}

static bool s_parse_notif(neighbor_notif_t* tgt_notif, const amxc_var_t* const data) {
    const char* family_str = NULL;
    const amxc_var_t* params = NULL;
    memset(tgt_notif, 0, sizeof(neighbor_notif_t));
    bool ok = false;
    const char* state = NULL;
    const char* previous_state = NULL;

    params = GET_ARG(data, "NeighborData");
    when_null_trace(params, exit, ERROR, "Missing NeighborData");

    state = GET_CHAR(params, "State");
    when_str_empty_trace(state, exit, ERROR, "Empty state");
    tgt_notif->reachable = 0 == strcmp(state, "reachable");

    previous_state = GET_CHAR(params, "OldState"); // Can be NULL.
    tgt_notif->prev_state_exists_and_was_reachable = previous_state != NULL && 0 == strcmp(previous_state, "reachable");

    family_str = GET_CHAR(params, "Family");
    when_str_empty_trace(family_str, exit, ERROR, "Empty family");
    tgt_notif->family = gmap_ip_family_string2id(family_str);
    when_false_trace(tgt_notif->family == AF_INET || tgt_notif->family == AF_INET6, exit, INFO, "Unsupported family: %s", family_str);

    tgt_notif->dev_mac = GET_CHAR(params, "LLAddress");
    // Sometimes we get NetDev messages for ipv6 neigh going to "failed":
    //     NetDev.Link.27.Neigh.7.
    //     NetDev.Link.27.Neigh.7.Alias="dyn10"
    //     NetDev.Link.27.Neigh.7.Dst="2a02:1802:94:4340:e4a4:c49f:1d4f:b421"
    //     NetDev.Link.27.Neigh.7.Family="ipv6"
    //     NetDev.Link.27.Neigh.7.Flags=""
    //     NetDev.Link.27.Neigh.7.LLAddress=""
    // The LLAddress becomes empty when the ipv6 neigh goes to failed mode. When it goes to failed
    // mode, the mac field (i.e. NetDev.Link.[].Neigh.[].LLAddress) becomes empty. It goes to failed
    // mode when the client disconnects.
    // So to be able to deal with this, we do not fail parsing when "LLAddress" is empty.
    // when_str_empty_trace(tgt_notif->dev_mac, exit, ERROR, "Empty mac");
    if((tgt_notif->dev_mac != NULL) && (*tgt_notif->dev_mac == '\0')) {
        tgt_notif->dev_mac = NULL;
    }

    tgt_notif->dev_ip = GET_CHAR(params, "Dst");
    when_str_empty_trace(tgt_notif->dev_ip, exit, ERROR, "Empty IP");

    ok = true;

exit:
    return ok;
}

static void s_subscription_callback(const char* const sig_name UNUSED,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    gmap_eth_dev_neighbors_t* neighbors = (gmap_eth_dev_neighbors_t*) priv;
    neighbor_notif_t notif;
    bool ok = s_parse_notif(&notif, data);
    when_false_trace(ok, exit, ERROR, "Error parsing NetDev's neighbor notification");
    /* Silently ignore notifications not meant for us. */
    when_false(notif.family == neighbors->family, exit);

    if(notif.family == AF_INET) {
        if(notif.reachable) {
            when_str_empty_trace(notif.dev_mac, exit, ERROR, "Parsing NetDev neighbor notif did not yield MAC");
            s_create_device_and_verify_ip(neighbors->discoping, notif.dev_mac, notif.dev_ip, notif.family);
        }
    } else if(notif.family == AF_INET6) {
        if(notif.reachable) {
            when_str_empty_trace(notif.dev_mac, exit, ERROR, "Parsing NetDev neighbor notif did not yield MAC");
            gmap_eth_dev_discoping_mark_seen_network(neighbors->discoping, notif.dev_ip, notif.dev_mac);
            gmap_eth_dev_discoping_set_periodic_reverify_enabled(neighbors->discoping, notif.dev_ip, false);
        } else if(!notif.reachable && notif.prev_state_exists_and_was_reachable) {
            gmap_eth_dev_discoping_set_periodic_reverify_enabled(neighbors->discoping, notif.dev_ip, true);
        }
    }

exit:
    return;
}

static void s_subscribe(gmap_eth_dev_neighbors_t* neighbors, const char* netdev_name) {
    int status = -1;
    amxc_string_t expression;
    amxc_string_init(&expression, 0);

    status = amxc_string_setf(&expression, "NetDev.Link.%s.Neigh.", netdev_name);
    when_failed_trace(status, exit, ERROR, "Error building path");
    status = amxb_subscription_new(&neighbors->subscription, gmap_eth_dev_get_ctx_netdev(),
                                   amxc_string_get(&expression, 0),
                                   "notification == 'Neighbor!'",
                                   s_subscription_callback, neighbors);

    when_failed_trace(status, exit, ERROR, "Error subscribing to NetDev");
exit:
    amxc_string_clean(&expression);
    return;
}

static void s_initial_populate(gmap_eth_dev_neighbors_t* neighbors, const char* netdev_name) {
    int status = -1;
    amxc_var_t ret;
    amxc_string_t expression;
    const amxc_var_t* netdev_dm = NULL;
    amxc_string_init(&expression, 0);
    amxc_var_init(&ret);

    status = amxc_string_setf(&expression, "NetDev.Link.%s.Neigh.", netdev_name);
    when_failed_trace(status, exit, ERROR, "Error building path");
    status = amxb_get(gmap_eth_dev_get_ctx_netdev(), amxc_string_get(&expression, 0), 1,
                      &ret, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(status, exit, ERROR, "amxb_get() failed (%d)", status);
    netdev_dm = amxc_var_get_first(&ret);
    amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, netdev_dm)) {
        const amxc_var_t* params = amxc_htable_it_get_data(it, amxc_var_t, hit);
        const char* ip = GET_CHAR(params, "Dst");
        const char* mac = GET_CHAR(params, "LLAddress");
        const char* family_str = GET_CHAR(params, "Family");
        uint32_t family = gmap_ip_family_string2id(family_str);
        if((ip == NULL) || (mac == NULL) || (family_str == NULL)
           || (ip[0] == '\0') || (mac[0] == '\0') || (family_str[0] == '\0')) {
            // Sometimes (e.g. NetDev.Link.[].Neigh.[].Status=error) not all fields are
            // filled in. In that case we can't do anything with the data, so ignore it.
            SAH_TRACEZ_INFO(ME, "NULL ip/mac/family");
            continue;
        }
        if((family != AF_INET) && (family != AF_INET6)) {
            SAH_TRACEZ_INFO(ME, "Unsupported family: %s", family_str);
            continue;
        }
        if(family != neighbors->family) {
            continue;
        }

        s_create_device_and_verify_ip(neighbors->discoping, mac, ip, family);

    }
exit:
    amxc_string_clean(&expression);
    amxc_var_clean(&ret);
}

void gmap_eth_dev_neighbors_new(gmap_eth_dev_neighbors_t** neighbors, const gmap_eth_dev_bridge_t* bridge) {
    gmap_eth_dev_discoping_t* discoping = NULL;
    const char* netdev_name = NULL;
    when_null_trace(neighbors, exit, ERROR, "NULL");
    when_null_trace(bridge, exit, ERROR, "NULL");

    discoping = gmap_eth_dev_bridge_get_discoping(bridge);
    netdev_name = gmap_eth_dev_bridge_get_netdev_name(bridge);
    when_null_trace(discoping, exit, ERROR, "NULL");
    when_str_empty_trace(netdev_name, exit, ERROR, "Null/empty");

    *neighbors = (gmap_eth_dev_neighbors_t*) calloc(1, sizeof(gmap_eth_dev_neighbors_t));
    when_null_trace(*neighbors, exit, ERROR, "Out of mem");
    (*neighbors)->discoping = discoping;
    (*neighbors)->family = gmap_eth_dev_bridge_get_protocol_family(bridge);

    s_subscribe(*neighbors, netdev_name);
    s_initial_populate(*neighbors, netdev_name);

exit:
    return;
}

void gmap_eth_dev_neighbors_delete(gmap_eth_dev_neighbors_t** neighbors) {
    if((neighbors == NULL) || (*neighbors == NULL)) {
        return;
    }

    amxb_subscription_delete(&(*neighbors)->subscription);
    free(*neighbors);
    *neighbors = NULL;
}

