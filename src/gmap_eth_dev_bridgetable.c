/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_bridgetable.h"
#include "gmap_eth_dev_linker.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"

#include <features.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_variant.h>
#include <amxd/amxd_path.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define GMAP_DISCOVERY_SOURCE_BRIDGE "bridge"

struct gmap_eth_dev_bridgetable {
    amxb_subscription_t* subscription;
    gmap_eth_dev_linker_t* linker;
    gmap_eth_dev_discoping_t* discoping;
    uint32_t protocol_family;
};

void gmap_eth_dev_bridgetable_create_or_update_device(gmap_eth_dev_bridgetable_t* bridgetable,
                                                      const char* const dev_mac,
                                                      bool active, uint32_t netdev_idx) {

    const char* dev_tags = "lan edev mac physical";
    char* dev_key = NULL;
    bool ok = false;
    bool already_existed = false;

    when_null_trace(bridgetable, exit, ERROR, "NULL");
    when_null_trace(dev_mac, exit, ERROR, "NULL");

    ok = gmap_devices_createIfActive(&dev_key, &already_existed,
                                     dev_mac,
                                     GMAP_DISCOVERY_SOURCE_BRIDGE,
                                     dev_tags,
                                     true,
                                     NULL,
                                     NULL, active);
    when_false_trace(ok, exit, ERROR, "Error createIfActive %s", dev_mac);
    when_null(dev_key, exit); // can legitimately happen if device is inactive.

    if(active) {
        gmap_eth_dev_linker_link(bridgetable->linker, NULL, netdev_idx, dev_key);
    } else {
        if(already_existed) {
            gmap_eth_dev_linker_make_inactive(bridgetable->linker, netdev_idx, dev_key);
        }
    }

    if(active && already_existed) {
        gmap_eth_dev_discoping_verify_all_unreachable_ips(bridgetable->discoping, dev_mac, dev_key, bridgetable->protocol_family);
    }
exit:
    free(dev_key);
}


uint32_t gmap_eth_dev_bridgetable_index_of_path(const char* path_string) {
    amxd_path_t path;
    uint32_t netdev_index = 0;
    char* index_string = NULL;
    amxd_path_init(&path, path_string);     // e.g. "NetDev.Link.11.BridgeTable.1."
    free(amxd_path_get_first(&path, true)); // e.g. "Link.11.BridgeTable.1."
    free(amxd_path_get_first(&path, true)); // e.g. "11.BridgeTable.1."
    index_string = amxd_path_get_first(&path, false);
    when_str_empty_trace(index_string, exit, ERROR, "Error parsing '%s'", path_string);
    errno = 0;
    netdev_index = (uint32_t) strtol(index_string, NULL, 10);
    if(errno != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid index '%s' in path '%s'", index_string, path_string);
        netdev_index = 0;
    }

exit:
    free(index_string);
    amxd_path_clean(&path);
    return netdev_index;
}

static void s_subscription_callback(const char* const sig_name UNUSED,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    gmap_eth_dev_bridgetable_t* bridgetable = (gmap_eth_dev_bridgetable_t*) priv;
    const char* dev_mac = NULL;
    const char* path = NULL;
    uint32_t netdev_index = 0;
    const char* notification = NULL;
    bool active = false;
    when_null_trace(bridgetable, exit, ERROR, "NULL");

    dev_mac = GETP_CHAR(data, "parameters.MACAddress");
    when_str_empty_trace(dev_mac, exit, ERROR, "Empty mac");

    path = GETP_CHAR(data, "path");
    when_str_empty_trace(dev_mac, exit, ERROR, "Empty path");

    netdev_index = gmap_eth_dev_bridgetable_index_of_path(path);
    when_false_trace(netdev_index != 0, exit, ERROR, "netdev index 0");

    notification = GETP_CHAR(data, "notification");
    when_null_trace(notification, exit, ERROR, "NULL");
    if(0 == strcmp(notification, "dm:instance-removed")) {
        active = false;
    } else if(0 == strcmp(notification, "dm:instance-added")) {
        active = true;
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown notification: '%s'", notification);
        goto exit;
    }

    gmap_eth_dev_bridgetable_create_or_update_device(bridgetable, dev_mac, active, netdev_index);

exit:
    return;
}

static void s_subscribe(gmap_eth_dev_bridgetable_t* bridgetable, uint32_t netdev_index) {
    int rv = -1;
    amxc_string_t expression;

    amxc_string_init(&expression, 0);

    rv = amxc_string_setf(&expression, "NetDev.Link.[Master == %" PRIu32 " && Kind == 'bridge_port'].BridgeTable.", netdev_index);
    when_failed_trace(rv, exit, ERROR, "Error building expression");

    rv = amxb_subscription_new(&bridgetable->subscription, gmap_eth_dev_get_ctx_netdev(), amxc_string_get(&expression, 0),
                               "notification in ['dm:instance-added', 'dm:instance-removed']", s_subscription_callback, bridgetable);
    when_failed_trace(rv, exit, ERROR, "Error subscribing to NetDev. New devices will not appear in gMap.");

exit:
    amxc_string_clean(&expression);
}

/**
 *
 * @return htable: key is device's mac, value is port's netdev-index. Can return NULL on error.
 */
static amxc_htable_t* s_get_bridgetable(const char* expression) {
    amxc_htable_t* bridgetable = NULL;
    amxc_var_t ret_list_of_tables;
    int status = -1;
    const amxc_htable_t* ret_table = NULL;
    amxc_var_init(&ret_list_of_tables);
    status = amxb_get(gmap_eth_dev_get_ctx_netdev(), expression, 10, &ret_list_of_tables, GMAP_ETH_DEV_BUS_TIMEOUT_SEC);
    when_failed_trace(status, exit, ERROR, "Error retrieving bridgestables '%s': %i", expression, status);
    ret_table = amxc_var_constcast(amxc_htable_t, amxc_var_get_index(&ret_list_of_tables, 0, AMXC_VAR_FLAG_DEFAULT));
    amxc_htable_new(&bridgetable, 8);
    when_null_trace(bridgetable, exit, ERROR, "Out of mem");
    amxc_htable_for_each(it, ret_table) {
        const char* path_string = amxc_htable_it_get_key(it);
        const amxc_var_t* fields = amxc_htable_it_get_data(it, amxc_var_t, hit);
        const char* dev_mac = GET_CHAR(fields, "MACAddress");
        uint32_t port_netdev_index = gmap_eth_dev_bridgetable_index_of_path(path_string);
        if((dev_mac != NULL) && (port_netdev_index != 0)) {
            amxc_var_t* value = NULL;
            amxc_var_new(&value);
            amxc_var_set(uint32_t, value, port_netdev_index);
            amxc_htable_insert(bridgetable, dev_mac, &value->hit);
        }
    }
exit:
    amxc_var_clean(&ret_list_of_tables);
    return bridgetable;
}

static void s_initial_populate(gmap_eth_dev_bridgetable_t* bridgetable, uint32_t netdev_index) {
    int status = -1;
    amxc_string_t expression;
    amxc_htable_t* htable = NULL;

    amxc_string_init(&expression, 0);

    when_null_trace(bridgetable, exit, ERROR, "NULL");
    when_true_trace(netdev_index == 0, exit, ERROR, "Zero device index");

    status = amxc_string_setf(&expression, "NetDev.Link.[Master == %" PRIu32 " && Kind == 'bridge_port'].BridgeTable.", netdev_index);
    when_failed_trace(status, exit, ERROR, "Error building expression");

    htable = s_get_bridgetable(amxc_string_get(&expression, 0));
    when_null_trace(htable, exit, ERROR, "Error retrieving bridgetable");

    amxc_htable_for_each(it, htable) {
        const char* dev_mac = amxc_htable_it_get_key(it);
        uint32_t port_netdev_index = amxc_var_constcast(uint32_t, amxc_htable_it_get_data(it, amxc_var_t, hit));
        gmap_eth_dev_bridgetable_create_or_update_device(bridgetable, dev_mac, true, port_netdev_index);
    }
exit:
    amxc_htable_delete(&htable, variant_htable_it_free);
    amxc_string_clean(&expression);
    return;
}

amxc_htable_t* gmap_eth_dev_bridgetable_macs_for_port(uint32_t port_netdev_index) {
    char expression[64] = {0};
    int rv = snprintf(expression, sizeof(expression), "NetDev.Link.%" PRIu32 ".BridgeTable.", port_netdev_index);
    when_false_trace(rv >= 0 && rv < (int) sizeof(expression), error, ERROR, "Error formatting expression");

    return s_get_bridgetable(expression);
error:
    return NULL;
}

void gmap_eth_dev_bridgetable_new(gmap_eth_dev_bridgetable_t** bridgetable,
                                  const gmap_eth_dev_bridge_t* bridge) {
    gmap_eth_dev_discoping_t* discoping = NULL;
    uint32_t netdev_index = 0;
    when_null_trace(bridgetable, exit, ERROR, "NULL");
    when_null_trace(bridge, exit, ERROR, "NULL");
    discoping = gmap_eth_dev_bridge_get_discoping(bridge);
    netdev_index = gmap_eth_dev_bridge_get_netdev_index(bridge);
    when_null_trace(discoping, exit, ERROR, "NULL");
    when_true_trace(netdev_index == 0, exit, ERROR, "Zero device index");
    *bridgetable = (gmap_eth_dev_bridgetable_t*) calloc(1, sizeof(gmap_eth_dev_bridgetable_t));
    when_null_trace(*bridgetable, exit, ERROR, "Out of mem");
    (*bridgetable)->discoping = discoping;
    (*bridgetable)->protocol_family = gmap_eth_dev_bridge_get_protocol_family(bridge);

    gmap_eth_dev_linker_new(&(*bridgetable)->linker, bridge);
    s_subscribe(*bridgetable, netdev_index);
    s_initial_populate(*bridgetable, netdev_index);

exit:
    return;
}

void gmap_eth_dev_bridgetable_delete(gmap_eth_dev_bridgetable_t** bridgetable) {
    if((bridgetable == NULL) || (*bridgetable == NULL)) {
        return;
    }

    amxb_subscription_delete(&(*bridgetable)->subscription);
    gmap_eth_dev_linker_delete(&(*bridgetable)->linker);
    free(*bridgetable);
    *bridgetable = NULL;
}

